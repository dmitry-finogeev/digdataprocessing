/*
 * DigSignals.cpp
 *
 *  Created on: May 11, 2016
 *      Author: mss
 */

#include "includes/DigSignals.h"

DigSignalLOGIC::DigSignalLOGIC(int NumChannel_, DigDataRecipe *DataRecipe_ptr_):DigChannel(NumChannel_, DataRecipe_ptr_){
    if(VERBOSE > 1)printf("LOGIC class created for CH%d\n", ThisNumChannel);
}

DigSignalLOGIC::~DigSignalLOGIC(){
    if(VERBOSE > 1)printf("LOGIC class deconstructed for CH%d\n", ThisNumChannel);
}

void DigSignalLOGIC::Reset(){
    this->DigChannel::Reset(); LogicSignal.Reset();
}

int DigSignalLOGIC::GetSignal(LOGIC_signal **Sg_param_ptr){
    *Sg_param_ptr = &LogicSignal;
    return 1;
}


void DigSignalLOGIC::SetBranchSignal(TTree* DataTree){
    DataTree->Branch(ChannelName,&LogicSignal,"Level/F");
}

void DigSignalLOGIC::CalculateSignal(){
    if(IS_VOID_VALUE(MainSignal.Time)) return;
    if(IS_VOID_VALUE(peak_front_stop_p)||IS_VOID_VALUE(peak_back_start_p)) return;
    if((peak_back_start_p - peak_front_stop_p) < 10)return;
    
    float SLevel = 0, RMSLevel = 0;
    MeanRMScalc(DataBuffer, &SLevel, &RMSLevel, peak_front_stop_p, peak_back_start_p, 1);
    LogicSignal.Level = (SLevel-MainSignal.ZeroLevel)*POLARITY;
}




// -------------------------------------------------
DigSignalAMPLITUDE::DigSignalAMPLITUDE(int NumChannel_, DigDataRecipe *DataRecipe_ptr_):DigChannel(NumChannel_, DataRecipe_ptr_){
    
    AmplProcParam_ptr = DataRecipe_ptr->GetAmplParam(ThisNumChannel);
    
    ZLevel_fit = new TF1("ZLevel_fit","pol9");
    Peak_fit = new TF1("Peak_fit","pol2");
    Front_fit = new TF1("Front_fit","pol1");
    
    if(VERBOSE > 1)printf("AMPLITUDE class created for CH%d\n", ThisNumChannel);
}

DigSignalAMPLITUDE::~DigSignalAMPLITUDE(){
    if(ZLevel_fit) delete ZLevel_fit;
    if(Peak_fit) delete Peak_fit;
    if(Front_fit) delete Front_fit;
    
    if(VERBOSE > 1)printf("AMPLITUDE class deconstructed for CH%d\n", ThisNumChannel);
}

void DigSignalAMPLITUDE::SetBranchSignal(TTree* DataTree){
    DataTree->Branch(ChannelName,&AmplitudeSignal,"mAmplitude/F:pfAmplitude:acTime:pfTime:Charge:prec_ZLevel:prec_ZLevRMS:LEDtime:CFDtime");
}

void DigSignalAMPLITUDE::Reset(){
    this->DigChannel::Reset(); AmplitudeSignal.Reset();
}

TF1* DigSignalAMPLITUDE::GetZLevel_fit(){return ZLevel_fit;}
TF1* DigSignalAMPLITUDE::GetPeak_fit(){return Peak_fit;}
TF1* DigSignalAMPLITUDE::GetFront_fitt(){return Front_fit;}

void DigSignalAMPLITUDE::CalculateSignal(){
    
    const float signal_gap = 10; //ns
    const float peak_range = 1.2; //ns
    const float peak_fit_max_ch = 10;
    
    if(MainSignal.IsVoid()) return;
    
    
    if(AmplProcParam_ptr->isPrecCalc)
    {
        //Zero level by fit
        int npoints = EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT;
        int signal_gap_p = TimeToPoints(signal_gap);
        
        TH1 *Level_hist = new TH1F(Form("Level_hist_%s",ChannelName ), "", npoints+1, 0,  npoints*(*TimeDim_ptr));
        ZLevel_fit->SetRange(0., npoints*(*TimeDim_ptr));
        
        for(int point = 0; point <= npoints; point++)
            Level_hist->SetBinContent(point+1, ((point<peak_start_p-signal_gap_p)||(peak_stop_p+signal_gap_p<point)) ? DataBuffer[point] : MainSignal.ZeroLevel);
        Level_hist->Fit(ZLevel_fit, "QNFR");
        AmplitudeSignal.prec_ZLevel = (ZLevel_fit)? ZLevel_fit->Eval(MainSignal.time_peak) : VOID_VALUE; 
        AmplitudeSignal.prec_ZLevRMS = (ZLevel_fit)? ZLevel_fit->GetChisquare() : VOID_VALUE;
        delete Level_hist;
        
        if( (!IS_VOID_VALUE( AmplitudeSignal.prec_ZLevel)&&(!IS_VOID_VALUE(AmplitudeSignal.prec_ZLevRMS))) )
        {
            //mean amplitude
            float amplitude, RMS; MeanRMScalc(DataBuffer, &amplitude, &RMS,peak_max_p - 1, peak_max_p + 1, 1);
            amplitude = (amplitude- AmplitudeSignal.prec_ZLevel)*POLARITY;
            AmplitudeSignal.mAmplitude = ((amplitude > AmplitudeSignal.prec_ZLevRMS*2.))? amplitude : VOID_VALUE;
            
            
            //fit peak
            Peak_fit->SetRange(MainSignal.time_peak-peak_range*0.5, MainSignal.time_peak+peak_range*0.5);
            SignalShapeGraph->Fit(Peak_fit, "QFRN");
            
            float pol_a = Peak_fit->GetParameter(2);
            float pol_b = Peak_fit->GetParameter(1);
            float pol_c = Peak_fit->GetParameter(0);
            float pol_D = pol_b*pol_b - 4.*pol_a*pol_c;
            
            AmplitudeSignal.pfTime =(Peak_fit->GetChisquare()<peak_fit_max_ch)? -pol_b/pol_a*0.5: VOID_VALUE;
            
            amplitude = (-pol_D/pol_a*0.25 -  AmplitudeSignal.prec_ZLevel)*POLARITY;
            AmplitudeSignal.pfAmplitude = ((amplitude > AmplitudeSignal.prec_ZLevRMS*2.)&&
                                           (Peak_fit->GetChisquare()<peak_fit_max_ch))?amplitude : VOID_VALUE;
        }//if prec Zlevel exist
    }
    else
    {
        float amplitude, RMS; MeanRMScalc(DataBuffer, &amplitude, &RMS,peak_max_p - 1, peak_max_p + 1, 1);
        amplitude = (amplitude-MainSignal.ZeroLevel)*POLARITY;
        AmplitudeSignal.mAmplitude = (amplitude > MainSignal.ZeroLevelRMS*2.)? amplitude : VOID_VALUE;
    }
    
    //if(MainSignal.Amplitude > 100.)
    //{
    //    Int_t signal_gap_p = TimeToPoints(signal_gap);
    
    //    TFile file(Form("Level_hist_%s.root",ChannelName ),"RECREATE");
    //    Level_hist->Write();
    //    delete Level_hist;
    //    file.Close();
    //}
    
    
    
    //Zero level before signal;
    //    Int_t Level_gap_p = TimeToPoints(Level_gap);
    //    Int_t Level_lead_p = TimeToPoints(Level_lead);
    //    if(peak_start_p - Level_gap_p - Level_lead_p < 0)return;
    //    float ZLevelRMS; MeanRMScalc(DataBuffer, &(AmplitudeSignal.prec_ZLevel), &ZLevelRMS, peak_start_p - Level_gap_p - Level_lead_p, peak_start_p - Level_lead_p, 1);
    
    
    //mean amplitude
    //    float amplitude, RMS; MeanRMScalc(DataBuffer, &amplitude, &RMS,peak_max_p - 1, peak_max_p + 1, 1);
    //    AmplitudeSignal.mAmplitude = (AmplitudeSignal.prec_ZLevel-amplitude)*POLARITY;
    //    if(AmplitudeSignal.mAmplitude < 2.* ZLevelRMS){AmplitudeSignal.mAmplitude = VOID_VALUE; return;}
    
    
    //determine charhe
        for(int point = peak_start_p; point <= peak_stop_p; point++){
    
            float ampl_cur = CurAmpl(point); //(DataBuffer[point]-MainSignal.ZeroLevel)*MainSignal.POLARITY;
            // dQ[fC] = dU[mV] * dT[ps] / R[Ω]
            AmplitudeSignal.Charge/*fC*/ += ampl_cur/*mV*/ * (*TimeDim_ptr) /*ps*/ * 0.02/*1/50Ω*/;
    
        }

//    AmplitudeSignal.Charge = SignalIntegral(peak_start_p, peak_stop_p);
    
    
    //    //determine LED time
    //    if(MainSignal.Amplitude > AmplProcParam_ptr->treshold){
    
    //        int peak_LED = peak_start_p;
    //        float ampl_tr = MainSignal.ZeroLevel+AmplProcParam_ptr->treshold/(*AmplitudeDim_ptr)*POLARITY;
    //        AmplitudeSignal.LEDtime = (*TimeDim_ptr)*GoToLevel(DataBuffer, ampl_tr,  &peak_LED, 1);
    
    //    }
    
    //    //determine CFD time
    //    int peak_bipolar = peak_start_p;
    //    int delay_points = floor(AmplProcParam_ptr->CFD_dt / (*TimeDim_ptr) + .5/*rounding*/);
    //    //float bipolar_min = 10000;
    
    //    if(  (peak_bipolar+delay_points+1) < EVENT_PATTERN_LENGHT){
    
    //        while( (peak_bipolar <= peak_stop_p) ){
    //            float origin_signal =  CurAmpl(peak_bipolar); //(DataBuffer[peak_bipolar]-MainSignal.ZeroLevel)*MainSignal.POLARITY;
    //            float delated_signal = CurAmpl(peak_bipolar + delay_points); //(DataBuffer[peak_bipolar + delay_points]-MainSignal.ZeroLevel)*MainSignal.POLARITY;
    //            float bipolar_signal = origin_signal - delated_signal*AmplProcParam_ptr->CFD_fV;
    
    //            float origin_signal_ =  CurAmpl(peak_bipolar+1); //(DataBuffer[peak_bipolar+1]-MainSignal.ZeroLevel)*MainSignal.POLARITY;
    //            float delated_signal_ = CurAmpl(peak_bipolar+1 + delay_points); //(DataBuffer[peak_bipolar+1 + delay_points]-MainSignal.ZeroLevel)*MainSignal.POLARITY;
    //            float bipolar_signal_ = origin_signal_ - delated_signal_*AmplProcParam_ptr->CFD_fV;
    
    //            if(  bipolar_signal*bipolar_signal_ <=  0.  ){
    //                AmplitudeSignal.CFDtime =  (*TimeDim_ptr)*LevelByTPoints(peak_bipolar, bipolar_signal, peak_bipolar+1,bipolar_signal_,0.);
    //                break;
    //            }
    
    //            peak_bipolar++;
    //        } //while
    
    //    }
}

int DigSignalAMPLITUDE::GetSignal(AMPLITUDE_signal **Sg_param_ptr){
    *Sg_param_ptr = &AmplitudeSignal;
    return 1;
}



// -------------------------------------------------
DigSignalPANELX::DigSignalPANELX(int NumChannel_, DigDataRecipe *DataRecipe_ptr_):DigChannel(NumChannel_, DataRecipe_ptr_){
    if(VERBOSE > 1)printf("PANELX class created for CH%d\n", ThisNumChannel);
}

DigSignalPANELX::~DigSignalPANELX(){
    if(VERBOSE > 1)printf("PANELX class deconstructed for CH%d\n", ThisNumChannel);
}

void DigSignalPANELX::SetBranchSignal(TTree* DataTree){
    DataTree->Branch(ChannelName,&PannelxSignal,"Level/F:Xch/I:Xarr[5]/O");
}

void DigSignalPANELX::CalculateSignal(){
    if(IS_VOID_VALUE(peak_front_stop_p)||IS_VOID_VALUE(peak_max_p)) return;
    if(peak_max_p - peak_front_stop_p < 5) return;
    
    float SLevel = 0, RMSLevel = 0;
    MeanRMScalc(DataBuffer, &SLevel, &RMSLevel, peak_front_stop_p, peak_back_start_p, 1);
    PannelxSignal.Level = (SLevel-MainSignal.ZeroLevel)*POLARITY;
    
    //PannelxSignal.Xch = CoordinateByLevel( PannelxSignal.Level, 12., 40., PannelxSignal.Xarr );
    PannelxSignal.Xch = CoordinateByLevel_Ars( MainSignal.Amplitude );
    
}

void DigSignalPANELX::Reset(){
    this->DigChannel::Reset();
    PannelxSignal.Reset();
}

int DigSignalPANELX::GetSignal(PANNELX_signal **Sg_param_ptr){
    *Sg_param_ptr = &PannelxSignal;
    return 1;
}



// -------------------------------------------------
DigSignalPANELY::DigSignalPANELY(int NumChannel_, DigDataRecipe *DataRecipe_ptr_):DigChannel(NumChannel_, DataRecipe_ptr_){
    if(VERBOSE > 1)printf("PANELY class created for CH%d\n", ThisNumChannel);
}

DigSignalPANELY::~DigSignalPANELY(){
    if(VERBOSE > 1)printf("PANELY class deconstructed for CH%d\n", ThisNumChannel);
}

void DigSignalPANELY::SetBranchSignal(TTree* DataTree){
    DataTree->Branch(ChannelName,&PannelySignal,"Level/F:Ych/I:Yarr[5]/O");
}

void DigSignalPANELY::CalculateSignal(){
    if(IS_VOID_VALUE(peak_front_stop_p)||IS_VOID_VALUE(peak_max_p)) return;
    if(peak_max_p - peak_front_stop_p < 5) return;
    
    float SLevel = 0, RMSLevel = 0;
    MeanRMScalc(DataBuffer, &SLevel, &RMSLevel, peak_front_stop_p, peak_back_start_p, 1);
    PannelySignal.Level = (SLevel-MainSignal.ZeroLevel)*POLARITY;
    
   // PannelySignal.Ych = CoordinateByLevel( PannelySignal.Level, 2., 40., PannelySignal.Yarr );
    PannelySignal.Ych = CoordinateByLevel_Ars( MainSignal.Amplitude );
}

void DigSignalPANELY::Reset(){
    this->DigChannel::Reset();
    PannelySignal.Reset();
}

int DigSignalPANELY::GetSignal(PANNELY_signal **Sg_param_ptr){
    *Sg_param_ptr = &PannelySignal;
    return 1;
}

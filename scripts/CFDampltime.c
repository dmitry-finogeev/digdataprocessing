#include "DigVariables.h"
#include "TimeAmplCorr.h"
#include "DigData.h"
#include "utilites.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TFile.h>
#include "TStyle.h"
#include <TProfile.h>
#include <TObjString.h>
#include <TStyle.h>

void CFDampltime()
{


    TObjArray CHcomment;

    /*** //night RUN61
    TString filename = "/media/data1/Data_Oct_2015/14102015_2136_RUN61_5MCP_radiator4x4_1524V_att_twoLFfilters_CFD_tr3mV/unnamed.root";
    CHcomment.Add( new TObjString("1524V 120mV/mip CFDtr2mV, MCP ampl: X2.5") );
    CHcomment.Add( new TObjString("1524V 120mV/mip CFDtr2mV, MCP ampl: X2.5 LPF 300MHz") );
    CHcomment.Add( new TObjString("1524V 120mV/mip CFDtr2mV, MCP ampl: X2.5") );
    CHcomment.Add( new TObjString("1524V 120mV/mip CFDtr2mV, MCP ampl: X2.5, LPF 400MHz") );
    /*** // RUN62
    TString filename = "/media/data1/Data_Oct_2015/15102015_1041_RUN62_5MCP_radiator4x4_1372V_LPF_CFD_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN63
    TString filename = "/media/data1/Data_Oct_2015/15102015_1237_RUN63_5MCP_radiator4x4_1372V_LPF_CFD_z-4_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-4, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-4, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-4, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-4, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN64
    TString filename = "/media/data1/Data_Oct_2015/15102015_1357_RUN64_5MCP_radiator4x4_1372V_LPF_CFD_z-3_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-3, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-3, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-3, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-3, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN65
    TString filename = "/media/data1/Data_Oct_2015/15102015_1443_RUN65_5MCP_radiator4x4_1372V_LPF_CFD_z-2_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-2, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-2, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-2, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-2, MCP ampl: X10, LPF 400MHz") );
    /***/ // RUN66
    TString filename = "/media/data1/Data_Oct_2015/15102015_1554_RUN66_5MCP_radiator4x4_1372V_LPF_CFD_z-1_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-1, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-1, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-1, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-1, MCP ampl: X10, LPF 400MHz") );





    /***/
    TFile *File = new TFile( filename.Data() );
    TTree *tree = (TTree*)File->Get("RawDataTree");

    TObjArray Hists;
    TH2 *currHist = NULL;

    for(int nMCP=1;nMCP<=4;nMCP++){
        Hists.Add( currHist = new TH2F(Form("MCPamplCFDtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5CFD%i time [%s]; MCP Amplitude, mV; CFD Time, ns",
                                 nMCP,nMCP,((TObjString*)CHcomment.At(nMCP-1) )->GetString().Data()),
                            500,0,1000, 1000, 13,28));
        currHist->SetTitleSize(0.05);

        tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",
                         nMCP,nMCP,nMCP),"","col");
    }


HDraw(Hists, "TimeAmpl", 2,2,1,0,100,0,0,"col");


}
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z1, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z1, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN69
    TString filename = "/media/data1/Data_Oct_2015/15102015_1757_RUN68_5MCP_radiator4x4_1372V_LPF_CFD_z1_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z2, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z2, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z2, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z2, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN70
    TString filename = "/media/data1/Data_Oct_2015/15102015_2010_RUN70_5MCP_radiator4x4_1372V_LPF_CFD_z3_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z3, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z3, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z3, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z3, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN71
    TString filename = "/media/data1/Data_Oct_2015/15102015_2048_RUN71_5MCP_radiator4x4_1372V_LPF_CFD_z4_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z4, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z4, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z4, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z4, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN72
    TString filename = "/media/data1/Data_Oct_2015/15102015_2140_RUN72_5MCP_radiator4x4_1372V_z_200_250_-250_-150_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z2, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z2.5, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-2.5, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-1.5, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN73
    TString filename = "/media/data1/Data_Oct_2015/15102015_2208_RUN73_5MCP_radiator4x4_1372V_z_172_150_-250_-125/allMCPampls.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z175, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z150, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-250, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-125, MCP ampl: X10, LPF 400MHz") );
    /*** // RUN74
    TString filename = "/media/data1/Data_Oct_2015/16102015_1119_RUN74_5MCP_radiator4x4_1372V_z_150_125_-275_-150_tr2mV/unnamed.root";
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z150, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z125, MCP ampl: X10 LPF 300MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-275, MCP ampl: X10 LPF 250MHz") );
    CHcomment.Add( new TObjString("1372V 15mV/mip CFDtr2mV z-150, MCP ampl: X10, LPF 400MHz") );





    /***/
    TFile *File = new TFile( filename.Data() );
    TTree *tree = (TTree*)File->Get("RawDataTree");

    TObjArray Hists;
    TH2 *currHist = NULL;
    Int_t crtlARRAY[3] = {0}, crtlIter = 0;

    /***/     //Ampl vs Time MCP
    for(int nMCP=1;nMCP<=4;nMCP++){
        Hists.Add( currHist = new TH2F(Form("MCPamplMCPtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5MCP%i time NO_CRTL [%s]; MCP Amplitude, mV; CFD Time, ns",
                                 nMCP,nMCP,((TObjString*)CHcomment.At(nMCP-1) )->GetString().Data()),
                            100,0,1000, 200, -30,-28));

        crtlIter = 0;
        for(Int_t ch=1; ch <= 4; ch++) if(ch != nMCP){ crtlARRAY[crtlIter++] = ch; printf("%i\n",ch);}


        tree->Draw( Form("main_5MCP%i.Time_pol1-main_T0_Dsc.Time_pol1:main_5MCP%i.Amplitude>>MCPamplMCPtime_%i",nMCP,nMCP,nMCP),
                    Form("(main_5MCP%i.Time_pol1<50)&&((main_5CFD%i.Time_pol1<50)&&(main_5CFD%i.Time_pol1<50))",
                                              crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]),"col");

        Hists.Add( currHist->ProfileX() );

    }
    /***/  //Ampl MCP vs time CFD
    for(int nMCP=1;nMCP<=4;nMCP++){
        Hists.Add( currHist = new TH2F(Form("MCPamplCFDtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5CFD%i time NO_CRTL [%s]; MCP Amplitude, mV; CFD Time, ns",
                                 nMCP,nMCP,((TObjString*)CHcomment.At(nMCP-1) )->GetString().Data()),
                            100,0,1000, 1000, 13,28));

        crtlIter = 0;
        for(Int_t ch=1; ch <= 4; ch++) if(ch != nMCP){ crtlARRAY[crtlIter++] = ch; printf("%i\n",ch);}

        tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP),
                    Form("(main_5CFD%i.Time_pol1<50)&&((main_5CFD%i.Time_pol1<50)&&(main_5CFD%i.Time_pol1<50))",
                         crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]),"col");

        Hists.Add( currHist->ProfileX() );
    }
    /***/


HDraw(Hists, "CFDTimeMCPAmpl",2,2,2,0,100,0,0,"col");


}
(ch != nMCP){ crtlARRAY[crtlIter++] = ch; printf("%i\n",ch);}

        tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP),
                    Form("(main_5CFD%i.Time_pol1<50)&&((main_5CFD%i.Time_pol1<50)&&(main_5CFD%i.Time_pol1<50))",
                         crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]),"col");

        Hists.Add( currHist->ProfileX() );
    }
    /***/


HDraw(Hists, "CFDTimeMCPAmpl",2,2,2,0,100,0,0,"col");


}
 currHist = new TH2F(Form("MCPamplCFDtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5CFD%i time NO_CRTL [%s]; MCP Amplitude, mV; CFD Time, ns",
                                 nMCP,nMCP,((TObjString*)CHcomment.At(nMCP-1) )->GetString().Data()),
                            100,0,1000, 1000, 13,28));

        crtlIter = 0;
        for(Int_t ch=1; ch <= 4; ch++) if(ch != nMCP){ crtlARRAY[crtlIter++] = ch; printf("%i\n",ch);}

        tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP),
                    Form("(main_5CFD%i.Time_pol1<50)&&((main_5CFD%i.Time_pol1<50)&&(main_5CFD%i.Time_pol1<50))",
                         crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]),"col");

        Hists.Add( currHist->ProfileX() );
    }
    /***/


HDraw(Hists, "CFDTimeMCPAmpl",2,2,2,0,100,0,0,"col");


}
MCP=1;nMCP<=4;nMCP++){
        Hists.Add( currHist = new TH2F(Form("MCPamplCFDtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5CFD%i time NO_CRTL [%s]; MCP Amplitude, mV; CFD Time, ns",
                                 nMCP,nMCP,((TObjString*)CHcomment.At(nMCP-1) )->GetString().Data()),
                            100,0,1000, 1000, 13,28));

        crtlIter = 0;
        for(Int_t ch=1; ch <= 4; ch++) if(ch != nMCP){ crtlARRAY[crtlIter++] = ch; printf("%i\n",ch);}

        tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP),
                    Form("(main_5CFD%i.Time_pol1<50)&&((main_5CFD%i.Time_pol1<50)&&(main_5CFD%i.Time_pol1<50))",
                         crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]),"col");

        Hists.Add( currHist->ProfileX() );
    }
    /***/


HDraw(Hists, "CFDTimeMCPAmpl",2,2,2,0,100,0,0,"col");


}
8_RUN85_5MCP_radiator4x4_1514_att_rad4x4_CFD_bestZ_tr3mV/unnamed.root";
    CHcomment.Add( new TObjString("1317 15mV/mip CFDtr3mV z125, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1317 15mV/mip CFDtr3mV z115, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1317 15mV/mip CFDtr3mV z-275, MCP ampl: X10") );
    CHcomment.Add( new TObjString("1317 15mV/mip CFDtr3mV z-175, MCP ampl: X10") );




    /***/



    TFile *File = new TFile( filename.Data() );
    TTree *tree = (TTree*)File->Get("RawDataTree");

    TObjArray Hists;
    TH2 *currHist = NULL;
    Int_t crtlARRAY[3] = {0}, crtlIter = 0;
    TString selection;

    /***/     //Ampl vs Time MCP
    for(int nMCP=1;nMCP<=4;nMCP++){
        Hists.Add( currHist = new TH2F(Form("MCPamplMCPtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5MCP%i time NO_CRTL [%s]; MCP Amplitude, mV; CFD Time, ns",
                                 nMCP,nMCP,((TObjString*)CHcomment.At(nMCP-1) )->GetString().Data()),
                            100,0,1000, 1000, -30,20));

        crtlIter = 0;
        for(Int_t ch=1; ch <= 4; ch++) if(ch != nMCP){ crtlARRAY[crtlIter++] = ch; printf("%i\n",ch);}

        selection = Form("(main_5MCP%i.Amplitude<40)&&((main_5MCP%i.Amplitude<40)&&(main_5MCP%i.Amplitude<40))",
                         crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]);

        selection = "";

        tree->Draw( Form("main_5MCP%i.Time_pol1-main_T0_Dsc.Time_pol1:main_5MCP%i.Amplitude>>MCPamplMCPtime_%i",nMCP,nMCP,nMCP),
                    selection.Data(),"col");

        Hists.Add( currHist->ProfileX() );

    }
    /***/  //Ampl MCP vs time CFD
    for(int nMCP=1;nMCP<=4;nMCP++){
        Hists.Add( currHist = new TH2F(Form("MCPamplCFDtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5CFD%i time NO_CRTL [%s]; MCP Amplitude, mV; CFD Time, ns",
                                 nMCP,nMCP,((TObjString*)CHcomment.At(nMCP-1) )->GetString().Data()),
                            100,0,1000, 1000, 13,28));

        crtlIter = 0;
        for(Int_t ch=1; ch <= 4; ch++) if(ch != nMCP){ crtlARRAY[crtlIter++] = ch; printf("%i\n",ch);}

        selection = Form("(main_5CFD%i.Amplitude<50)&&((main_5CFD%i.Amplitude<50)&&(main_5CFD%i.Amplitude<50))",
                         crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]);

        selection = "";

        tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP),
                    selection.Data(),"col");

        Hists.Add( currHist->ProfileX() );
    }
    /***/


HDraw(Hists, "CFDTimeMCPAmpl",2,2,2,0,100,0,0,"col");


}
ction.Data(),"col");

        //tree->Draw( Form("(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_T0_D2.Time_pol1>>MCPamplMCPtime_%i",nMCP,nMCP), selection.Data(),"col");


        Hists.Add( currHist->ProfileX() );

    }
    /***/  //Ampl MCP vs time CFD
    for(int nMCP=1;nMCP<=4;nMCP++){
        Hists.Add( currHist = new TH2F(Form("MCPamplCFDtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5CFD%i time [%s]; MCP Amplitude, mV; CFD Time, ns",
                                 nMCP,nMCP,((TObjString*)CHcomment.At(nMCP-1) )->GetString().Data()),
                            250,0,1000, 1200, 10,22));

        crtlIter = 0;
        for(Int_t ch=1; ch <= 4; ch++) if(ch != nMCP){ crtlARRAY[crtlIter++] = ch; printf("%i\n",ch);}

        selection = Form("(main_5CFD%i.Amplitude<50)&&((main_5CFD%i.Amplitude<50)&&(main_5CFD%i.Amplitude<50))",
                         crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]);



        selection = Form("(main_T0_C2.Time_pol1 > 120)&&(main_T0_C2.Time_pol1 < 135)&&(main_T0_D2.Time_pol1 > 120)&&(main_T0_D2.Time_pol1 < 135)&&(main_5CFD%i.Time_pol1 > 135)&&(main_5CFD%i.Time_pol1 < 155)",nMCP,nMCP);

       // selection = Form("(main_T0_C2.Time_pol1 > 120)&&(main_T0_C2.Time_pol1 < 135)&&(main_T0_D2.Time_pol1 > 120)&&(main_T0_D2.Time_pol1 < 135)");

        selection = "";

        tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP), selection.Data(),"col");

        //tree->Draw( Form("main_5CFD%i.Time_pol1:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP), selection.Data(),"col");

        //tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP), selection.Data(),"col");


        Hists.Add( currHist->ProfileX() );
    }
    /***/


HDraw(Hists, "CFDTimeMCPAmpl",1,1,2,0,100,0,0,"col");


}
m("(main_5CFD%i.Amplitude<50)&&((main_5CFD%i.Amplitude<50)&&(main_5CFD%i.Amplitude<50))",crtlARRAY[0],crtlARRAY[1],crtlARRAY[2]);



        //selection = Form("(main_T0_C2.Time_pol1 > 120)&&(main_T0_C2.Time_pol1 < 135)&&(main_T0_D2.Time_pol1 > 120)&&(main_T0_D2.Time_pol1 < 135)&&(main_5CFD%i.Time_pol1 > 135)&&(main_5CFD%i.Time_pol1 < 155)",nMCP,nMCP);

       // selection = Form("(main_T0_C2.Time_pol1 > 120)&&(main_T0_C2.Time_pol1 < 135)&&(main_T0_D2.Time_pol1 > 120)&&(main_T0_D2.Time_pol1 < 135)");

        //selection = "((main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5 < 67)&&( 59 < (main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5)";


        selection =           "(main_T0_C2.Time_pol1 > 120)";
        selection +=        "&&(main_T0_C2.Time_pol1 < 135)";
        selection +=        "&&(main_T0_D2.Time_pol1 > 120)";
        selection +=        "&&(main_T0_D2.Time_pol1 < 135)";
        selection +=        "&&(main_T0_Dsc.Time_pol1 > 145)";
        selection +=        "&&(main_T0_Dsc.Time_pol1 < 165)";
        selection +=        "&&((main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5 < 134)";
        selection +=        "&&((main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5 > 120)";
        //selection +=   Form("&&(main_5MCP%i.Time_pol1 > 122.5)",nMCP);
        //selection +=   Form("&&(main_5MCP%i.Time_pol1 < 134.5)",nMCP);
        //selection +=   Form("&&(main_5CFD%i.Time_pol1 > 135.)",nMCP);
        //selection +=   Form("&&(main_5CFD%i.Time_pol1 < 155.)",nMCP);


        tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP), selection.Data(),"col");

        //tree->Draw( Form("main_5CFD%i.Time_pol1:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP), selection.Data(),"col");

        //tree->Draw( Form("main_5CFD%i.Time_pol1-(main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5:main_5MCP%i.Amplitude>>MCPamplCFDtime_%i",nMCP,nMCP,nMCP), selection.Data(),"col");


        //Hists.Add( currHist->ProfileX() );
    }
    /***/


HDraw(Hists, "CFDTimeMCPAmpl",1,1,1,0,100,0,0,"col");


}

/*!
 * \file
 * \brief DigEvent class description
 */

#ifndef DIGEVENT_H
#define DIGEVENT_H

#include "DigVariables.h"
#include "DigDataRecipe.h"
#include "DigChannel.h"
#include "DigSignals.h"

#include <TTree.h>

/*!
 * \brief Manage the list of channels
 * \details class DigEvent contains the list of channels (class DigChannel) connected to digitizer\n
 * It fils list accordance to data configuration while constution and deletes all elements while deconstruntion\n
 * Contains methods related to all channels/n
 */
class DigEvent{
public:
	DigEvent(DigDataRecipe *DataRecipe_ptr_);
	~DigEvent();

private:
	DigDataRecipe *DataRecipe_ptr;
	DigChannel *ChannelList[MAX_CH];
	int NCHloaded;

public:
	DigEvent* SetEventNumber(int Event);
	void CalculateEvent();
	int SetTreeParam(TTree *); //set branches

	DigChannel* GetChannel(int nCh);
	DigChannel* GetChannelByName(char* CHname);

	int GetTotalEvents();
        bool IsSelectorSuit(EventSelectorCond selector);

};

#endif // DIGEVENT_H

/*!
 * \file
 * \brief DigChannel class description
 */

#ifndef DIGCHANNEL_H
#define DIGCHANNEL_H

#include <stdio.h>
#include <stdlib.h>

#include "DigVariables.h"
#include "DigDataRecipe.h"

#include <TGraph.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TF1.h>

/*!
 * \brief Opens data file, analyse signals
 * \details class DigChannel opens file with data recorded by digitizer, event may be read with funtion SetEvent(int NumEvent). \n
 * It also contains all procedure for analysing signal shape.
 * Calculated signals values stored in structure Main_Processing_Param described in DigVariables.h. This structure are writing down in TTree while data saving.\n
 * This class has successors for analyse different signals types described in DigSignals.h
 */
class DigChannel{
public:
	DigChannel(int NumChannel_, DigDataRecipe *DataRecipe_ptr_);
	virtual ~DigChannel();

protected:
	static const int EVENT_PATTERN_LENGHT = 1024;
        static const int N_LAST_POINT_REJECT = 20;
	static const int AD_FIT_POINTS_LENGHT = 50; //aditional points for fitting
	float DataBuffer[EVENT_PATTERN_LENGHT]; //data array, readed in ch and converted to ampl[V] //given ch polarity

	FILE *DataFile; //file with data for this channel

	int ThisNumChannel; //number of current channel
	int CurEvent; //event numer (at which file_pointer point)

	DigDataRecipe *DataRecipe_ptr;
	float *AmplitudeDim_ptr;
	float *TimeDim_ptr;
	char ChannelName[MAX_NAME_LENGHT];

	Main_Processing_Param *MainProcParam_ptr;
	MAIN_signal MainSignal;

	TGraph *SignalShapeGraph; //signal graph by Data[]
	TF1 *SignalFront_pol1;
	TF1 *SignalFront_pol3;
	TCanvas *DemoCanv; //to demonstrate process work



	//temporal peak parameters
	float MaxAmplitude;                                 //max and min amplitude in array
	float MinAmplitude;
	int AmplValid;                                       //if amplitude < minAmpl, event not calculat, AmplValid = 0; -1:ampl was not calculated
	int EventCalculated;                                 //to not calculate event twice
	int MaxAmpl_p;   // point at data array of max ampl	 //                    0 peak_max
	int MinAmpl_p;   //point of  min ampl                // peak_front_end  0      0   peak_back_start
	int peak_max_p;				                  		 //               *           *
	int peak_start_p;				                 	 //             *                *
	int peak_stop_p;		                			 //            *                     *
	int peak_front_stop_p;// point of front end of peak	 //           *                          *
	int peak_back_start_p;// point of start back of peak //          *                               *
	int POLARITY;						                 //         0  peak_start                           0   peak_end
                                                         //     * *                                               *           *

	unsigned int timestamp_;
	unsigned int timestamp_us_;
	
	void Filter(); // HF filter for DataBuffer array
	void FindZeroLevel(); //found signal zero level for MainSignal
	void InsertAdFrontPoints(); //insert aditional front points in graph for goot fitting

	void MeanRMScalc(float*DataArr, float* Mean, float* RMS, int begin, int end, int step);
	float GoToLevel(float *DataArr, float Level, int *start, int iterator);
	float GetFuncXbyY(TF1 *func, float Y_sought_for, float Sigma, float X1, float X2);
	float LevelByTPoints(float X1, float Y1, float X2, float Y2, float Y0);
	float CurAmpl(int ch);
	int CoordinateByLevel(float Level_, float lv_bias, float lv_step, bool *CRDarr);
	int CoordinateByLevel_Ars(float Level_);
	
        float SignalIntegral(int pointFirst, int pointLast); // dQ[fC] = dU[mV] * dT[ps] / R[Ω]

	virtual void CalculateSignal() = 0;
	virtual void SetBranchSignal(TTree*) = 0;
	virtual void Reset();

public:
	int SetEvent(int NumEvent);//read NumEvent, fill TGraph;
	int FindSignalAmplitude();
	int FindSignalAmplitude(float *Ampl);
	int FindSignalParametrs();

	void SetBranch(TTree*);

	int GetSignal(MAIN_signal **Sg_param_ptr);
	//virtual int GetSignal(AMPLITUDE_signal **Sg_param_ptr){return 0;}
	//virtual int GetSignal(LOGIC_signal **Sg_param_ptr){return 0;}
	//virtual int GetSignal(PANNELX_signal **Sg_param_ptr){return 0;}
	//virtual int GetSignal(PANNELY_signal **Sg_param_ptr){return 0;}

    int CheckParCorrect(); //if processing parameters are correct
	int GetNumberOfCh();
	int GetTotalEvents();
	float *GetDataBuffer(int *ArrayPoint_);
	TGraph *GetSignalGraph();
	TF1 *GetSignalFitFunc_pol1();
	TF1 *GetSignalFitFunc_pol3();
    int TimeToPoints(float time);
    float PointsToTime(int point);

};

#endif // DIGCHANNEL_H

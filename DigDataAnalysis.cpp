#include "includes/DigDataAnalysis.h"


void ChannelGroup::PrintOut(){
    if(enabled){
        printf("\"%s\":\n",GroupName);
        for(int ch=0; ch<CHnumber; ch++)printf("\"%s\"; ",CHnames[ch]);
        printf("\n");
    }
}
ChannelGroup::ChannelGroup(){
    enabled = 0;
    CHnumber = 0;
    CHnames = new char*[MAX_CH];
    for(int ch = 0; ch < MAX_CH; ch++) CHnames[ch] = new char[MAX_NAME_LENGHT];
}
ChannelGroup::~ChannelGroup(){
    for(int ch = 0; ch < MAX_CH; ch++) delete []CHnames[ch];
    delete []CHnames;
}




//---------------------------------------------------------------------------------------------------
DigDataAnalysis::DigDataAnalysis(GlobalProcessStatus *state){
    
    gProcessState = state;
    EventToAnalyse = 0;
    EventToStudy = 5000;
    RawDataTree_ptr = NULL;
    DataRecipe = NULL;
    AmplRange = 3.;
    AmplMinRange = 0.;
    TimeRange = 3.;
    XRange = 1.;
    YRange = 1.;
    CrtlGrNumber = 0;
    
    strcpy(StartDetectors[0].GroupName,"Start Detectors GR0");
    StartDetectors[0].CHnumber = 0;
    StartDetectors[0].enabled = 1;
    
    strcpy(StartDetectors[1].GroupName,"Start Detectors GR1");
    StartDetectors[1].CHnumber = 0;
    StartDetectors[1].enabled = 1;
    
    //    TAcorrection = new AliT0TimeAmplCorr();
    //    TAcorrection->SetVerbose(1);
    
    for(int ch=0; ch<MAX_CH; ch++) ATfunc_pol1_ptr[ch] = 0;
    
    
    //ProcDataTree_ptr = new TTree("ProcDataTree","Analysed data");
    //ProcHists_ptr = new TObjArray();
    //ResuHists_ptr = new TObjArray();
        ProcHists.SetOwner(kTRUE);
        ResuHists.SetOwner(kTRUE);
    
}
//---------------------------------------------------------------------------------------------------
DigDataAnalysis::~DigDataAnalysis(){
    
    //    if(TAcorrection) delete TAcorrection;
    
    //    //printf("debug: deconstructor deleting ProcHists\n");
    //    for(Int_t element = 0; element <= ProcHists.GetLast(); element++)
    //        if(ProcHists.At(element)){
    //            //printf("debug: deleting %s\n", ProcHists.At(element)->GetName());
    //            delete ProcHists.At(element);
    //        }
    //    ProcHists.Clear();
    
    //    //printf("debug: deleting ResuHists\n");
    //    for(Int_t element = 0; element <= ResuHists.GetLast(); element++)
    //        if(ResuHists.At(element)){
    //            //printf("debug: deleting %s\n", ResuHists.At(element)->GetName());
    //            delete ResuHists.At(element);
    //        }
    //    ResuHists.Clear();
    
    
}
//---------------------------------------------------------------------------------------------------
void DigDataAnalysis::AddStartDetector(const char *Name, int Groupe){
    
    strcpy( StartDetectors[Groupe].CHnames[ StartDetectors[Groupe].CHnumber ] , Name);
    StartDetectors[Groupe].CHnumber++;
    
}

//---------------------------------------------------------------------------------------------------
void DigDataAnalysis::AddCrtlGroup(char *name, ... ){
    //va_list vaList;
    //va_start(vaList,name);
    
    int nParam = 0;
    char **ArrName = &name;
    for(char **curName = &name; *curName; curName++){
        printf("%i:%s\n",nParam,ArrName[nParam]);
        if(nParam == 0) strcpy(CrtlGroupes[CrtlGrNumber].GroupName, *curName);
        
        if(nParam != 0){
            strcpy(CrtlGroupes[CrtlGrNumber].CHnames[nParam], *curName);
            CrtlGroupes[CrtlGrNumber].CHnumber++;
        }
        nParam ++;
    }
    
    CrtlGroupes[CrtlGrNumber].enabled = 1;
    CrtlGrNumber++;
    //va_end(vaList);
}

//---------------------------------------------------------------------------------------------------
void DigDataAnalysis::GetMeanValForLeaf(ValueRange *ValRange, float(DigDataAnalysis::*func)(int), int Channel, char *name, int Bining,float MinRange, float Range, float FitRange){
    
    float *Mean = &(ValRange->Mean);
    float *Sigma = &(ValRange->Sigma);
    float *MinVal = &(ValRange->MinValue);
    float *MaxVal = &(ValRange->MaxValue);
    *MinVal = 1000;
    *MaxVal = -1000;
    
    printf("Range analysis for CH%i %s: ",Channel,name);
    
    for(int Ev = 0; Ev < EventToStudy; Ev++){
        RawDataTree_ptr->GetEntry(Ev);
        float Val = (this->*func)(Channel);
        if( IS_VOID_VALUE(Val) )continue;
        if(*MinVal > Val) *MinVal = Val;
        if(*MaxVal < Val) *MaxVal = Val;
    }
    
    if(*MaxVal < *MinVal) {if(VERBOSE > 0)printf("DigDataAnalysis::GetMeanValForLeaf: ERROR: No data found\n"); return;}
    
    if(*MinVal < MinRange) *MinVal = MinRange;
    int Val_bin = ceil(*MaxVal-*MinVal)*Bining;
    
    TH1F *ValHist = new TH1F(Form("range_%s",name), Form("Range %s",name),Val_bin, *MinVal,*MaxVal);
    
    for(int Ev = 0; Ev < EventToStudy; Ev++){
        RawDataTree_ptr->GetEntry(Ev);
        float Val = (this->*func)(Channel);
        if( IS_VOID_VALUE(Val) )continue;
        if(Val < *MinVal)continue;
        
        ValHist->Fill(Val);
    }
    
    if(ValHist->GetEntries() > 0){
        *Mean = ValHist->GetMean(); *Sigma = ValHist->GetRMS();
        
        //temporary fit exluded
        
        //TF1 Resolution("Resolution", "gaus");
        //        ValHist->Fit(&Resolution, "QW","",ValRange->MinRange(Range), ValRange->MaxRange(Range) );
        //        *Mean = Resolution.GetParameter(1); *Sigma = Resolution.GetParameter(2);
        
        if(0.1 < FitRange)
            if(ValHist->GetFunction("Resolution"))
                ValHist->GetFunction("Resolution")->SetRange(ValRange->MinRange(FitRange), ValRange->MaxRange(FitRange)); //to see the range oh hists
        ValRange->PrintOut();printf("\n");
        
    }
    else if(VERBOSE > 0)printf("DigDataAnalysis::GetMeanValForLeaf: ERROR: Events too low for fitting\n");
    
    ProcHists.Add( ValHist );
    
}


//---------------------------------------------------------------------------------------------------
float DigDataAnalysis::StartPMTTime(int Ch){
    
    int Groupe = ((Ch < 8)||(Ch == 16)?0:1);
    
    float MeanTime = 0.;
    int Ndetectors = StartDetectors[Groupe].CHnumber, CurCH;
    for(int detector=0; detector<Ndetectors; detector++){
        CurCH = DataRecipe->FindChannelByName( StartDetectors[Groupe].CHnames[detector] );
        float CurTime = MainSignals[CurCH].Time_pol1;
        if( MainSignals[CurCH].IsVoid() ){return VOID_VALUE;}
        MeanTime += CurTime;
    }
    
    return MeanTime / (float)Ndetectors;
}
float DigDataAnalysis::LeafAmpl(int Ch){
    return MainSignals[Ch].IsVoid() ? VOID_VALUE : MainSignals[Ch].Amplitude;
}
float DigDataAnalysis::LeafXcoord(int Ch){
    return MainSignals[Ch].IsVoid() ? VOID_VALUE : PanelXSignals[  DataRecipe->FingChannelByType(XY_PANEL_X)  ].Xch;
}
float DigDataAnalysis::LeafYcoord(int Ch){
    return MainSignals[Ch].IsVoid()? VOID_VALUE : PanelYSignals[  DataRecipe->FingChannelByType(XY_PANEL_Y)  ].Ych;
}
float DigDataAnalysis::LeafTime_pol1(int Ch){
    if(StartPMTTime(Ch) == 0.)           return VOID_VALUE;
    return IS_VOID_VALUE(MainSignals[Ch].Time_pol1) ? VOID_VALUE : MainSignals[Ch].Time_pol1 - StartPMTTime(Ch);
}
float DigDataAnalysis::LeafTime_pol1_ATC(int Ch){
    
    if(ATfunc_pol1_ptr[Ch] == NULL) return VOID_VALUE;
    
    float Time = LeafTime_pol1(Ch);   if(  IS_VOID_VALUE(Time) )  return VOID_VALUE;
    float Amplitude = LeafAmpl(Ch);   if( IS_VOID_VALUE(Amplitude) ) return VOID_VALUE;
    
    
    //float MeanTime = TimesPOL1Ranges[Ch].Mean;
    float CorrFuncVal = ATfunc_pol1_ptr[Ch]->Eval(Amplitude);
    
    //float TimeCorrected = Time - CorrFuncVal + MeanTime;
    float TimeCorrected = Time - CorrFuncVal;
    
    
    return TimeCorrected;
}

//---------------------------------------------------------------------------------------------------
int DigDataAnalysis::AnalyseFile(const char *FileName){
    
    //open TTree file
    if(VERBOSE >= 1)printf("\n\n-------------- DigDataAnalysis -----------------\n\n");
    TFile *DataFile = new TFile(FileName, "UPDATE");
    if(!DataFile->IsOpen()){
        if(VERBOSE > 0) printf("DigDataAnalysis::AnalyseFile::ERROR: Tree data file \"%s\" was NOT opened\n",FileName);
        return 0;
    }
    if(VERBOSE > 0) printf("DigDataAnalysis::AnalyseFile: Tree file \"%s\" was opened analysis\n",FileName);
    
    
    //read and reset variables
    if(VERBOSE > 0)printf("\nReading variabled and setting branches:\n");
    
    RawDataTree_ptr = (TTree*)DataFile->Get("RawDataTree");
    if(RawDataTree_ptr == 0)
    { if(VERBOSE > 0)
            printf("DigDataAnalysis::AnalyseFile::ERROR: RawDataTree from file \"%s\" was NOT loaded\n",FileName);
        return 0;
    }
    
    DataRecipe = (DigDataRecipe*)DataFile->Get("DataRecipe");
    if(DataRecipe == 0)
    { if(VERBOSE > 0)
            printf("DigDataAnalysis::AnalyseFile::ERROR: DataRecipe from file \"%s\" was NOT loaded\n",FileName);
        return 0;
    }
    
    //gDirectory->cd("Rint:/");
    
    //DataRecipe->GetConfiguration()->ChannelConfig[12].IsEnable = 0;
    
    //ProcDataTree_ptr -> Clear();
    //ProcHists_ptr -> Clear();
    //ResuHists_ptr -> Clear();
    
    //  printf("debug: deleting ProcHists\n");
    //  for(Int_t element = 0; element <= ProcHists.GetLast(); element++)
    //      if(ProcHists.At(element)){
    //            printf("debug: deleting %s\n", ProcHists.At(element)->GetName());
    //          delete ProcHists.At(element);
    //      }
    //  ProcHists.Clear();
    
    //  printf("debug: deleting ResuHists\n");
    //  for(Int_t element = 0; element <= ResuHists.GetLast(); element++)
    //      if(ResuHists.At(element)){
    //            printf("debug: deleting %s\n", ResuHists.At(element)->GetName());
    //          delete ResuHists.At(element);
    //      }
    //  ResuHists.Clear();
    
    
    int TreeEntries = RawDataTree_ptr->GetEntries();
    if((EventToStudy > TreeEntries)||(EventToStudy == 0)) EventToStudy = TreeEntries;
    if((EventToAnalyse > TreeEntries)||(EventToAnalyse == 0)) EventToAnalyse = TreeEntries;
    
    //if(VERBOSE >= 2) DataRecipe->PrintOut(VERBOSE);
    
    
    char CHname[MAX_NAME_LENGHT];
    ChConfiguration *CurChConf_ptr;
    
    //adding crosstalk griupes by crtlGrNameMask
    Int_t wordBegin = 0, wordEnd = 0, numWords = 0;
    TString word = "";
    
    //if(!crtlGrNameMask.EndsWith(";")) crtlGrNameMask += ";";
    while((wordEnd+2 < crtlGrNameMask.Sizeof())&&(numWords <MAX_CH)){
        
        for(;( wordEnd+1 < crtlGrNameMask.Sizeof())&&(crtlGrNameMask[wordEnd] != ';');wordEnd++);
        word = crtlGrNameMask(wordBegin,wordEnd);
        
        //printf("debug:%i\nmask :%s\nbegin:%i\nend:%i\nword:%s\n\n",numWords, crtlGrNameMask.Data(),wordBegin,wordEnd,word.Data());
        wordEnd++;
        wordBegin = wordEnd;
        numWords++;
        
        for(int chNameIter = 0; chNameIter < 9; chNameIter++)
            if( 0 <= DataRecipe->FindChannelByName( Form(word.Data(), chNameIter) ) ){
                
                if(!CrtlGroupes[CrtlGrNumber].enabled)
                {
                    CrtlGroupes[CrtlGrNumber].enabled = 1;
                    strcpy(CrtlGroupes[CrtlGrNumber].GroupName,Form( Form("%s_crtl", word.Data()), 0 ) );
                }
                
                strcpy( CrtlGroupes[CrtlGrNumber].CHnames[
                        CrtlGroupes[CrtlGrNumber].CHnumber  ], Form(word.Data(), chNameIter) );
                CrtlGroupes[CrtlGrNumber].CHnumber++;
                if(MAX_CH <= CrtlGroupes[CrtlGrNumber].CHnumber) break;
            }
        
        if(CrtlGroupes[CrtlGrNumber].enabled) CrtlGrNumber++;
        
        
    }
    
    
    if(VERBOSE >= 1){
        printf("EventToAnalyse = %i\n",EventToAnalyse );
        printf("EventToStudy = %i\n", EventToStudy);
        printf("AmplMinRange = %.1f\n", AmplMinRange);
        if(0.1 < AmplRange) printf("AmplFitRange = %.1f\n", AmplRange);
        else printf("No CUT by AmplFitRange\n");
        printf("TimeFitRange = %.1f\n", TimeRange);
        printf("XFitRange = %.1f\n", XRange);
        printf("YFitRange = %.1f\n", YRange);
        
        printf("Start detectors:\n");
        StartDetectors[0].PrintOut();
        StartDetectors[1].PrintOut();
        
        printf("Crosstalk groupes:\n");
        if(CrtlGrNumber == 0)printf("No groupes\n");
        for(int g=0; g<CrtlGrNumber; g++) CrtlGroupes[g].PrintOut();
    }
    
    //set branches:
    TTree *ProcDataTree_ptr = new TTree("ProcDataTree","Analysed data");
    for(int Ch = 0; Ch < MAX_CH; Ch++){
        CurChConf_ptr = DataRecipe->GetConfiguration()->ChannelConfig+Ch;
        strcpy(CHname, CurChConf_ptr->ChName);
        
        if(!CurChConf_ptr->IsEnable) continue;
        
        if(CurChConf_ptr->ChannelSignalType != UNDEFINED)
            RawDataTree_ptr->SetBranchAddress(Form("main_%s",CHname), MainSignals+Ch);
        
        if(CurChConf_ptr->ChannelSignalType == LOGIC){
            ProcDataTree_ptr->Branch(CHname,&(LogicData[Ch]),"Time_pol1/F");
            RawDataTree_ptr->SetBranchAddress(CHname, LogicSignals+Ch);
        }
        
        if(CurChConf_ptr->ChannelSignalType == AMPLITUDE){
            RawDataTree_ptr->SetBranchAddress(CHname, AmplitudeSignals+Ch);
            ProcDataTree_ptr->Branch(CHname,&(AmplData[Ch]),"Amplitude/F:Time_pol1:Time_pol1_ATC:X_range:Y_range");
        }
        
        if(CurChConf_ptr->ChannelSignalType == XY_PANEL_X)
            RawDataTree_ptr->SetBranchAddress(CHname, PanelXSignals+Ch);
        
        if(CurChConf_ptr->ChannelSignalType == XY_PANEL_Y)
            RawDataTree_ptr->SetBranchAddress(CHname, PanelYSignals+Ch);
        
    }
    
    //Crosstalk groupes branches
    for(int Gr=0; Gr<CrtlGrNumber; Gr++){
        ProcDataTree_ptr->Branch(CrtlGroupes[Gr].GroupName,Crtl_Flag+Gr,"Crtl_Flag/I");
    }
    
    //Analysing
    if(VERBOSE > 0)printf("\nAnalysing data:\n");
    for(int Ch = 0; Ch < MAX_CH; Ch++){
        CurChConf_ptr = DataRecipe->GetConfiguration()->ChannelConfig+Ch;
        strcpy(CHname, CurChConf_ptr->ChName);
        
        gProcessState->SetStatus(3, (float)Ch / (float)DataRecipe->GetConfiguration()->NenableCh);
        
        if(!CurChConf_ptr->IsEnable) continue;
        
        if(CurChConf_ptr->ChannelSignalType == AMPLITUDE){
            
            //Timing and ATcorrection for POL1
            GetMeanValForLeaf(AmplsRange+Ch, &DigDataAnalysis::LeafAmpl, Ch, Form("%s_ampl",CHname), 1,AmplMinRange, 3.,AmplRange);
            GetMeanValForLeaf(TimesPOL1Ranges+Ch, &DigDataAnalysis::LeafTime_pol1, Ch, Form("%s_pol1",CHname), 500,-1000.,1.,TimeRange);
            GetMeanValForLeaf(XcoordRange+Ch, &DigDataAnalysis::LeafXcoord, Ch, Form("%s_X",CHname), 1,0.,3.,XRange);
            GetMeanValForLeaf(YcoordRange+Ch, &DigDataAnalysis::LeafYcoord, Ch, Form("%s_Y",CHname), 1,0.,4.,YRange);
            
            if(VERBOSE > 0){printf("Amplitude-Time correction for CH%i [%s] ... ",Ch,CHname);std::cout<<std::flush;}
            MakeATcorrectionPOL1(CHname);if(VERBOSE > 0)printf("done\n");
        }
        
        if(CurChConf_ptr->ChannelSignalType == LOGIC){
            GetMeanValForLeaf(TimesPOL1Ranges+Ch, &DigDataAnalysis::LeafTime_pol1, Ch, Form("%s_pol1",CHname), 500,-1000,.2, TimeRange);
        }
    }
    
    //result hists:
    const int AmplHistsTypes = 2; //number of hists for each channel
    const int LogicHistTypes = 1;
    TH1 *AmplHists_arrptr[MAX_CH * AmplHistsTypes];
    TH1 *LogicHists_arrptr[MAX_CH * LogicHistTypes];
    int Ampl_pointer = 0;
    int Logic_pointer = 0;
    for(int Ch = 0; Ch < MAX_CH; Ch++){
        ChConfiguration *CurCh_ptr = DataRecipe->GetConfiguration()->ChannelConfig+Ch;
        strcpy(CHname, CurCh_ptr->ChName);
        
        if(!CurCh_ptr->IsEnable) continue;
        
        if(CurCh_ptr->ChannelSignalType == AMPLITUDE){
            float MinTime = TimesPOL1Ranges[Ch].MinRange(4.);
            float MaxTime = TimesPOL1Ranges[Ch].MaxRange(4.);
            int Time_bin = ceil(MaxTime-MinTime)*500;
            
            if(0 < Time_bin){
                AmplHists_arrptr[Ampl_pointer] = new TH1F(Form("%s_time_pol1",CHname),Form("Time pol1 %s;time, ns", CHname), Time_bin, MinTime, MaxTime);
                ResuHists.Add(AmplHists_arrptr[Ampl_pointer]); Ampl_pointer++;
                
                AmplHists_arrptr[Ampl_pointer] = new TH1F(Form("%s_time_pol1_atr",CHname),Form("Time pol1 (Time-Ampl corr) %s;time, ns", CHname), Time_bin, MinTime, MaxTime);
                ResuHists.Add(AmplHists_arrptr[Ampl_pointer]); Ampl_pointer++;
            } else {AmplHists_arrptr[Ampl_pointer++] = NULL; AmplHists_arrptr[Ampl_pointer++] = NULL;}
        }
        
        if(CurCh_ptr->ChannelSignalType == LOGIC){
            float MinTime = TimesPOL1Ranges[Ch].MinRange(4.);
            float MaxTime = TimesPOL1Ranges[Ch].MaxRange(4.);
            int Time_bin = ceil(MaxTime-MinTime)*500;
            
            if(0 < Time_bin){
                LogicHists_arrptr[Logic_pointer] = new TH1F(Form("%s_time_pol1",CHname),Form("Time pol1 PMTs%s;time, ns", CHname), Time_bin, MinTime, MaxTime);
                ResuHists.Add(LogicHists_arrptr[Logic_pointer]); Logic_pointer++;
            } else {LogicHists_arrptr[Logic_pointer++] = NULL;}
            
        }
        
    }
    
        
    if(VERBOSE > 0){printf("\nProcessing data:\nin progress ");std::cout<<std::flush;}
    for(int nEvent = 0; nEvent < EventToAnalyse; nEvent++){
        
        gProcessState->SetStatus(4, (float)nEvent / (float)EventToAnalyse);
        
        if(VERBOSE > 0) if( nEvent%20000==0) std::cout<<"."<<std::flush;
        
        RawDataTree_ptr->GetEntry(nEvent);
        
        Ampl_pointer = 0;//for result hists arrays
        Logic_pointer = 0;
        for(int Channel = 0; Channel < MAX_CH; Channel++){
            CurChConf_ptr = DataRecipe->GetConfiguration()->ChannelConfig+Channel;
            if(!CurChConf_ptr->IsEnable) continue;
            
            if(CurChConf_ptr->ChannelSignalType == AMPLITUDE){
                float Amplitude = LeafAmpl(Channel);
                int AmplInRange = (AmplsRange[Channel].InSigmaRange(Amplitude,AmplRange))||(AmplRange < 0.1);
                if( IS_VOID_VALUE(Amplitude) ) AmplInRange = 0;
                AmplData[Channel].Amplitude = (AmplInRange)?Amplitude:VOID_VALUE;
                
                float Time_pol1 = LeafTime_pol1(Channel);
                int TimeInRange = (TimesPOL1Ranges[Channel].InSigmaRange(Time_pol1,TimeRange));
                AmplData[Channel].Time_pol1 = (TimeInRange && AmplInRange)?Time_pol1:VOID_VALUE;
                
                float Time_pol1_ATC = LeafTime_pol1_ATC(Channel);
                AmplData[Channel].Time_pol1_ATC = (TimeInRange && AmplInRange)?Time_pol1_ATC:VOID_VALUE;
                
                float X_range = TMath::Abs( XcoordRange[Channel].Mean - LeafXcoord(Channel) ) / XcoordRange[Channel].Sigma;
                AmplData[Channel].X_range = X_range;
                
                float Y_range = TMath::Abs( YcoordRange[Channel].Mean - LeafYcoord(Channel) ) / YcoordRange[Channel].Sigma;
                AmplData[Channel].Y_range = Y_range;
                
                //filling hists:
                if(TimeInRange && AmplInRange)
                {
                    if(AmplHists_arrptr[Ampl_pointer]) AmplHists_arrptr[Ampl_pointer]->Fill(Time_pol1); Ampl_pointer++;
                    if(AmplHists_arrptr[Ampl_pointer]) AmplHists_arrptr[Ampl_pointer]->Fill(Time_pol1_ATC); Ampl_pointer++;
                }else Ampl_pointer+=AmplHistsTypes;//if no signal in channel pointers must setted to next channel
            }
            
            if(CurChConf_ptr->ChannelSignalType == LOGIC){
                float Time_pol1 = LeafTime_pol1(Channel);
                int TimeInRange = TimesPOL1Ranges[Channel].InSigmaRange(Time_pol1,TimeRange);
                LogicData[Channel].Time_pol1 = (TimeInRange)?Time_pol1:VOID_VALUE;
                
                //filling hists:
                if(TimeInRange)
                {
                    if(LogicHists_arrptr[Logic_pointer]) LogicHists_arrptr[Logic_pointer]->Fill(Time_pol1);Logic_pointer++;
                }else Logic_pointer+=LogicHistTypes;//if no signal in channel pointers must setted to next channel
            }
            
        }
        
        //crosstalk:
        for(int crtlGr = 0; crtlGr<CrtlGrNumber; crtlGr++)
            if(CrtlGroupes[crtlGr].enabled){
                
                int GrFlag = 0;
                for(int crtlCH=0; crtlCH<CrtlGroupes[crtlGr].CHnumber;crtlCH++){
                    int Ch = DataRecipe->FindChannelByName(CrtlGroupes[crtlGr].CHnames[crtlCH]);
                    if( !IS_VOID_VALUE(LeafAmpl(Ch)) ) GrFlag++;
                }
                Crtl_Flag[crtlGr] = GrFlag;
                
            }
        ProcDataTree_ptr->Fill();
    }
    
    //fitting results hists:
    Ampl_pointer = 0;
    Logic_pointer = 0;
    TH1 *currHist;
    TF1 Resolution("resolution","gaus");
    
    for(int Ch = 0; Ch < MAX_CH; Ch++){
        CurChConf_ptr = DataRecipe->GetConfiguration()->ChannelConfig+Ch;
        strcpy(CHname, CurChConf_ptr->ChName);
        
        if(!CurChConf_ptr->IsEnable) continue;
        
        if(CurChConf_ptr->ChannelSignalType == AMPLITUDE)
            for(int Htype=0;Htype<AmplHistsTypes;Htype++){
                currHist = AmplHists_arrptr[Ampl_pointer];Ampl_pointer++;
                if(!currHist) continue;
                if(currHist->Integral() > 0) currHist->Fit(&Resolution,"QW","");
                else if(VERBOSE > 0)printf("DigDataAnalysis::AnalyseFile: ERROR: Events too low for fitting");
                currHist->SetTitle( Form("%s[mean=%.3f, sigma=%.3f]",currHist->GetTitle(),Resolution.GetParameter(1),Resolution.GetParameter(2)) );
            }
        
        if(CurChConf_ptr->ChannelSignalType == LOGIC)
            for(int Htype=0;Htype<LogicHistTypes;Htype++){
                currHist = LogicHists_arrptr[Logic_pointer];Logic_pointer++;
                if(!currHist) continue;
                if(currHist->Integral() > 0) currHist->Fit(&Resolution,"QW");
                else if(VERBOSE > 0)printf("DigDataAnalysis::AnalyseFile: ERROR: Events too low for fitting");
                currHist->SetTitle( Form("%s[mean=%.3f, sigma=%.3f]",currHist->GetTitle(),Resolution.GetParameter(1),Resolution.GetParameter(2)) );
            }
        
    }
    
    
    //writing results in same file
    if(VERBOSE > 0)printf("\nWriting results in file:\n");
    
    
    DataFile->rmdir("AnalysisHists");
    TDirectory *ProcHistDir = DataFile->mkdir("AnalysisHists"); ProcHistDir->cd();
    ProcHists.Write();
    ProcHists.Clear();
    if(VERBOSE > 0)printf("AnalysisHists saved ...\n");
    
    
    DataFile->rmdir("ResultHists");
    TDirectory *ResuHistDir = DataFile->mkdir("ResultHists"); ResuHistDir->cd();
    ResuHists.Write();
    ResuHists.Clear();
    if(VERBOSE > 0)printf("ResultHists saved ...\n");
    
    DataFile->cd();
    ProcDataTree_ptr->Write();
    delete ProcDataTree_ptr;
    if(VERBOSE > 0)printf("ProcDataTree saved ...\n");
    
    //delete created pointers and close opened file
    //printf("debug: deleting ATfunc_pol1_ptr\n");
    for(int ch=0; ch<MAX_CH; ch++)if(ATfunc_pol1_ptr[ch])delete ATfunc_pol1_ptr[ch];
    
    
    DataFile->Close();
    delete DataFile;
    
    
    if(VERBOSE > 0)printf("\nAnalysing finished\n");
    
    return 1;
}

//---------------------------------------------------------------------------------------------------
int DigDataAnalysis::MakeATcorrectionPOL1(char *ChName){
    
    int ChNumber = DataRecipe->FindChannelByName(ChName); if(ChNumber < 0) return 0;
    float *Time_pol1_ptr = &( MainSignals[ChNumber].Time_pol1 );
    float MinAmpl = DataRecipe->GetMainParam(ChNumber)->minAmplitude;
    float MaxAmpl = DataRecipe->GetMainParam(ChNumber)->maxAmplitude;
    //int Ampl_bin = ceil( (MaxAmpl-MinAmpl)*0.4 );
    int Ampl_bin = ceil( (MaxAmpl-MinAmpl)*1. );
    float MinTime = TimesPOL1Ranges[ChNumber].MinRange(TimeRange);
    float MaxTime = TimesPOL1Ranges[ChNumber].MaxRange(TimeRange);
    
    if(MaxTime < MinTime){ if(VERBOSE > 0)printf("DigDataAnalysis::MakeATcorrectionPOL1:ERROR Time range is NOT correct\n"); return 0;}
    
    int Time_bin = ceil(MaxTime-MinTime)*1000;
    
    //if(VERBOSE >= 1) printf("Amplitude-Time Correction for CH%i (%s); Time range: %.2f, %.2f, Mean = %.3f, Sigma = %.3f \n",ChNumber,ChName,MinTime,MaxTime,TimesPOL1Ranges[ChNumber].Mean,TimesPOL1Ranges[ChNumber].Sigma );
    
    //TH2F *AThist = new TH2F( Form("Ampl_TimePol1_%s",ChName), Form("Time vs Amplitude %s;Amplitude, mV; Time, ns",ChName), Ampl_bin, MinAmpl, MaxAmpl,Time_bin,MinTime,MaxTime );
    TH2F *AThist = new TH2F( Form("Ampl_TimePol1_%s",ChName), Form("Time vs Amplitude %s;Amplitude, mV; Time, ns",ChName),
                             Ampl_bin, MinAmpl, MaxAmpl, Time_bin, MinTime - TimesPOL1Ranges[ChNumber].Mean, MaxTime - TimesPOL1Ranges[ChNumber].Mean);
    
    
    for(int Ev = 0; Ev < EventToStudy; Ev++){
        //printf("debug: event %i, event %i\n",Ev, AThist->GetEntries());
        RawDataTree_ptr->GetEntry(Ev);
        if( (*Time_pol1_ptr == 0.) || (StartPMTTime(ChNumber) == 0.)) continue;
        float Time = *Time_pol1_ptr - StartPMTTime(ChNumber);
        if((Ev > 100)&&(AThist->GetEntries() < 0)) break;
        if(MainSignals[ChNumber].IsVoid()) continue;
        
        AThist->Fill( MainSignals[ChNumber].Amplitude, Time - TimesPOL1Ranges[ChNumber].Mean);
    }
    
    if(AThist->GetEntries() < 0){ if(VERBOSE > 0)printf("\nDigDataAnalysis::MakeATcorrectionPOL1:ERROR NO entries in histogram for profile\n"); return 0;}
    
    TProfile *ATprof = AThist->ProfileX( Form("Ampl_TimePol1_%s_prof",ChName) );
    
    if(!ATprof){ if(VERBOSE > 0)printf("\nDigDataAnalysis::MakeATcorrectionPOL1:ERROR Profile NOT created\n"); return 0;}
    if(ATprof->GetEntries() < 0){ if(VERBOSE > 0)printf("\nDigDataAnalysis::MakeATcorrectionPOL1:ERROR NO entries in profile\n"); return 0;}
    
    TF1 *curATf_ptr = new TF1(Form("ATfunction_%s",ChName), "pol6");
    
    ATprof->Fit(curATf_ptr,"QWI","",MinAmpl,MaxAmpl);
    
    ATfunc_pol1_ptr[ChNumber] = curATf_ptr;
    ProcHists.Add( AThist );
    ProcHists.Add( ATprof );
    
    
    
    /*
  TH1 *bestprojection = NULL, *bestassayhist = NULL;
  TF1 *bestfunction = NULL;
  
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","pebr",10,2,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","pebr",10,2,1.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"","pebr",10,2,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","ebr",10,2,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","ebr",10,2,1.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"","ebr",10,2,5.);
  
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","pebr",10,2,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","pebr",10,2,1.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"","pebr",10,2,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","ebr",10,2,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","ebr",10,2,1.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"","ebr",10,2,5.);
  
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","pebr",10,6,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","pebr",10,6,1.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"","pebr",10,6,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","ebr",10,6,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","ebr",10,6,1.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"","ebr",10,6,5.);
  
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","pebr",10,6,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","pebr",10,6,1.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"","pebr",10,6,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","ebr",10,6,5.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"r","ebr",10,6,1.);
  TAcorrection->TryProjFitOption(AThist,&bestprojection,&bestfunction,&bestassayhist,NULL,ChNumber,"","ebr",10,6,5.);
  
  if(bestassayhist) delete bestassayhist;
  ProcHists.Add( AThist);
  ProcHists.Add( bestprojection );
  ProcHists.Add( ATfunc_pol1_ptr[ChNumber] = bestfunction );
     */
    return 1;
}


void DigDataAnalysis::SetAnalyseEv(int Events){
    EventToAnalyse = Events;
}
void DigDataAnalysis::SetStudyEv(int Events){
    EventToStudy = Events;
}
void DigDataAnalysis::SetAmplMin(int AmplMin){
    AmplMinRange = AmplMin;
}
void DigDataAnalysis::SetAmplRange(float AmplRange_){
    AmplRange = AmplRange_;
}
void DigDataAnalysis::SetCRTLgroupNameMask(TString mask){
    crtlGrNameMask = mask;
}
ChannelGroup* DigDataAnalysis::GetCrtlArr(int N){
    CrtlGrNumber = N; return CrtlGroupes;
} //temp, AddCrtlGroup does not work :(

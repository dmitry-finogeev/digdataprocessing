/*
 * DigVariables.cpp
 *
 *  Created on: May 11, 2016
 *      Author: mss
 */

#include "includes/DigVariables.h"

static char ChSignalTypeName[5][MAX_NAME_LENGHT] =
{"UNDEFINED", "LOGIC", "AMPLITUDE", "XY_PANEL_X", "XY_PANEL_Y"};

static char ChPolarityName[2][MAX_NAME_LENGHT] =
{"POSITIVE", "NEGATIVE"};

GlobalProcessStatus::GlobalProcessStatus()
{
    enabled = 0;
    break_flag = 0;
    step = 0;
    percent = 0;
}

GlobalProcessStatus::~GlobalProcessStatus(){
    
}

int GlobalProcessStatus::GetPercent(){
    return floor(100. * percent);
}

void GlobalProcessStatus::SetStatus(int step_, float percent_){
    step = step_;
    percent = percent_;
    if(enabled)  gSystem->ProcessEvents();
    if(break_flag){break_flag = 0; throw 1;}
}

const char* GlobalProcessStatus::GlobalProcessStatus::GetStepName(){
    switch(step){
    case 0: return "...";
    case 1: return "(1/4) Data processing";
    case 2: return "(2/4) Saving data tree";
    case 3: return "(3/4) Analysing processed data";
    case 4: return "(4/4) Saving analysed data";
    }
    return "(0/0) Incorrect step";
}

ChConfiguration::ChConfiguration(){
    Reset();
}

void ChConfiguration::Reset(){
    strcpy(ChName, "NO NAME");
    strcpy(ChComment, "NO COMMENT");
    strcpy(FileName, "");
    IsEnable = 0;
    ChannelSignalType = UNDEFINED;
    ChannelPolarity = POSITIVE;
}

DataConfiguration::DataConfiguration(){
    Reset();
}

void DataConfiguration::Reset(){
    strcpy(DataName, "NO NAME");
    strcpy(RunComment, "NO COMMENT");
    strcpy(DataPath, "");
    NenableCh = 0;
    NactiveCh = 0;
    IsConfigured = 0;
    IsReadHeader = false;
    for(int ch=0; ch<MAX_CH; ch++) ChannelConfig[ch].Reset();
}

ValueRange::ValueRange(){
    Mean = Sigma = MinValue = MaxValue = VOID_VALUE;
}

ValueRange::ValueRange(float Mn, float Sg, float Min, float Max){
    Mean = Mn;
    Sigma = Sg;
    MinValue = Min;
    MaxValue = Max;
}

float ValueRange::MinRange(float nSigma){
    if(IS_VOID_VALUE(Mean)) return VOID_VALUE;
    return ( (Mean - Sigma*nSigma)<MinValue )?MinValue:(Mean - Sigma*nSigma);
}

float ValueRange::MaxRange(float nSigma){
    if(IS_VOID_VALUE(Mean)) return VOID_VALUE;
    return ( (Mean + Sigma*nSigma)>MaxValue )?MaxValue:(Mean + Sigma*nSigma);
}

int ValueRange::InSigmaRange(float Value, float nSigma){
    if(IS_VOID_VALUE(Value)) return 0;
    return (Value > MinRange(nSigma)) &&(Value < MaxRange(nSigma));
}

void ValueRange::PrintOut(){
    printf("[min %.2f, max %.2f](mean %.2f ± sigma %.2f)",MinValue, MaxValue, Mean, Sigma);
}



Fitting_Processing::Fitting_Processing(){
    Reset();
}

void Fitting_Processing::PrintOut(){
    printf("Fitting procedure:   %s\n",FittingON?"ON":"OFF");
    
    if(FittingON){
        printf("Fitting parameter:   %s\n",parameter);
        printf("Minimal amplitude:   %.1f mV\n",minAmplitude);
        printf("Maximal chisquare:   %.1f\n",MaxChisquare);
        printf("Chisquare range:     %.1f\n",ChiRange);
        printf("Sigma Fit Func:      %.1f ps\n",SigmaFitFunc*1000.);
        printf("Border Fit Rej Time: %.1f\n",BorderFitRejTime);
    }
    
}

void Fitting_Processing::Reset(){
    FittingON = 0;
    parameter[0] = '\0';
    //sprintf(parameter,"");
    minAmplitude = 0.;
    MaxChisquare = 0.;
    ChiRange = 0.;
    SigmaFitFunc = 0.;
    BorderFitRejTime = 0.;
}



Main_Processing_Param::Main_Processing_Param(){
    Reset();
}

void Main_Processing_Param::PrintOut(){
    printf("Main proc param for CH %i\n",Channel);
    printf("Minimal amplitude:     %.1f mV\n",minAmplitude);
    printf("Maximal amplitude:     %.1f mV\n",maxAmplitude);
    printf("Minimal ampl Gate:     %.1f ns\n",minAmplGate);
    printf("Maximal ampl Gate:     %.1f ns\n",maxAmplGate);
    printf("Signal point interval: %.1f * Ampl\n",MainSgnPointsInerval);
    printf("Zero level max RMS:    %.1f mV\n",ZeroLevelmaxRMS);
    printf("POL1 parameters:\n"); POL1.PrintOut();
    printf("POL3 parameters:\n"); POL3.PrintOut();
}



void Main_Processing_Param::Reset(){
    Channel = -1;
    minAmplitude = 0.;
    maxAmplitude = 0.;
    minAmplGate = 0.;
    maxAmplGate = 0;
    MainSgnPointsInerval = 0.;
    ZeroLevelmaxRMS = 0.;
    POL1.Reset();
    POL3.Reset();
}



Amplitude_Processing_Param::Amplitude_Processing_Param(){
    Reset();
}

void Amplitude_Processing_Param::PrintOut(){
    printf("Amplitude proc param for CH %i\n",Channel);
    printf("Precise calculation %s\n",isPrecCalc?"ON":"OFF");
    printf("LED treshold:               %.1f mv\n",treshold);
    printf("CFD time shift:            -%.1f ns\n",CFD_dt);
    printf("CFD amplitude gain:         %.1f\n",CFD_fV);
}

void Amplitude_Processing_Param::Reset(){
    Channel = -1; isPrecCalc = 0; treshold = CFD_dt = CFD_fV = 0;
}



DataProcParameters::DataProcParameters(){
    Reset();
}

void DataProcParameters::PrintOut(){
    printf("Amplitude dimension:  %.2f mV/ch\n", AmplitudeDimension);
    printf("Amplitude zero level: %.2f mV\n", AmplitudeZeroLevel);
    printf("Time dimension:       %.2f\n", TimeDimension);
    printf("stat for calc ch conf: %i\n",Statistic);
    printf("Amplitude range:      %.1f * Ampl_sigma\n",AmplRange);
    printf("Aditional points:      %s\n",AddFrontPoints?"ON":"OFF");
    printf("Filter procedure:      %s\n",FilterON?"ON":"OFF");
    if(FilterON) printf("Filter lenght %i, Fs %i, Fx %i\n",Filter_lenght,Filter_Fs,Filter_Fx);
    printf("\n");
    for(int ch = 0; ch<essentialChannels; ch++){
        printf("For channel %s must be: ", essentialChName[ch].Data());
        if(essentialValue[ch] == 0) printf("AMPL ");
        if(essentialValue[ch] == 1) printf("POL1 ");
        if(essentialValue[ch] == 2) printf("POL3 ");
        printf("\n");
    }
    
}

void DataProcParameters::Reset(){
    AmplitudeDimension = 0.;
    AmplitudeZeroLevel = 0.;
    TimeDimension = 0.;
    FilterON = 0;
    Filter_lenght = Filter_Fs = Filter_Fx = 0;
    AddFrontPoints = 0;
    Statistic = 0;
    AmplRange = 0.;
    essentialChannels = 0;
    for(int ch=0; ch<MAX_CH; ch++){
        essentialChName[ch] = "EMPTY";
        essentialValue[ch] = 0;
    }
}



MAIN_signal::MAIN_signal(){
    Reset();
}

void MAIN_signal::Reset(){
    Amplitude = VOID_VALUE;
    Time = VOID_VALUE;
    Time_pol1 = VOID_VALUE;
    Time_pol3 = VOID_VALUE;
    ZeroLevel = VOID_VALUE;
    ZeroLevelRMS = VOID_VALUE;
    time_peak = VOID_VALUE;
    time_begin = VOID_VALUE;
    time_end = VOID_VALUE;
    time_front_end = VOID_VALUE;
    time_back_begin = VOID_VALUE;
    Charge_GATE = VOID_VALUE;
    timestamp =VOID_VALUE;
    timestamp_us = VOID_VALUE;
}

void MAIN_signal::PrintOut(){
    printf("ampl %.1f ",Amplitude);
    printf("time %.1f ",Time);
    printf("pol1 %.1f ",Time_pol1);
    printf("pol3 %.1f ",Time_pol3);
    printf("ZLvl %.1f ",ZeroLevel);
    printf("ZLRMS %.1f ",ZeroLevelRMS);
    printf("t_peak %.1f ",time_peak);
    printf("t_begin %.1f ",time_begin);
    printf("t_end %.1f ",time_end);
    printf("t_f_end %.1f ",time_front_end);
    printf("t_b_begin %.1f ",time_back_begin);
    printf("Charge_GATE %.1f ",Charge_GATE);
    printf("Timestamp %.1f ",timestamp);
    printf("Time us %d ",timestamp_us);
    printf("\n");
}

bool MAIN_signal::IsVoid(){
    return IS_VOID_VALUE(Amplitude)||IS_VOID_VALUE(Time);
}



LOGIC_signal::LOGIC_signal(){
    Reset();
}

void LOGIC_signal::Reset(){
    Level = 0.;
}

bool LOGIC_signal::IsVoid(){
    return IS_VOID_VALUE(Level);
}



AMPLITUDE_signal::AMPLITUDE_signal(){
    Reset();
}

void AMPLITUDE_signal::Reset(){
    mAmplitude = VOID_VALUE;
    pfAmplitude = VOID_VALUE;
    acTime = VOID_VALUE;
    pfTime = VOID_VALUE;
    prec_ZLevel = VOID_VALUE;
    prec_ZLevRMS = VOID_VALUE;
    Charge = VOID_VALUE;
    LEDtime = VOID_VALUE;
    CFDtime = VOID_VALUE;
}

bool AMPLITUDE_signal::IsVoid(){
    return IS_VOID_VALUE(mAmplitude);
}

void AMPLITUDE_signal::PrintOut(){
    printf("mean_ampl %.1f ",mAmplitude);
    printf("fit_ampl %.1f ",pfAmplitude);
    printf("fr_fit_time %.1f ",acTime);
    printf("pk_fit_time %.1f ",pfTime);
    printf("prec_ZLvl %.1f ",prec_ZLevel);
    printf("prec_ZLRMS %.1f ",prec_ZLevRMS);
    printf("charge %.1f ",Charge);
    printf("led_time %.1f ",LEDtime);
    printf("cfd_time %.1f ",CFDtime);
    printf("\n");
}

PANNELX_signal::PANNELX_signal(){
    Reset();
}

void PANNELX_signal::Reset(){
    Level = VOID_VALUE;
    Xch = VOID_VALUE;
    for(int cord = 0; cord < 5; cord++) Xarr[cord] = false;
}

bool PANNELX_signal::IsVoid(){
    return IS_VOID_VALUE(Xch);
}


PANNELY_signal::PANNELY_signal(){
    Reset();
}

void PANNELY_signal::Reset(){
    Level = VOID_VALUE;
    Ych = VOID_VALUE;
    for(int cord = 0; cord < 5; cord++) Yarr[cord] = false;
}

bool PANNELY_signal::IsVoid(){
    return IS_VOID_VALUE(Ych);
}


int AmplitudeData::Check(){//1- event exist
    //return (Amplitude != 0.) && (Time_pol1 != 0.);
    return (!IS_VOID_VALUE(Amplitude))&&(!IS_VOID_VALUE(Time_pol1));
    /*
return (Amplitude == 0.)
    &&(Time_pol1 == 0.)
    &&(Time_pol1_ATC == 0.);
         */
}

int LogicalData::Check(){
    return !IS_VOID_VALUE(Time_pol1);
}


EventSelectorCond::EventSelectorCond(){
    Reset();
}

void EventSelectorCond::Reset(){
    isSelectorON = false;
    isANDch_selection = false;

    for(int ch = 0; ch < MAX_CH; ch++) Channel[ch] = false;
    Channel[0] = true;

    MinValues.Amplitude = VOID_VALUE -1.;
    MinValues.Time = VOID_VALUE -1.;
    MinValues.Time_pol1 = VOID_VALUE -1.;
    MinValues.Time_pol3 = VOID_VALUE -1.;
    MinValues.ZeroLevel = -1000.;
    MinValues.ZeroLevelRMS = VOID_VALUE -1.;
    MinValues.time_peak = VOID_VALUE -1.;
    MinValues.time_begin = VOID_VALUE -1.;
    MinValues.time_end = VOID_VALUE -1.;
    MinValues.time_front_end = VOID_VALUE -1.;
    MinValues.time_back_begin = VOID_VALUE -1.;
    MinValues.Charge_GATE = -10000.;

    MaxValues.Amplitude = 2000.;
    MaxValues.Time = 250.;
    MaxValues.Time_pol1 = 250.;
    MaxValues.Time_pol3 = 250.;
    MaxValues.ZeroLevel = 1000.;
    MaxValues.ZeroLevelRMS = 1000.;
    MaxValues.time_peak = 250.;
    MaxValues.time_begin = 250.;
    MaxValues.time_end = 250.;
    MaxValues.time_front_end = 250.;
    MaxValues.time_back_begin = 250.;
    MaxValues.Charge_GATE = 10000.;
}

void EventSelectorCond::PrintOut(){
    printf("Event Selector is %s\n", isSelectorON?"ON":"OFF");

    printf("Channel \"%s\" selection for:", isANDch_selection?"AND":"OR");
    for(int ch = 0; ch < MAX_CH; ch++) if(Channel[ch]) printf(" %i;", ch);
    printf("\n");

    printf("Low edge values:\n");
    MinValues.PrintOut();
    printf("High edge values:\n");
    MaxValues.PrintOut();
}

bool EventSelectorCond::IsSuit(MAIN_signal signal){

    if(!isSelectorON) return true;
    bool isSuit = true;

    isSuit = isSuit && ( signal.Amplitude > MinValues.Amplitude );
    isSuit = isSuit && ( signal.Amplitude < MaxValues.Amplitude );

    isSuit = isSuit && ( signal.Time > MinValues.Time );
    isSuit = isSuit && ( signal.Time < MaxValues.Time );

    isSuit = isSuit && ( signal.Time_pol1 > MinValues.Time_pol1 );
    isSuit = isSuit && ( signal.Time_pol1 < MaxValues.Time_pol1 );

    isSuit = isSuit && ( signal.Time_pol3 > MinValues.Time_pol3 );
    isSuit = isSuit && ( signal.Time_pol3 < MaxValues.Time_pol3 );

    isSuit = isSuit && ( signal.ZeroLevel > MinValues.ZeroLevel );
    isSuit = isSuit && ( signal.ZeroLevel < MaxValues.ZeroLevel );

    isSuit = isSuit && ( signal.ZeroLevelRMS > MinValues.ZeroLevelRMS );
    isSuit = isSuit && ( signal.ZeroLevelRMS < MaxValues.ZeroLevelRMS );

    isSuit = isSuit && ( signal.time_begin > MinValues.time_begin );
    isSuit = isSuit && ( signal.time_begin < MaxValues.time_begin );

    isSuit = isSuit && ( signal.time_end > MinValues.time_end );
    isSuit = isSuit && ( signal.time_end < MaxValues.time_end );

    isSuit = isSuit && ( signal.time_front_end > MinValues.time_front_end );
    isSuit = isSuit && ( signal.time_front_end < MaxValues.time_front_end );

    isSuit = isSuit && ( signal.time_back_begin > MinValues.time_back_begin );
    isSuit = isSuit && ( signal.time_back_begin < MaxValues.time_back_begin );

    isSuit = isSuit && ( signal.Charge_GATE > MinValues.Charge_GATE );
    isSuit = isSuit && ( signal.Charge_GATE < MaxValues.Charge_GATE );

    return isSuit;
}

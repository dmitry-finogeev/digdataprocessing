#include "includes/DigMonitor.h"

ClassImp(MyMainFrame);
ClassImp(ParametersFrame);

EvSelectotrFrame::EvSelectotrFrame(DigData *Data_, EventSelectorCond *Selector_, const TGWindow *p, UInt_t w, UInt_t h, GlobalProcessStatus *state)
{

    int FrameLenght = 360;

    DigEvData = Data_;
    Selector = Selector_;

    TGVerticalFrame *main_frame = new TGVerticalFrame(this, FrameLenght, 1000); //main area
    AddFrame(main_frame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));


    IsSelectorON = new TGCheckButton(main_frame,"Selector is ON");
    main_frame->AddFrame(IsSelectorON, new TGLayoutHints(kLHintsLeft,1,1,1,1));

    ISselectionAND = new TGCheckButton(main_frame,"AND (signals channels selection AND/OR)");
    main_frame->AddFrame(ISselectionAND, new TGLayoutHints(kLHintsLeft,1,1,1,1));


    LselectCH = new TGListBox(main_frame, MAX_CH); LselectCH->SetMultipleSelections(kTRUE);//list of enabled channels
    main_frame->AddFrame(LselectCH, new TGLayoutHints(kLHintsTop | kLHintsLeft |
                                                      kLHintsExpandX | kLHintsExpandY,
                                                      5, 5, 5, 5));
    LselectCH->Resize(300,100);


    TGLabel *label_Name;
    TGHorizontalFrame *SelParamFr;
    EventSelectorCond defaultSelector;

    //--------------------------------------------------------------
    label_Name = new TGLabel(main_frame,"Amplitude Interval:");
    main_frame->AddFrame(label_Name, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    SelParamFr = new TGHorizontalFrame(main_frame, FrameLenght, 30); //start stop buttons area
    main_frame->AddFrame(SelParamFr, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    int fr_id = 0;

    minAmplitude = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(minAmplitude, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    minAmplitude->SetLimitValues(defaultSelector.MinValues.Amplitude,defaultSelector.MaxValues.Amplitude);

    maxAmplitude = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(maxAmplitude, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    maxAmplitude->SetLimitValues(defaultSelector.MinValues.Amplitude,defaultSelector.MaxValues.Amplitude);
    //--------------------------------------------------------------
    label_Name = new TGLabel(main_frame,"Time Interval:");
    main_frame->AddFrame(label_Name, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    SelParamFr = new TGHorizontalFrame(main_frame, FrameLenght, 30); //start stop buttons area
    main_frame->AddFrame(SelParamFr, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    minTime = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(minTime, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    minTime->SetLimitValues(defaultSelector.MinValues.Time,defaultSelector.MaxValues.Time);

    maxTime = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(maxTime, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    maxTime->SetLimitValues(defaultSelector.MinValues.Time,defaultSelector.MaxValues.Time);
    //--------------------------------------------------------------
    label_Name = new TGLabel(main_frame,"Time pol1 Interval:");
    main_frame->AddFrame(label_Name, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    SelParamFr = new TGHorizontalFrame(main_frame, FrameLenght, 30); //start stop buttons area
    main_frame->AddFrame(SelParamFr, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    minTime_pol1 = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(minTime_pol1, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    minTime_pol1->SetLimitValues(defaultSelector.MinValues.Time_pol1,defaultSelector.MaxValues.Time_pol1);

    maxTime_pol1 = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(maxTime_pol1, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    maxTime_pol1->SetLimitValues(defaultSelector.MinValues.Time_pol1,defaultSelector.MaxValues.Time_pol1);
    //--------------------------------------------------------------
    label_Name = new TGLabel(main_frame,"Time pol3 Interval:");
    main_frame->AddFrame(label_Name, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    SelParamFr = new TGHorizontalFrame(main_frame, FrameLenght, 30); //start stop buttons area
    main_frame->AddFrame(SelParamFr, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    minTime_pol3 = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(minTime_pol3, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    minTime_pol3->SetLimitValues(defaultSelector.MinValues.Time_pol3,defaultSelector.MaxValues.Time_pol3);

    maxTime_pol3 = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(maxTime_pol3, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    maxTime_pol3->SetLimitValues(defaultSelector.MinValues.Time_pol3,defaultSelector.MaxValues.Time_pol3);
    //--------------------------------------------------------------
    label_Name = new TGLabel(main_frame,"Zero_Level Interval:");
    main_frame->AddFrame(label_Name, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    SelParamFr = new TGHorizontalFrame(main_frame, FrameLenght, 30); //start stop buttons area
    main_frame->AddFrame(SelParamFr, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    minZeroLevel = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(minZeroLevel, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    minZeroLevel->SetLimitValues(defaultSelector.MinValues.ZeroLevel,defaultSelector.MaxValues.ZeroLevel);

    maxZeroLevel = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(maxZeroLevel, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    maxZeroLevel->SetLimitValues(defaultSelector.MinValues.ZeroLevel,defaultSelector.MaxValues.ZeroLevel);
    //--------------------------------------------------------------
    label_Name = new TGLabel(main_frame,"Zero_Level RMS Interval:");
    main_frame->AddFrame(label_Name, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    SelParamFr = new TGHorizontalFrame(main_frame, FrameLenght, 30); //start stop buttons area
    main_frame->AddFrame(SelParamFr, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    minZeroLevelRMS = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(minZeroLevelRMS, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    minZeroLevelRMS->SetLimitValues(defaultSelector.MinValues.ZeroLevelRMS,defaultSelector.MaxValues.ZeroLevelRMS);

    maxZeroLevelRMS = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(maxZeroLevelRMS, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    maxZeroLevelRMS->SetLimitValues(defaultSelector.MinValues.ZeroLevelRMS,defaultSelector.MaxValues.ZeroLevelRMS);
    //--------------------------------------------------------------
    label_Name = new TGLabel(main_frame,"Charge_GATE Interval:");
    main_frame->AddFrame(label_Name, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    SelParamFr = new TGHorizontalFrame(main_frame, FrameLenght, 30); //start stop buttons area
    main_frame->AddFrame(SelParamFr, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    minCharge_GATE = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(minCharge_GATE, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    minCharge_GATE->SetLimitValues(defaultSelector.MinValues.Charge_GATE,defaultSelector.MaxValues.Charge_GATE);

    maxCharge_GATE = new TGNumberEntry(SelParamFr, 1,5,fr_id++, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax);
    SelParamFr->AddFrame(maxCharge_GATE, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    maxCharge_GATE->SetLimitValues(defaultSelector.MinValues.Charge_GATE,defaultSelector.MaxValues.Charge_GATE);




    TGHorizontalFrame *ButtonFr = new TGHorizontalFrame(main_frame, FrameLenght, 30); //start stop buttons area
    main_frame->AddFrame(ButtonFr, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    Breset = new TGTextButton(ButtonFr,"RESET");
    Breset->Connect("Clicked()","EvSelectotrFrame",this,"DOreset()");
    ButtonFr->AddFrame(Breset, new TGLayoutHints(kLHintsLeft,1,1,1,1));

    Bapply = new TGTextButton(ButtonFr,"Apply");
    Bapply->Connect("Clicked()","EvSelectotrFrame",this,"DOapply()");
    ButtonFr->AddFrame(Bapply, new TGLayoutHints(kLHintsLeft,1,1,1,1));



    LoadSelection();

    SetWindowName("Event Selector");

    MapSubwindows();

    // Initialize the layout algorithm via Resize()
    Resize(GetDefaultSize());

    // Map main frame
    MapWindow();

}

void EvSelectotrFrame::LoadSelection(){

    int firstActCh = -1;

    LselectCH->RemoveAll();
    ChConfiguration *ChConfig = DigEvData->GetDataRecipe()->GetConfiguration()->ChannelConfig;
    for(int Ch=0; Ch<MAX_CH;Ch++)
        if(ChConfig[Ch].IsEnable){
            LselectCH->AddEntry(Form("[%.2d] %s",Ch,ChConfig[Ch].ChName), Ch);
            if(Selector->Channel[Ch]) LselectCH->Select(Ch);
            if(firstActCh == -1)firstActCh = Ch;
        }

    LselectCH->SetTopEntry(firstActCh);

    IsSelectorON->SetOn( Selector->isSelectorON );
    ISselectionAND->SetOn( Selector->isANDch_selection );

    minAmplitude->SetIntNumber( int( Selector->MinValues.Amplitude ) *10 );
    minTime->SetIntNumber( int( Selector->MinValues.Time ) *10 );
    minTime_pol1->SetIntNumber( int( Selector->MinValues.Time_pol1 ) *10 );
    minTime_pol3->SetIntNumber( int( Selector->MinValues.Time_pol3 ) *10 );
    minZeroLevel->SetIntNumber( int( Selector->MinValues.ZeroLevel ) *10 );
    minZeroLevelRMS->SetIntNumber( int( Selector->MinValues.ZeroLevelRMS ) *10 );
    minCharge_GATE->SetIntNumber( int( Selector->MinValues.Charge_GATE ) *10 );

    maxAmplitude->SetIntNumber( int( Selector->MaxValues.Amplitude ) *10 );
    maxTime->SetIntNumber( int( Selector->MaxValues.Time ) *10 );
    maxTime_pol1->SetIntNumber( int( Selector->MaxValues.Time_pol1 ) *10 );
    maxTime_pol3->SetIntNumber( int( Selector->MaxValues.Time_pol3 ) *10 );
    maxZeroLevel->SetIntNumber( int( Selector->MaxValues.ZeroLevel ) *10 );
    maxZeroLevelRMS->SetIntNumber( int( Selector->MaxValues.ZeroLevelRMS ) *10 );
    maxCharge_GATE->SetIntNumber( int( Selector->MaxValues.Charge_GATE ) *10 );

}

EvSelectotrFrame::~EvSelectotrFrame(){
    Cleanup();
}

void EvSelectotrFrame::CloseWindow(){
    delete this;
}

void EvSelectotrFrame::DOapply(){


    for(int ch=0; ch <MAX_CH; ch++) Selector->Channel[ch] = false;

    TList *ChList = new TList;
    ChList -> Clear(); LselectCH->GetSelectedEntries(ChList);
    for(int nlist=0; nlist<=ChList->GetLast();nlist++){
        int Ch = ( (TGLBEntry*) (ChList->At(nlist)) )->EntryId();
        Selector->Channel[Ch] = true;
    }



    Selector->isSelectorON = IsSelectorON->IsOn();
    Selector->isANDch_selection = ISselectionAND->IsOn();

    Selector->MinValues.Amplitude = minAmplitude->GetNumber();
    Selector->MinValues.Time = minTime->GetNumber();
    Selector->MinValues.Time_pol1 = minTime_pol1->GetNumber();
    Selector->MinValues.Time_pol3 = minTime_pol3->GetNumber();
    Selector->MinValues.ZeroLevel = minZeroLevel->GetNumber();
    Selector->MinValues.ZeroLevelRMS = minZeroLevelRMS->GetNumber();
    Selector->MinValues.Charge_GATE = minCharge_GATE->GetNumber();

    Selector->MaxValues.Amplitude = maxAmplitude->GetNumber();
    Selector->MaxValues.Time = maxTime->GetNumber();
    Selector->MaxValues.Time_pol1 = maxTime_pol1->GetNumber();
    Selector->MaxValues.Time_pol3 = maxTime_pol3->GetNumber();
    Selector->MaxValues.ZeroLevel = maxZeroLevel->GetNumber();
    Selector->MaxValues.ZeroLevelRMS = maxZeroLevelRMS->GetNumber();
    Selector->MaxValues.Charge_GATE = maxCharge_GATE->GetNumber();


    printf("\n ============== SELECTOR ==============");
    Selector->PrintOut();
    printf(   "======================================");

}

void EvSelectotrFrame::DOreset(){
    Selector->Reset();
    LoadSelection();
}




//----------------------------------
void ParametersFrame::DOtimer(){
    
    int NenabledCh = DigEvData->GetDataRecipe()->GetConfiguration()->NenableCh;
    //norming time operation, sec
    float weight1 = 55. * StatProc * NenabledCh/17.;
    float weight2 = 55. * StatSave;
    float weight3 = 20. * StatProc * NenabledCh/17.;
    float weight4 = 1.  * StatSave;
    float summWeght = weight1+weight2+weight3+weight4;
    
    float FullPercent=0;
    
    switch(gProcessState->step){
    case 1: FullPercent = ( weight1 * gProcessState->percent )/summWeght; break;
    case 2: FullPercent = ( weight2 * gProcessState->percent + weight1)/summWeght; break;
    case 3: FullPercent = ( weight3 * gProcessState->percent + weight1+weight2)/summWeght; break;
    case 4: FullPercent = ( weight4 * gProcessState->percent + weight1+weight2+weight3)/summWeght; break;
    }
    
    time_t current_time = time(NULL);
    int proc_sec = difftime(current_time, start_time);
    int time_est = (FullPercent == 0)?0:proc_sec/FullPercent*(1. - FullPercent);
    
    statusLabel->SetText( Form("%s: (%.2d%%)",gProcessState->GetStepName(),gProcessState->GetPercent()) );
    
    ProgressBar->ShowPosition(kTRUE, kFALSE,
                              Form("%s  (%3.0dm %2.0is) [%3.0dm %2.0is]","%.0f\%",proc_sec/60, proc_sec%60, (time_est/60), time_est%60 ));
    ProgressBar->Increment( FullPercent*100. - ProgressBar->GetPosition() );
}

//----------------------------------
void ParametersFrame::DOcalculate(){
    
    if(gProcessState->step != 0){ gProcessState->break_flag=1; return; }
    
    
    if(VERBOSE > 1)printf("Saving data ... \n");
    
    gProcessState->enabled = 1;
    gProcessState->break_flag = 0;
    ProgressBar->Reset();
    
    DigDataRecipe *DataRecipe = DigEvData->GetDataRecipe();
    DigDataAnalysis *Analysis = new DigDataAnalysis(gProcessState);
    
    StatSave = NEstat_s->GetIntNumber();
    StatProc = NEstat_p->GetIntNumber();
    
    DataRecipe->GetProcParam()->Statistic = StatProc;
    //DataRecipe->GetMainDetault()->minAmplitude = NEampl_min->GetNumber();
    
    //DataRecipe->GetAmplitudeDefault()->isPrecCalc = IsAmplitudeCalc->IsOn();
    
    Analysis->SetStudyEv( StatProc );
    //Analysis->SetAmplRange( NEampl_rng->GetNumber() );
    
    Analysis->SetCRTLgroupNameMask( crtlGroupMask->GetText() );
    
    //DataRecipe->BuildParameters();
    //DataRecipe->PrintOut();
    
    //filling start detectors list
    TList *StartDetList = new TList;
    StartDetList ->Clear(); LstartCH->GetSelectedEntries(StartDetList);
    DataRecipe->ClearEssentialChList();
    
    int NumStartDetectors = 0;
    while(StartDetList->At(NumStartDetectors)) NumStartDetectors++;
    
    if(NumStartDetectors < 1){
        statusLabel->SetText("Unspecified start detector");
        if(Analysis) delete Analysis;
        return;
    }
    
    //Setting start detectors
    if(VERBOSE >= 4)printf("\nSetting start detector:\n");
    for(int nlist=0; nlist<NumStartDetectors;nlist++){
        int Ch = ( (TGLBEntry*) (StartDetList->At(nlist)) )->EntryId();
        char StartDetectorName[MAX_NAME_LENGHT];
        strcpy(StartDetectorName, DataRecipe->GetConfiguration()->ChannelConfig[Ch].ChName);
        //printf("debug: trg %i\n",Ch);
        int IsCHpol1 = DataRecipe->GetMainParam(Ch)->POL1.FittingON;
        DataRecipe->AddEssentialChannel(StartDetectorName,IsCHpol1);
        Analysis->AddStartDetector(StartDetectorName, ((Ch < 8)||(Ch == 16)?0:1));
        if(VERBOSE >= 4)printf("%s\n", DataRecipe->GetConfiguration()->ChannelConfig[Ch].ChName);
        
    }
    
    //default selection start GR1
    //    int startGR1pmt1 = DigEvData->GetDataRecipe()->FindChannelByName("T0_C2") > 0;
    //    int startGR1pmt2 = DigEvData->GetDataRecipe()->FindChannelByName("T0_D2") > 0;
    
    
    //    if( 0 <= startGR1pmt1 &&  0 <= startGR1pmt2){
    //        Analysis->AddStartDetector("T0_C2", 1);
    //        Analysis->AddStartDetector("T0_D2", 1);
    //        if(VERBOSE >= 4)printf("T0_C2\n");
    //        if(VERBOSE >= 4)printf("T0_D2\n");
    //    }else{
    //        int startGR1Trg;
    //        if(startGR1Trg = DigEvData->GetDataRecipe()->FindChannelByName("TRG2") > 0){
    //            Analysis->AddStartDetector("TRG2", 1);
    //            if(VERBOSE >= 4)printf("TRG2\n");
    //        }
    //    }
    
    
    
    
    //choose file name
    static TString dir(".");
    const char *filetypes[] = { "ROOT files",    "*.root",
                                "All files",     "*",
                                0,               0};
    TGFileInfo fi;
    fi.fFileTypes = filetypes;
    fi.fIniDir    = StrDup(".");
    new TGFileDialog(gClient->GetRoot(), this, kFDSave, &fi);
    
    dir = fi.fIniDir;

    if(IsDeleteFiles->IsOn()) DataRecipe->DeleteUnusedDataFiles();
    
    if(fi.fFilename != 0){
        char FileName[MAXLINE]; strcpy(FileName, fi.fFilename);
        
        if(gProcessState->step == 0){
            gProcessState->SetStatus(1,0.);
            Bcalculate->SetText(" BREAK ");
        }
        
        Timer->TurnOn();
        Timer->Start(200);
        start_time = time(NULL);
        gSystem->ProcessEvents();
        
        try{
            statusLabel->SetText("saving tree ... ");
            gSystem->ProcessEvents();
            DigEvData->SaveDataAsTree(FileName,StatSave);
            
            statusLabel->SetText("analysing data ... ");
            gSystem->ProcessEvents();
            Analysis->AnalyseFile(FileName);
            
        }
        catch(int a){
            if(a == 1){
                if(VERBOSE > 1)printf("*\n*\n*\ndata process interrupted\n");
                statusLabel->SetText("data process interrupted");
                gSystem->ProcessEvents();
            }
        }
        
        
        Timer->TurnOff();
        Timer->Stop();
        
        ProgressBar->Increment( 100. - ProgressBar->GetPosition() );
        statusLabel->SetText("analysed");
        gSystem->ProcessEvents();
        
        gProcessState->SetStatus(0,0.);
        Bcalculate->SetText("SAVE AS");
        
        delete StartDetList;
        delete Analysis;
        
    }
    
}
//----------------------------------
void ParametersFrame::DOreset(){
    
    int MaxEv = DigEvData->GetEvent(0)->GetTotalEvents();
    int MinEv = DigEvData->GetDataRecipe()->GetProcParam()->Statistic; if(MinEv > MaxEv) MinEv = MaxEv;
    
    //IsAmplitudeCalc->SetOn(0);
    IsDeleteFiles->SetOn(0);
    
    NEstat_p->SetLimitValues(MinEv,MaxEv);
    NEstat_p->SetIntNumber(MinEv);
    NEstat_s->SetLimitValues(MinEv,MaxEv);
    NEstat_s->SetIntNumber(MaxEv);
    
    //    NEampl_min->SetLimitValues(0,1000);
    //    NEampl_min->SetIntNumber(50);
    
    //    NEampl_rng->SetLimitValues(0.,1);
    //    NEampl_rng->SetIntNumber(0);
    
    ProgressBar->Reset();
    
    LstartCH->RemoveAll();
    
    int firstActCh = -1;
    ChConfiguration *ChConfig = DigEvData->GetDataRecipe()->GetConfiguration()->ChannelConfig;
    for(int Ch=0; Ch<MAX_CH;Ch++)
        if(ChConfig[Ch].IsEnable){
            LstartCH->AddEntry(Form("[%.2d] %s",Ch,ChConfig[Ch].ChName), Ch);
            if(firstActCh == -1)firstActCh = Ch;
        }
    
    //default selection
    int PMT10, PMT20, Trg0, PMT11, PMT21, Trg1;
    
    
    if( (PMT10 = DigEvData->GetDataRecipe()->FindChannelByName("T0_C1")) > 0 ) LstartCH->Select(PMT10);
    if( (PMT20 = DigEvData->GetDataRecipe()->FindChannelByName("T0_D1")) > 0 ) LstartCH->Select(PMT20);
    
    if( (PMT11 = DigEvData->GetDataRecipe()->FindChannelByName("T0_C2")) > 0 ) LstartCH->Select(PMT11);
    if( (PMT21 = DigEvData->GetDataRecipe()->FindChannelByName("T0_D2")) > 0 ) LstartCH->Select(PMT21);
    
    if(PMT10 < 0 && PMT20 < 0)
        if((Trg0 = DigEvData->GetDataRecipe()->FindChannelByName("TRG1")) > 0) LstartCH->Select(Trg0);
    
    if(PMT11 < 0 && PMT21 < 0)
        if((Trg1 = DigEvData->GetDataRecipe()->FindChannelByName("TRG2")) > 0) LstartCH->Select(Trg1);
    
    
    
    LstartCH->SetTopEntry(firstActCh);
    LstartCH->Resize(300,100);
    
    if(VERBOSE > 1)printf("Data parameters reseted\n");
    
}

//----------------------------------
ParametersFrame::ParametersFrame(DigData *Data_, const TGWindow *p, UInt_t w, UInt_t h, GlobalProcessStatus *state){
    
    gProcessState = state;
    DigEvData = Data_;
    
    TGVerticalFrame *main_frame = new TGVerticalFrame(this, FrameLenght, 1000); //main area
    AddFrame(main_frame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    FrameLenght = 360;
    
    //timer
    Timer = new TTimer(200,1);
    Timer->Connect("Timeout()","ParametersFrame",this,"DOtimer()");
    gProcessState->enabled = 1;
    
    //statistic:
    TGGroupFrame *FRstatistic = new TGGroupFrame(main_frame, "Statistic",kVerticalFrame);//frame for event stats
    main_frame->AddFrame(FRstatistic, new TGLayoutHints(kLHintsExpandX,2,2,2,2));
    
    TGHorizontalFrame *stat1Frame = new TGHorizontalFrame(FRstatistic, FrameLenght, 200);
    FRstatistic->AddFrame(stat1Frame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    TGLabel *statLabel1 = new TGLabel(stat1Frame, "study: "); statLabel1->SetTextJustify(kTextLeft|kTextBottom);
    stat1Frame->AddFrame(statLabel1, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    NEstat_p = new TGNumberEntry(stat1Frame, 1,0,999,TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax);
    stat1Frame->AddFrame(NEstat_p, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    
    TGHorizontalFrame *stat2Frame = new TGHorizontalFrame(FRstatistic, FrameLenght, 200);
    FRstatistic->AddFrame(stat2Frame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    TGLabel *statLabel2 = new TGLabel(stat2Frame, "process: "); statLabel2->SetTextJustify(kTextLeft|kTextBottom);
    stat2Frame->AddFrame(statLabel2, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    NEstat_s = new TGNumberEntry(stat2Frame, 1,0,999,TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax);
    stat2Frame->AddFrame(NEstat_s, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    //amplitude
    //    TGGroupFrame *FRamplitude = new TGGroupFrame(main_frame, "Amplitude",kVerticalFrame);//frame for amplitude param
    //    main_frame->AddFrame(FRamplitude, new TGLayoutHints(kLHintsExpandX,2,2,2,2));
    
    //    TGHorizontalFrame *ampl1Frame = new TGHorizontalFrame(FRamplitude, FrameLenght, 200);
    //    FRamplitude->AddFrame(ampl1Frame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    //    TGLabel *amplLabel1 = new TGLabel(ampl1Frame, "min, mV: "); amplLabel1->SetTextJustify(kTextLeft|kTextBottom);
    //    ampl1Frame->AddFrame(amplLabel1, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    //    NEampl_min = new TGNumberEntry(ampl1Frame, 1,1,999, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax);
    //    ampl1Frame->AddFrame(NEampl_min, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    
    //    TGHorizontalFrame *ampl2Frame = new TGHorizontalFrame(FRamplitude, FrameLenght, 200);
    //    FRamplitude->AddFrame(ampl2Frame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    //    TGLabel *amplLabel2 = new TGLabel(ampl2Frame, "range, sigma: "); amplLabel2->SetTextJustify(kTextLeft|kTextBottom);
    //    ampl2Frame->AddFrame(amplLabel2, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    //    NEampl_rng = new TGNumberEntry(ampl2Frame, 1,1,999, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax);
    //    ampl2Frame->AddFrame(NEampl_rng, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    //options
    TGGroupFrame *FRoption = new TGGroupFrame(main_frame, "Options",kVerticalFrame);//frame for amplitude param
    main_frame->AddFrame(FRoption, new TGLayoutHints(kLHintsExpandX,2,2,2,2));
    
    TGHorizontalFrame *option1Frame = new TGHorizontalFrame(FRoption, FrameLenght, 200);
    FRoption->AddFrame(option1Frame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    TGLabel *optionLabel1 = new TGLabel(option1Frame, "crtlk group mask: "); optionLabel1->SetTextJustify(kTextLeft|kTextBottom);
    option1Frame->AddFrame(optionLabel1, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    crtlGroupMask = new TGTextEntry(option1Frame, new TGTextBuffer(20)); //current event may be set by user  kSunkenFrame | kDoubleBorder
    option1Frame->AddFrame(crtlGroupMask, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    crtlGroupMask->SetText("5MCP%i;5CFD%i");
    
    //start detector
    TGGroupFrame *FRstartd = new TGGroupFrame(main_frame, "Start detectors",kVerticalFrame);//frame for start detectors list
    main_frame->AddFrame(FRstartd, new TGLayoutHints(kLHintsExpandX,2,2,2,2));
    
    //    TGLabel *startLabel1 = new TGLabel(FRstartd, "       GROUP#1         GROUP#2"); startLabel1->SetTextJustify(kTextLeft|kTextBottom);
    //    FRstartd->AddFrame(startLabel1, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    TGHorizontalFrame *startFrame = new TGHorizontalFrame(FRstartd, FrameLenght, 500);
    FRstartd->AddFrame(startFrame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    
    LstartCH = new TGListBox(startFrame, MAX_CH); LstartCH->SetMultipleSelections(kTRUE);//list of enabled channels
    startFrame->AddFrame(LstartCH, new TGLayoutHints(kLHintsTop | kLHintsLeft |
                                                     kLHintsExpandX | kLHintsExpandY,
                                                     5, 5, 5, 5));
    
    //    LstartCHgr1 = new TGListBox(startFrame, MAX_CH); LstartCHgr1->SetMultipleSelections(kTRUE);//list of enabled channels
    //    startFrame->AddFrame(LstartCHgr1, new TGLayoutHints(kLHintsTop | kLHintsLeft |
    //                                                     kLHintsExpandX | kLHintsExpandY,
    //                                                     5, 5, 5, 5));
    
    
    //status
    TGGroupFrame *FRstatus = new TGGroupFrame(main_frame, "Status", kVerticalFrame);//frame for start detectors list
    main_frame->AddFrame(FRstatus, new TGLayoutHints(kLHintsExpandX,2,2,2,2));
    
    TGVerticalFrame *statusFrame = new TGVerticalFrame(FRstatus, FrameLenght, 500);
    FRstatus->AddFrame(statusFrame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    statusLabel = new TGLabel(statusFrame, "setting parameters ... ");
    statusFrame->AddFrame(statusLabel, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    ProgressBar = new TGHProgressBar(statusFrame, 300);
    ProgressBar->SetRange(0.,100.);
    ProgressBar->ShowPosition();
    statusFrame->AddFrame(ProgressBar, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    //buttons
    TGHorizontalFrame *buttonsFrame = new TGHorizontalFrame(main_frame, FrameLenght, 200);
    main_frame->AddFrame(buttonsFrame, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    Breset = new TGTextButton(buttonsFrame,"RESET");
    Breset->Connect("Clicked()","ParametersFrame",this,"DOreset()");
    buttonsFrame->AddFrame(Breset, new TGLayoutHints(kLHintsLeft,1,1,1,1));
    
    Bcalculate = new TGTextButton(buttonsFrame,"SAVE AS");
    Bcalculate->Connect("Clicked()","ParametersFrame",this,"DOcalculate()");
    buttonsFrame->AddFrame(Bcalculate, new TGLayoutHints(kLHintsLeft,1,1,1,1));
    
    //    IsAmplitudeCalc = new TGCheckButton(buttonsFrame,"Prec calc");
    //    buttonsFrame->AddFrame(IsAmplitudeCalc, new TGLayoutHints(kLHintsLeft,1,1,1,1));
    
    IsDeleteFiles = new TGCheckButton(buttonsFrame,"Dlt unsd data");
    buttonsFrame->AddFrame(IsDeleteFiles, new TGLayoutHints(kLHintsLeft,1,1,1,1));
    
    DOreset();
    
    this->SetIconPixmap("icon.png");
    MapSubwindows();
    
    // Initialize the layout algorithm via Resize()
    this->Resize(250,500);
    
    // Map main frame
    MapWindow();
    this->SetWindowName("Data Parameters");
    this->Connect("CloseWindow()", "MyMainFrame", this, "CloseWindow()");
    
}

ParametersFrame::~ParametersFrame(){
    Cleanup();
}

void ParametersFrame::CloseWindow(){
    delete this;
}


//------------------------------
void MyMainFrame::DOparameters(){
    if(DigEvData == 0)
    {
        if(VERBOSE > 1) printf("ERROR: Data was not opened for calculation\n");
        return;
    }
    
    if(DigEvData->GetDataRecipe()->GetConfiguration()->IsConfigured <= 0)
    {
        if(VERBOSE > 1) printf("ERROR: Data was not opened for calculation\n");
        return;
    }
    
    new ParametersFrame(DigEvData, gClient->GetRoot(), 3000, 400, gProcessState);
    show_ongoing = 0;
}

//------------------------------
void MyMainFrame::DOeventSelector(){

    if(DigEvData == 0)
    {
        if(VERBOSE > 1) printf("ERROR: Data was not opened for calculation\n");
        return;
    }

    if(DigEvData->GetDataRecipe()->GetConfiguration()->IsConfigured <= 0)
    {
        if(VERBOSE > 1) printf("ERROR: Data was not opened for calculation\n");
        return;
    }

    new EvSelectotrFrame(DigEvData, &Selector_cond, gClient->GetRoot(), 3000, 400, gProcessState);

}

//------------------------------
void MyMainFrame::DOopen(){
    
    static TString dir(".");
    const char *filetypes[] = { "Config Files",   "DataConfig.txt",
                                "Text files",     "*.[tT][xX][tT]",
                                "All files",      "*",
                                0,               0 };
    
    TGFileInfo *fi = new TGFileInfo;
    fi->fFileTypes = filetypes;
    fi->fIniDir    = StrDup(".");
    TGFileDialog *FileWin = new TGFileDialog(gClient->GetRoot(), this, kFDOpen, fi);
    
    dir = fi->fIniDir;
    if(fi->fFilename != 0){
        SetFileName(fi->fFilename);
        LoadFiles();
    }
    
    //FileWin->Cleanup();
    //delete FileWin;
    delete fi;
    
    
    
}

//------------------------------
void MyMainFrame::ReDivideCanvas(){
    
    AmplCanv = dynamic_cast<TCanvas*>( gDirectory->FindObject( "AmplCanv") );
    if(AmplCanv){ delete AmplCanv; AmplCanv = NULL; }
    AmplHistsArr.Clear();

    ChList->Clear(); fChannelList->GetSelectedEntries(ChList);
    
    //num_ch_to_show = ChList->GetEntries();
    
    
    num_ch_to_show =0;
    while(ChList->At(num_ch_to_show)) num_ch_to_show++;
    
    TCanvas *ShCanvas = fEcanvas->GetCanvas();
    ShCanvas->Clear();
    
    int nlines, ncolumns;
    if(num_ch_to_show > 0){
        ncolumns = ceil(TMath::Sqrt( num_ch_to_show ));
        nlines = ceil(num_ch_to_show/(float)ncolumns);
        
        ShCanvas->Divide(nlines,ncolumns);
    }
    if(num_ch_to_show == 1)
    {
        ShCanvas->cd(1)->SetGridx();
        ShCanvas->cd(1)->SetGridy();
    }
    
    ShCanvas->Update();
    //ChList->ls();
    //printf("%d [%d, %d]\n",num_ch_to_show,nlines, ncolumns);
    
}
//------------------------------
void MyMainFrame::LoadFiles(){
    if(VERBOSE>0)printf("DigMonitor::LoadFiles: Loading file: %s\n", FileName.Data());
    char fname[MAXLINE]; strcpy(fname,FileName.Data());
    
    if(DigEvData) delete DigEvData;
    DigEvData = new DigData(gProcessState);
    
    if( DigEvData->OpenData(fname) == 1 )
    {
        SetWindowName( Form("Digitizer events monitoring/%s", DigEvData->GetDataRecipe()->GetConfiguration()->DataName)  );

        InfoLabel->SetText(Form("%s; \"%s\"",DigEvData->GetDataRecipe()->GetConfiguration()->DataName,DigEvData->GetDataRecipe()->GetConfiguration()->RunComment));
        DigEvData->GetDataRecipe()->PrintOut();
        TotalEvents = DigEvData->GetEvent(0)->GetTotalEvents();
        
        CurEvent = 0; show_ongoing = 1;
        EvLabel1->SetText(Form("Total: %14d", TotalEvents));
        EvEdit->SetText(Form("%d",CurEvent));
        
        fChannelList->RemoveAll();
        //fChannelList->Layout();
        
        ChConfiguration *ChConfig = DigEvData->GetDataRecipe()->GetConfiguration()->ChannelConfig;
        for(int Ch=0; Ch<MAX_CH;Ch++)
            if(ChConfig[Ch].IsEnable){
                fChannelList->AddEntry(Form("[%.2d] %s",Ch,ChConfig[Ch].ChName), Ch);
            }
        
        fChannelList->MapSubwindows();
        fChannelList->Layout();
        fChannelList->Resize(200,300);
        
    }
    
}
//------------------------------
void MyMainFrame::ShowEvLoop(int EventsToShow){
    
    TCanvas *ShCanvas = fEcanvas->GetCanvas();
    
    CurEvent = atoi( EvEdit->GetText() );
    
    DigEvent *CurEvent_ptr;
    if(DigEvData->GetDataRecipe()->GetConfiguration()->IsConfigured < 3)
        show_ongoing = 0;
    
    TGraph *currGraph = 0;
    MAIN_signal *SignalParam = 0;
    
    const Int_t nMarkers = 8;
    Float_t x[nMarkers] = {0.};
    Float_t y[nMarkers] = {0.};
    
    int ShowedEvent = 0;
    while(show_ongoing){
        TotalEvents = DigEvData->GetEvent(0)->GetTotalEvents();
        EvLabel1->SetText(Form("Total: %14d", TotalEvents));
        BeventSelector->SetTextColor( Selector_cond.isSelectorON ? 0 : kBlack );
        
        
        if(CurEvent < TotalEvents-1){ //if new events exist
            
            CurEvent_ptr = DigEvData->GetEvent(CurEvent);
            if(IsInspect->IsOn() || Selector_cond.isSelectorON) CurEvent_ptr->CalculateEvent();

            if(Selector_cond.isSelectorON)
                if(!CurEvent_ptr->IsSelectorSuit(Selector_cond)){CurEvent++; continue;}


            for(int nlist=0; nlist<num_ch_to_show;nlist++){
                int Ch = ( (TGLBEntry*) (ChList->At(nlist)) )->EntryId();
                ShCanvas->cd(nlist+1);
                
                currGraph = CurEvent_ptr->GetChannel(Ch)->GetSignalGraph();

                //                DigEvData->GetDataRecipe()->GetMainParam(Ch)->minAmplGate = minAmplGateN->GetNumber();
                //                DigEvData->GetDataRecipe()->GetMainParam(Ch)->maxAmplGate = maxAmplGateN->GetNumber();

                currGraph->Draw("ACP");
                

                if(IsInspect->IsOn())
                {
                    //Bool_t is_prec_calc = DigEvData->GetDataRecipe()->GetAmplParam(Ch)->isPrecCalc;
                    //DigEvData->GetDataRecipe()->GetAmplParam(Ch)->isPrecCalc = 1;

                    
                    CurEvent_ptr->GetChannel(Ch)->GetSignal(&SignalParam);
                    if(AmplHistsArr.At(nlist))
                        ((TH1F*)AmplHistsArr.At(nlist))->Fill(SignalParam->Amplitude);


                    x[0] = SignalParam->time_begin;
                    x[1] = SignalParam->time_front_end;
                    x[2] = SignalParam->time_peak;
                    x[3] = SignalParam->time_back_begin;
                    x[4] = SignalParam->time_end;
                    x[5] = SignalParam->time_begin-10.; //zero level
                    x[6] = DigEvData->GetDataRecipe()->GetMainParam(Ch)->minAmplGate;
                    x[7] = DigEvData->GetDataRecipe()->GetMainParam(Ch)->maxAmplGate;

                    
                    y[0] = currGraph->Eval(x[0]);
                    y[1] = currGraph->Eval(x[1]);
                    y[2] = currGraph->Eval(x[2]);
                    y[3] = currGraph->Eval(x[3]);
                    y[4] = currGraph->Eval(x[4]);
                    y[5] = SignalParam->ZeroLevel;
                    y[6] = SignalParam->ZeroLevel+100.;
                    y[7] = SignalParam->ZeroLevel+100.;

                    SignalMarkers->Draw();
                    
                    SignalMarkers->SetPolyMarker(nMarkers, x, y);
                    
                    if(!IS_VOID_VALUE(SignalParam->Time_pol1))
                        if(CurEvent_ptr->GetChannel(Ch)->GetSignalFitFunc_pol1())
                        {
                            CurEvent_ptr->GetChannel(Ch)->GetSignalFitFunc_pol1()->Draw("same");
                        }
                    
                    DigSignalAMPLITUDE *amplSignal = dynamic_cast<DigSignalAMPLITUDE*>(CurEvent_ptr->GetChannel(Ch));
                    if(amplSignal)
                    {
                        amplSignal->GetZLevel_fit()->SetLineColor(kGreen);
                        amplSignal->GetZLevel_fit()->Draw("same");
                        
                        amplSignal->GetPeak_fit()->SetLineColor(kBlue);
                        amplSignal->GetPeak_fit()->Draw("same");
                    }
                    
                    InfoLabel->SetText(Form("CH[%i]: Time %.2f, Time_pol1 %.2f, Chi %.1f, Ampl %.1f, Zero Level %.2f, tm_fr_beg %.2f, tm_fr_end %.2f, tm_bk_beg %.2f, tm_bk_end %.2f, tm_ampl %.2f, Charge_GATE %.2f",
                                            Ch, SignalParam->Time, SignalParam->Time_pol1, CurEvent_ptr->GetChannel(Ch)->GetSignalFitFunc_pol1()->GetChisquare(),
                                            SignalParam->Amplitude, SignalParam->ZeroLevel, x[0], x[1], x[3], x[4], x[2], SignalParam->Charge_GATE));
                    
                    //DigEvData->GetDataRecipe()->GetAmplParam(Ch)->isPrecCalc = is_prec_calc;
                    
                    if(VERBOSE >= 1)
                    {
                        printf(" --- CH %i;  EVENT %i; ---\n", Ch, CurEvent);
                        SignalParam->PrintOut();
                        if(amplSignal){AMPLITUDE_signal *ampl_s; amplSignal->GetSignal(&ampl_s); ampl_s->PrintOut();}
                        printf("--------------------------------\n\n");
                    }
                } //is inspect is on
                else
                {
                    InfoLabel->SetText(Form("%s; \"%s\"",DigEvData->GetDataRecipe()->GetConfiguration()->DataName,DigEvData->GetDataRecipe()->GetConfiguration()->RunComment));
                }

                ShCanvas->Modified();
                ShCanvas->Update();

            } //channels loop
            CurEvent++;
            ShowedEvent++;
            //if(AmplCanv){ AmplCanv->Modified();  AmplCanv->Update(); }
        }//if new events exist
        
        
        EvEdit->SetText(Form("%d",CurEvent));
        
        gSystem->ProcessEvents();
        if(  (ShowedEvent >= EventsToShow) && (EventsToShow != 0)  )show_ongoing = 0;
        if(!show_ongoing) break;
        gSystem->Sleep(show_speed);
        
        
        
        gSystem->ProcessEvents();
        
    }
    
}
//------------------------------
void MyMainFrame::CloseWindow()
{
    if(VERBOSE>0)printf("DigMonitor::CloseWindow: Closing window ...\n");
    show_ongoing = 0;
    if(DigEvData) delete DigEvData;
    if(ChList) delete ChList;
    if(SignalMarkers) delete SignalMarkers;
    
    gROOT -> CloseFiles();
    gDirectory -> Clear();
    
    printf("debug: END\n");
    
    gApplication->Terminate(0);
}
//------------------------------
MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, GlobalProcessStatus *state) :
    TGMainFrame(p, w, h)
{
    gProcessState = state;
    
    int panel_width = 220;
    TGHorizontalFrame *panel_wspace = new TGHorizontalFrame(this, 1000, 1000); //main area
    AddFrame(panel_wspace, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    TGVerticalFrame  *panel = new TGVerticalFrame (panel_wspace, panel_width, 100, kFixedWidth); //parameters panel
    panel_wspace->AddFrame(panel, new TGLayoutHints(kLHintsExpandY, 2, 2, 2, 2));
    
    TGVerticalFrame *wspace = new TGVerticalFrame(panel_wspace, 400, 400); //space for drawing
    panel_wspace->AddFrame(wspace, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY, 2, 2, 2, 2));
    
    InfoLabel = new TGLabel(wspace,"Data not loaded");
    wspace->AddFrame(InfoLabel, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    InfoLabel->ChangeOptions(InfoLabel->GetOptions() | kFixedSize);
    InfoLabel->Resize(350, 40);
    
    fEcanvas = new TRootEmbeddedCanvas("Ecanvas",wspace,500,500);// Create canvas widget for graphics
    wspace->AddFrame(fEcanvas, new TGLayoutHints(kLHintsExpandX| kLHintsExpandY,2,2,2,2));
    
    TGGroupFrame *EventStatf = new TGGroupFrame(panel, "Event statistic",kVerticalFrame);//frame for event stats
    panel->AddFrame(EventStatf, new TGLayoutHints(kLHintsExpandX,2,2,2,2));
    
    EvLabel1 = new TGLabel(EventStatf, "EMPTY"); EvLabel1->SetTextJustify(kTextLeft|kTextBottom);//total events
    EventStatf->AddFrame(EvLabel1, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    
    TGHorizontalFrame *panel_curev = new TGHorizontalFrame(EventStatf, 100, 30); //horisontal space fo current event
    EventStatf->AddFrame(panel_curev, new TGLayoutHints(kLHintsExpandX,2,2,2,2));
    
    EvLabel2 = new TGLabel(panel_curev, "Current:"); EvLabel2->SetTextJustify(kTextLeft|kTextBottom); //"current"
    panel_curev->AddFrame(EvLabel2, new TGLayoutHints(kLHintsExpandX,0,0,2,2));
    
    EvEdit = new TGTextEntry(panel_curev, new TGTextBuffer(10)); //current event may be set by user  kSunkenFrame | kDoubleBorder
    panel_curev->AddFrame(EvEdit, new TGLayoutHints(kLHintsLeft,0,0,2,2));
    
    TGHorizontalFrame *ssbuttons = new TGHorizontalFrame(EventStatf, panel_width, 30); //start stop buttons area
    EventStatf->AddFrame(ssbuttons, new TGLayoutHints(kLHintsExpandX,0,0,0,0));
    
    Bstart = new TGTextButton(ssbuttons,"START");
    Bstart->Connect("Clicked()","MyMainFrame",this,"DOstart()");
    ssbuttons->AddFrame(Bstart, new TGLayoutHints(kLHintsLeft,1,1,1,1));
    
    Bstop = new TGTextButton(ssbuttons,"STOP");
    Bstop->Connect("Clicked()","MyMainFrame",this,"DOstop()");
    ssbuttons->AddFrame(Bstop, new TGLayoutHints(kLHintsLeft,1,1,1,1));
    
    Bnext = new TGTextButton(ssbuttons,"NEXT");
    Bnext->Connect("Clicked()","MyMainFrame",this,"DOnext()");
    ssbuttons->AddFrame(Bnext, new TGLayoutHints(kLHintsLeft,1,1,1,1));
    
    Bstartfb = new TGTextButton(ssbuttons,"CONTINUE");
    Bstartfb->Connect("Clicked()","MyMainFrame",this,"DOcontinue()");
    ssbuttons->AddFrame(Bstartfb, new TGLayoutHints(kLHintsLeft,1,1,1,1));
    

    //parameters
    TGGroupFrame *Parampanel = new TGGroupFrame(panel, "Parameters",kVerticalFrame);//frame for parameters
    panel->AddFrame(Parampanel, new TGLayoutHints(kLHintsExpandX,2,2,2,2));

    TGLabel *label_Gate = new TGLabel(Parampanel,"Signal Time GATE, ns:");
    Parampanel->AddFrame(label_Gate, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    TGHorizontalFrame *samplgate = new TGHorizontalFrame(Parampanel, panel_width, 30); //start stop buttons area
    Parampanel->AddFrame(samplgate, new TGLayoutHints(kLHintsExpandX,0,0,0,0));


    minAmplGateN = new TGNumberEntry(samplgate, 1,1,999, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax);
    samplgate->AddFrame(minAmplGateN, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    minAmplGateN->SetLimitValues(0,2000);
    minAmplGateN->SetIntNumber(100);

    maxAmplGateN = new TGNumberEntry(samplgate, 1,1,999, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax);
    samplgate->AddFrame(maxAmplGateN, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    maxAmplGateN->SetLimitValues(0,2000);
    maxAmplGateN->SetIntNumber(1800);

    TGLabel *label_Ampl = new TGLabel(Parampanel,"Amplitude: min, nsigma");
    Parampanel->AddFrame(label_Ampl, new TGLayoutHints(kLHintsExpandX,0,2,2,2));

    TGHorizontalFrame *amplpar = new TGHorizontalFrame(Parampanel, panel_width, 30); //start stop buttons area
    Parampanel->AddFrame(amplpar, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    NEampl_min = new TGNumberEntry(amplpar, 1,1,999, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax);
    amplpar->AddFrame(NEampl_min, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    NEampl_min->SetLimitValues(0,1000);
    NEampl_min->SetIntNumber(50);

    NEampl_rng = new TGNumberEntry(amplpar, 1,1,999, TGNumberFormat::kNESRealOne, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax);
    amplpar->AddFrame(NEampl_rng, new TGLayoutHints(kLHintsExpandX,0,2,2,2));
    NEampl_rng->SetLimitValues(0.,1);
    NEampl_rng->SetIntNumber(0);



    TGHorizontalFrame *checkpar = new TGHorizontalFrame(Parampanel, panel_width, 30); //start stop buttons area
    Parampanel->AddFrame(checkpar, new TGLayoutHints(kLHintsExpandX,0,0,0,0));

    IsPOL1 = new TGCheckButton(checkpar,"POL1");
    checkpar->AddFrame(IsPOL1, new TGLayoutHints(kLHintsLeft,1,1,1,1));

    IsPOL3 = new TGCheckButton(checkpar,"POL3");
    checkpar->AddFrame(IsPOL3, new TGLayoutHints(kLHintsLeft,1,1,1,1));

    IsAmplitudeCalc = new TGCheckButton(checkpar,"PC");
    checkpar->AddFrame(IsAmplitudeCalc, new TGLayoutHints(kLHintsLeft,1,1,1,1));

    IsInspect = new TGCheckButton(checkpar, "INSP"); //current event may be set by user  kSunkenFrame | kDoubleBorder
    checkpar->AddFrame(IsInspect, new TGLayoutHints(kLHintsLeft,0,0,2,2));


    TGHorizontalFrame *btpar = new TGHorizontalFrame(Parampanel, panel_width, 30); //start stop buttons area
    Parampanel->AddFrame(btpar, new TGLayoutHints(kLHintsExpandX,0,0,0,0));


    Bparall = new TGTextButton(btpar,"To ALL");
    Bparall->Connect("Clicked()","MyMainFrame",this,"DOparall()");
    btpar->AddFrame(Bparall, new TGLayoutHints(kLHintsLeft,1,1,1,1));

    Bparsel = new TGTextButton(btpar,"To SEL");
    Bparsel->Connect("Clicked()","MyMainFrame",this,"DOparsel()");
    btpar->AddFrame(Bparsel, new TGLayoutHints(kLHintsLeft,1,1,1,1));

    Bparpr = new TGTextButton(btpar,"PRNT");
    Bparpr->Connect("Clicked()","MyMainFrame",this,"DOparpr()");
    btpar->AddFrame(Bparpr, new TGLayoutHints(kLHintsLeft,1,1,1,1));

    BeventSelector = new TGTextButton(btpar,"EV_SEL");
    BeventSelector->Connect("Clicked()","MyMainFrame",this,"DOeventSelector()");
    btpar->AddFrame(BeventSelector, new TGLayoutHints(kLHintsLeft,1,1,1,1));



    fChannelList = new TGListBox(panel); fChannelList->SetMultipleSelections(kTRUE);//list of enabled channels
    fChannelList->Connect("SelectionChanged()", "MyMainFrame", this, "ReDivideCanvas()");
    panel->AddFrame(fChannelList, new TGLayoutHints(kLHintsTop | kLHintsLeft |
                                                    kLHintsExpandX | kLHintsExpandY,
                                                    5, 5, 5, 5));
    
    Bopen = new TGTextButton(panel,"OPEN DATA");
    Bopen->Connect("Clicked()","MyMainFrame",this,"DOopen()");
    panel->AddFrame(Bopen, new TGLayoutHints(kLHintsExpandX,5,5,5,5));
    
    Bampldraw = new TGTextButton(panel,"AMPL SPECTRA(last 5000ev)");
    Bampldraw->Connect("Clicked()","MyMainFrame",this,"DOampldraw()");
    panel->AddFrame(Bampldraw, new TGLayoutHints(kLHintsExpandX,5,5,5,5));


    Bparameters = new TGTextButton(panel,"CALCULATE DATA");
    Bparameters->Connect("Clicked()","MyMainFrame",this,"DOparameters()");
    panel->AddFrame(Bparameters, new TGLayoutHints(kLHintsExpandX,5,5,5,5));
    
    Bbrowser = new TGTextButton(panel,"OPEN ROOT Browser");
    Bbrowser->Connect("Clicked()","MyMainFrame",this,"DObrowser()");
    panel->AddFrame(Bbrowser, new TGLayoutHints(kLHintsExpandX,5,5,5,5));
    
    this->SetIconPixmap("icon.png");
    this->Connect("CloseWindow()", "MyMainFrame", this, "CloseWindow()");
    SetWindowName("Digitizer events monitoring");
    
    // Set a name to the main frame
    
    MapSubwindows();
    
    // Initialize the layout algorithm via Resize()
    Resize(GetDefaultSize());
    
    // Map main frame
    MapWindow();
    
    AmplCanv = NULL;
    AmplHistsArr.SetOwner();


    show_speed = 0;
    DigEvData = new DigData(gProcessState);
    DigEvData = NULL;
    ChList = new TList;
    
    SignalMarkers = new TPolyMarker();
    SignalMarkers->SetMarkerStyle(3);
    SignalMarkers->SetMarkerSize(1);
    SignalMarkers->SetMarkerColor(2);
    
    
    ReDivideCanvas();

}

MyMainFrame::~MyMainFrame(){
    if(AmplCanv) delete AmplCanv;
    Cleanup();
    DeleteWindow();
}

void MyMainFrame::DOstart(){
    show_ongoing = 1;
    ShowEvLoop(0);
}
void MyMainFrame::DOstop(){
    show_ongoing = 0;
}
void MyMainFrame::DOnext(){
    show_ongoing = 1;
    ShowEvLoop(1);
}
void MyMainFrame::DOcontinue(){
    EvEdit->SetText(Form("%d",TotalEvents));
    CurEvent = TotalEvents; DOstart();
}
void MyMainFrame::DOparall(){
    if(!DigEvData){printf("MyMainFrame::DOparall() Data was NOT loaded !!!\n"); return;}
    DigEvData->GetDataRecipe()->GetProcParam()->AmplRange =  NEampl_rng->GetNumber();

    DigEvData->GetDataRecipe()->GetMainDetault()->minAmplGate = minAmplGateN->GetNumber();
    DigEvData->GetDataRecipe()->GetMainDetault()->maxAmplGate = maxAmplGateN->GetNumber();
    DigEvData->GetDataRecipe()->GetMainDetault()->minAmplitude = NEampl_min->GetNumber();

    DigEvData->GetDataRecipe()->GetMainDetault()->POL1.FittingON = IsPOL1->IsOn();
    DigEvData->GetDataRecipe()->GetMainDetault()->POL3.FittingON = IsPOL3->IsOn();
    DigEvData->GetDataRecipe()->GetAmplitudeDefault()->isPrecCalc = IsAmplitudeCalc->IsOn();

    DigEvData->GetDataRecipe()->BuildParameters();

}
void MyMainFrame::DOparsel(){
    if(!DigEvData){printf("MyMainFrame::DOparpr() Data was NOT loaded !!!\n"); return;}

    DigEvData->GetDataRecipe()->GetProcParam()->AmplRange =  NEampl_rng->GetNumber();

    for(int nlist=0; nlist<num_ch_to_show;nlist++){
        int Ch = ( (TGLBEntry*) (ChList->At(nlist)) )->EntryId();

        DigEvData->GetDataRecipe()->GetMainParam(Ch)->minAmplGate = minAmplGateN->GetNumber();
        DigEvData->GetDataRecipe()->GetMainParam(Ch)->maxAmplGate = maxAmplGateN->GetNumber();
        DigEvData->GetDataRecipe()->GetMainParam(Ch)->minAmplitude = NEampl_min->GetNumber();

        DigEvData->GetDataRecipe()->GetMainParam(Ch)->POL1.FittingON = IsPOL1->IsOn();
        DigEvData->GetDataRecipe()->GetMainParam(Ch)->POL3.FittingON = IsPOL3->IsOn();

        DigEvData->GetDataRecipe()->GetAmplParam(Ch)->isPrecCalc = IsAmplitudeCalc->IsOn();
    }
}
void MyMainFrame::DOparpr(){

    if(!DigEvData){printf("MyMainFrame::DOparpr() Data was NOT loaded !!!\n"); return;}
    DigEvData->GetDataRecipe()->PrintOut();
}
void MyMainFrame::DObrowser(){
    new TBrowser();
}

void MyMainFrame::DOampldraw(){

    AmplCanv = dynamic_cast<TCanvas*>( gDirectory->FindObject( "AmplCanv") );
    if(AmplCanv){ delete AmplCanv; AmplCanv = NULL; }
    AmplHistsArr.Clear();

    AmplCanv = new TCanvas("AmplCanv", "Amplitude Canvas");

    AmplCanv->DivideSquare (num_ch_to_show);

    //creating hisogramms
    TH1 *curr_hist;
    for(int nlist=0; nlist<num_ch_to_show;nlist++){
        int Ch = ( (TGLBEntry*) (ChList->At(nlist)) )->EntryId();
        curr_hist = new TH1F(Form("AmplCH%i", Ch),
                             Form("Amplitude CH%i [%s];mV", Ch, DigEvData->GetDataRecipe()->GetConfiguration()->ChannelConfig[Ch].ChName),
                             DigEvData->GetDataRecipe()->GetMainParam(Ch)->maxAmplitude - DigEvData->GetDataRecipe()->GetMainParam(Ch)->minAmplitude,
                             DigEvData->GetDataRecipe()->GetMainParam(Ch)->minAmplitude,
                             DigEvData->GetDataRecipe()->GetMainParam(Ch)->maxAmplitude);
        AmplHistsArr.Add( curr_hist );
        AmplCanv->cd(nlist+1);
        curr_hist->Draw();
    }

    //plotting hists
    DigEvent *currEvent;
    float currAmpl;
    int nEvents = DigEvData->GetEvent(0)->GetTotalEvents();
    for(int Event = (nEvents <5000)?0:nEvents-5000; Event <= nEvents; Event++){

        currEvent = DigEvData->GetEvent(Event);
        for(int nlist=0; nlist<num_ch_to_show;nlist++){
            int Ch = ( (TGLBEntry*) (ChList->At(nlist)) )->EntryId();
            currEvent->GetChannel(Ch)->FindSignalAmplitude(&currAmpl);
            ((TH1F*)AmplHistsArr.At(nlist))->Fill(currAmpl);
        }

        //AmplCanv->Update();
    }
    AmplCanv->Update();


}

void MyMainFrame::SetFileName(TString FName){
    FileName = FName;
}
void MyMainFrame::OpenFile(TString filename){
    SetFileName(filename);
    LoadFiles();
}


//------------------------------
void DigMonitor(GlobalProcessStatus *gProcessState, TString filename)
{
    // Popup the GUI...
    MyMainFrame *Frame = new MyMainFrame(gClient->GetRoot(), 1000, 1000, gProcessState);
    if(filename != "") Frame->OpenFile(filename);
}







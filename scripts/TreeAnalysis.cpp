#include "includes/DigData.h"
#include "includes/DigDataAnalysis.h"
#include "includes/utilities.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include <TStyle.h>
#include <TFile.h>
#include <TString.h>

int TreeAnalysis(){

	TString Path("/media/data1/Data_Oct_2015/10102015_2000_RUN34_5MCP_1439_CFD_tr10mV/DATA_sel100mV.root");

	TFile *File = new TFile( Path.Data() );
	TTree *Tree = (TTree*)File->Get("ProcDataTree");


	const int CHtoShow = 8; // !!!!
	TString SIGNALname[CHtoShow];
	//SIGNALname[0] = "5MCP1";

	/*MCP & CFD ------------------------------*/
	for(int ch = 0; ch<CHtoShow*0.5; ch++){
		SIGNALname[ch]=Form("5MCP%i",ch+1);
		SIGNALname[ch+4]=Form("5CFD%i",ch+1);
	}
	/*----------------------------------------*/

	AmplitudeData SIGNAL1ampl[CHtoShow];
	int SIGNAL1_ctlk[CHtoShow];

	for(int ch=0; ch<CHtoShow; ch++){
		Tree->SetBranchAddress(SIGNALname[ch].Data(), &(SIGNAL1ampl[ch]) );
		Tree->SetBranchAddress(Form("%s_crtl",SIGNALname[ch].Data()), &(SIGNAL1_ctlk[ch]) );
	}

	TObjArray HistArray;

	/*MCP & CFD ------------------------------*/
	for(int ch=0; ch<CHtoShow*0.5; ch++)
		HistArray.Add( new TH1F(Form("%s_Ampl",SIGNALname[ch].Data()), Form("%s Amplitude; mV",SIGNALname[ch].Data()),
				1000,0,1000) );

	for(int ch=0; ch<CHtoShow*0.5; ch++)
		HistArray.Add( new TH1F(Form("%s_Time",SIGNALname[ch+4].Data()), Form("%s Time; ns",SIGNALname[ch+4].Data()),
				1000,-100,100) );
	/*----------------------------------------*/

	int EvCalc = Tree->GetEntries();
	//EvCalc = 10000;

	printf("Total events: %lli\n", Tree->GetEntries());
	printf("Events to calculate: %i\n", EvCalc);

	for(int nEvent = 0; nEvent < EvCalc; nEvent+=1){
		if( nEvent%31==0){ printf("processed %i (%.0f\%%)\r",nEvent, floor((float)nEvent/(float)EvCalc*100.)); std::cout<<std::flush;};

		Tree->GetEntry(nEvent);



		/*MCP & CFD ------------------------------*/
		for(int ch=0; ch<CHtoShow*0.5; ch++){
			//if(  SIGNAL1ampl[ch+4].Time_pol1 == 0  ) continue;
			//if(  SIGNAL1ampl[ch].Amplitude == 0  ) continue;

			( (TH1*)HistArray.At(ch) )->Fill(SIGNAL1ampl[ch].Amplitude );
			( (TH1*)HistArray.At(ch+4) )->Fill(SIGNAL1ampl[ch+4].Time_pol1 );
		}
		/*----------------------------------------*/

	}
	HDraw(HistArray, "signals");
	return 0;
}

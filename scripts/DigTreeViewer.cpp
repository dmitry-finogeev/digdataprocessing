#include "includes/DigVariables.h"
#include "includes/TimeAmplCorr.h"
#include "includes/DigData.h"
#include "includes/utilities.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TProfile.h>
#include<TObjString.h>

void MCP_Analysis(TObjArray *Array, TTree *tree, int N_mcp, int N_qd, char* label, int type, char **pr_out_name, float *pr_out_array, int *pr_out_curr){
    //type: 0 all, 1 lin, 2 pol1, 3 pol3, 4 led, 5 cfd sim, 6 cfd module

    int Ev = 5000000;
    float min_ampl = 50.;
    float max_ampl = 500.;
    int cond_level = 3; //0 all,1 with right amplitude, 2 w/o crosstalks, 3 XY separaton
    float ampl_min_r, ampl_max_r; //amplitude range by fitting
    char condition[500];
    char MCP[10]; sprintf(MCP,"%iMCP%i",N_mcp,N_qd);
    char Fit_OP[10]; sprintf(Fit_OP,"QN");
    //float time_scale = 200./1024.;
    float time_scale = 1.;

    char Hist_label[200]; sprintf(Hist_label,"%s %s %s %s",MCP,label,(cond_level >1)?"crt_ON":"crt_OFF",(cond_level >2)?"XY_ON":"XY_OFF");
    TF1 Resolution("Resolution","gaus");
    float  fit_mean, fit_sigma, H_Mean, H_RMS;
    float X_mean, X_RMS, Y_mean, Y_RMS;
    TH1 *Hcur1;

    //mcp quadrants excluded for crosstlks
    int crt_ex[3], crt_cur = 1;
    for(int i=0;i<3;i++){
        if(crt_cur == N_qd) crt_cur++;
        crt_ex[i] = crt_cur;
        crt_cur++;
    }

    //amplitude analysis
    Hcur1 = new TH1F(Form("Ampl_%s_%i_%s",MCP,type,label),Form("Amplitude %s",Hist_label),5000,0,1000);
    tree->Draw(Form("main_%s.Amplitude>>Ampl_%s_%i_%s",MCP,MCP,type,label),"","",Ev);
    Hcur1->Fit(&Resolution, "Q","",min_ampl,max_ampl);
    fit_mean = Resolution.GetParameter(1); fit_sigma = Resolution.GetParameter(2);
    ampl_min_r = fit_mean-2.*fit_sigma; ampl_max_r = fit_mean+2.*fit_sigma;
    //Array->Add( Hcur1 );

    //print header
    printf("\n----------------------------------------------------\n");
    printf("%s, %s Analysis\n", MCP, label);
    printf("Ampl MEAN %.1f, Ampl SIGMA %.1f, Ampl RANGE [%.1f, %.1f]\n",fit_mean,fit_sigma,ampl_min_r,ampl_max_r);

    //condition equation
    //sprintf(condition,"(main_T0_C1.Time_pol3>0)&&(main_T0_D1.Time_pol3>0)");
    sprintf(condition,"(main_T0_C1.Time_pol1>0)&&(main_T0_D1.Time_pol1>0)");
    if(cond_level > 0) sprintf(condition,"%s && (main_%s.Amplitude>=%.2f)&&(main_%s.Amplitude<=%.2f)",condition,MCP,ampl_min_r,MCP,ampl_max_r);
    if(cond_level > 1) sprintf(condition,"%s && (main_%iMCP%i.Amplitude<%.2f) && (main_%iMCP%i.Amplitude<%.2f) && (main_%iMCP%i.Amplitude<%.2f)",condition,
                               N_mcp,crt_ex[0],    min_ampl,   N_mcp,crt_ex[1],    min_ampl,    N_mcp,crt_ex[2],    min_ampl);


    //X determine
    Hcur1 = new TH1F(Form("X_%s_%i_%s",MCP,type,label),Form("X coordinate %s",Hist_label),6,0,6);
    tree->Draw(Form("X.Xch>>X_%s_%i_%s",MCP,type,label),Form("%s && X.Xch>0",condition),"",Ev);
    X_mean = Hcur1->GetMean(), X_RMS = Hcur1->GetRMS();
    //Array->Add( Hcur1 );


    //Y determine
    Hcur1 = new TH1F(Form("Y_%s_%i_%s",MCP,type,label),Form("Y coordinate %s",Hist_label),6,0,6);
    tree->Draw(Form("Y.Ych>>Y_%s_%i_%s",MCP,type,label),Form("%s && Y.Ych>0",condition),"",Ev);
    Y_mean = Hcur1->GetMean(), Y_RMS = Hcur1->GetRMS();
    //Array->Add( Hcur1 );

    printf("Mean COORDINATES: X = %.1f ± %.1f; Y = %.1f ± %.1f\n",X_mean,X_RMS,Y_mean,Y_RMS);
    if(cond_level > 2) sprintf(condition,"%s && (X.Xch > %.1f) && (X.Xch < %.1f) && (Y.Ych > %.1f) && (Y.Ych < %.1f)",condition,X_mean-X_RMS,X_mean+X_RMS,Y_mean-Y_RMS,Y_mean+Y_RMS);
    printf("Crosstalks exlusion %s, to exlude: %iMCP%i, %iMCP%i, %iMCP%i\n",cond_level?"ON":"OFF",N_mcp,crt_ex[0],N_mcp,crt_ex[1],N_mcp,crt_ex[2]);
    printf("\nExlusion equation:\n%s\n\n",condition);




    //signal lenght
    Hcur1 = new TH1F(Form("slenght_%s_%i_%s",MCP,type,label),Form("Signal lenght %s; time [ns]",Hist_label),1000,0,100);
    tree->Draw(Form("(main_%s.time_end - main_%s.time_begin)*%.3f >>slenght_%s_%i_%s",MCP,MCP,time_scale,MCP,type,label),condition,"",Ev);
    //Array->Add( Hcur1 );

    //signal front lenght
    Hcur1 = new TH1F(Form("flenght_%s_%i_%s",MCP,type,label),Form("Front lenght %s; time [ns]",Hist_label),1000,0,10);
    tree->Draw(Form("(main_%s.time_front_end - main_%s.time_begin)*%.3f >>flenght_%s_%i_%s",MCP,MCP,time_scale,MCP,type,label),condition,"",Ev);
    Hcur1->SetMaximum(2000);
    //Array->Add( Hcur1 );

    //signal back lenght
    Hcur1 = new TH1F(Form("blenght_%s_%i_%s",MCP,type,label),Form("Back lenght %s; time [ns]",Hist_label),1000,0,100);
    tree->Draw(Form("(main_%s.time_end - main_%s.time_back_begin)*%.3f >>blenght_%s_%i_%s",MCP,MCP,time_scale,MCP,type,label),condition,"",Ev);
    //Array->Add( Hcur1 );

    //signal back lenght
    Hcur1 = new TH1F(Form("blenght_%s_%i_%s",MCP,type,label),Form("Back lenght %s; time [ns]",Hist_label),1000,0,100);
    tree->Draw(Form("(main_%s.time_end - main_%s.time_back_begin)*%.3f >>blenght_%s_%i_%s",MCP,MCP,time_scale,MCP,type,label),condition,"",Ev);
    //Array->Add( Hcur1 );

    int NumBin = 200;
    float MinBin = 25, MaxBin = 70.;NumBin = ceil(MaxBin-MinBin)*NumBin;
    float MaxHAmpl = 3000.;

    //linear MCP vs T0 PMT
    if((type == 0)||(type == 1)){
        Hcur1 = new TH1F(Form("time_lin_T0PMT_%s_%i_%s",MCP,type,label),"",NumBin,MinBin,MaxBin);
        tree->Draw(Form("(main_%s.Time - (main_T0_C1.Time_pol3+main_T0_D1.Time_pol3)*0.5)*%.3f >>time_lin_T0PMT_%s_%i_%s",MCP,time_scale,MCP,type,label),
                   Form("%s && (main_%s.Time > 0)",condition,MCP),"",Ev);
        //tree->Draw(Form("(main_%s.Time - (main_T0_C1.Time+main_T0_C1.Time)*0.5)*%.3f >>time_lin_T0PMT_%s",MCP,time_scale,MCP),condition,"",Ev);
        H_Mean = Hcur1->GetMean(), H_RMS = Hcur1->GetRMS();
        Hcur1->Fit(&Resolution, Fit_OP,"",H_Mean-H_RMS*2,H_Mean+H_RMS*2);
        fit_mean = Resolution.GetParameter(1); fit_sigma = Resolution.GetParameter(2);
        printf("LIN Time resolution by T0PMTs [%.1f, %.1f]:	mean = %.1f, sigma = %.4f [ns]\n",H_Mean-H_RMS*2,H_Mean+H_RMS*2,fit_mean,fit_sigma);
        Hcur1->SetTitle( Form("LIN %s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        //Hcur1->SetTitle( Form("%s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );
    }

    //polynome1 MCP vs T0 PMT
    if((type == 2)||(type == 1)){
        Hcur1 = new TH1F(Form("time_pol1_T0PMT_%s_%i_%s",MCP,type,label),"",NumBin,MinBin,MaxBin);
        tree->Draw(Form("(main_%s.Time_pol1 - (main_T0_C1.Time_pol1+main_T0_D1.Time_pol1)*0.5)*%.3f >>time_pol1_T0PMT_%s_%i_%s",MCP,time_scale,MCP,type,label),
                   Form("%s && (main_%s.Time_pol1  > 0)",condition,MCP),"",Ev);
        H_Mean = Hcur1->GetMean(), H_RMS = Hcur1->GetRMS();
        Hcur1->Fit(&Resolution, Fit_OP,"",H_Mean-H_RMS*2,H_Mean+H_RMS*2);
        fit_mean = Resolution.GetParameter(1); fit_sigma = Resolution.GetParameter(2);
        printf("POL1 Time resolution by T0PMTs [%.1f, %.1f]:	mean = %.1f, sigma = %.4f [ns]\n",H_Mean-H_RMS*2,H_Mean+H_RMS*2,fit_mean,fit_sigma);
        Hcur1->SetTitle( Form("POL1 %s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        sprintf(pr_out_name[*pr_out_curr],"POL1 %s",Hist_label);
        pr_out_array[*pr_out_curr] = fit_sigma*1000.;
        (*pr_out_curr)++;
        //Hcur1->SetTitle( Form("%s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        //Hcur1->SetBins(100, floor(fit_mean-3.*fit_sigma),ceil(fit_mean+3.*fit_sigma));
        //Hcur1->Rebuild();
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );
    }

    //polynome3 MCP vs T0 PMT
    if((type == 3)||(type == 1)){
        Hcur1 = new TH1F(Form("time_pol3_T0PMT_%s_%i_%s",MCP,type,label),"",NumBin,MinBin,MaxBin);
        tree->Draw(Form("(main_%s.Time_pol3 - (main_T0_C1.Time_pol1+main_T0_D1.Time_pol1)*0.5)*%.3f >>time_pol3_T0PMT_%s_%i_%s",MCP,time_scale,MCP,type,label),
                   Form("%s && (main_%s.Time_pol3  > 0)",condition,MCP),"",Ev);
        H_Mean = Hcur1->GetMean(), H_RMS = Hcur1->GetRMS();
        Hcur1->Fit(&Resolution, Fit_OP,"",H_Mean-H_RMS*2,H_Mean+H_RMS*2);
        fit_mean = Resolution.GetParameter(1); fit_sigma = Resolution.GetParameter(2);
        printf("POL3 Time resolution by T0PMTs [%.1f, %.1f]:	mean = %.1f, sigma = %.4f [ns]\n",H_Mean-H_RMS*2,H_Mean+H_RMS*2,fit_mean,fit_sigma);
        Hcur1->SetTitle( Form("POL3 %s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        sprintf(pr_out_name[*pr_out_curr],"POL3 %s",Hist_label);
        pr_out_array[*pr_out_curr] = fit_sigma*1000.;
        (*pr_out_curr)++;
        //Hcur1->SetTitle( Form("%s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        //Hcur1->SetBins(100, floor(fit_mean-3.*fit_sigma),ceil(fit_mean+3.*fit_sigma));
        //Hcur1->Rebuild();
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );
    }

    //LED MCP vs T0 PMT
    if((type == 4)||(type == 4)){
        Hcur1 = new TH1F(Form("time_LED_T0PMT_%s_%i_%s",MCP,type,label),"",NumBin,MinBin,MaxBin);
        tree->Draw(Form("(%s.LEDtime - (main_T0_C1.Time_pol1+main_T0_D1.Time_pol1)*0.5)*%.3f >>time_LED_T0PMT_%s_%i_%s",MCP,time_scale,MCP,type,label),
                   Form("%s && (%s.LEDtime > 0)",condition,MCP),"",Ev);
        H_Mean = Hcur1->GetMean(), H_RMS = Hcur1->GetRMS();
        Hcur1->Fit(&Resolution, Fit_OP,"",H_Mean-H_RMS*2,H_Mean+H_RMS*2);
        fit_mean = Resolution.GetParameter(1); fit_sigma = Resolution.GetParameter(2);
        printf("CFD Time resolution by T0PMTs [%.1f, %.1f]:	mean = %.1f, sigma = %.4f [ns]\n",H_Mean-H_RMS*2,H_Mean+H_RMS*2,fit_mean,fit_sigma);
        Hcur1->SetTitle( Form("LED %s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );
    }

    //CFD sim MCP vs T0 PMT
    if((type == 0)||(type == 5)){
        Hcur1 = new TH1F(Form("time_CFDsim_T0PMT_%s_%i_%s",MCP,type,label),"",NumBin,MinBin,MaxBin);
        tree->Draw(Form("(%s.CFDtime - (main_T0_C1.Time_pol1+main_T0_D1.Time_pol1)*0.5)*%.3f >>time_CFDsim_T0PMT_%s_%i_%s",MCP,time_scale,MCP,type,label),
                   Form("%s && (%s.CFDtime > 0)",condition,MCP),"",Ev);
        H_Mean = Hcur1->GetMean(), H_RMS = Hcur1->GetRMS();
        Hcur1->Fit(&Resolution, Fit_OP,"",H_Mean-H_RMS*2,H_Mean+H_RMS*2);
        fit_mean = Resolution.GetParameter(1); fit_sigma = Resolution.GetParameter(2);
        printf("CFD sim Time resolution by T0PMTs [%.1f, %.1f]:	mean = %.1f, sigma = %.4f [ns]\n",H_Mean-H_RMS*2,H_Mean+H_RMS*2,fit_mean,fit_sigma);
        Hcur1->SetTitle( Form("CFD sim %s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        sprintf(pr_out_name[*pr_out_curr],"CFD sim %s",Hist_label);
        pr_out_array[*pr_out_curr] = fit_sigma*1000.;
        (*pr_out_curr)++;
        //Hcur1->SetTitle( Form("%s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );
    }



    //CFD module MCP vs T0 PMT
    if((type == 0)||(type == 6)){
        float time_corr;
        if(N_qd == 1)time_corr = -26.;
        if(N_qd == 2)time_corr = -22.6;
        if(N_qd == 3)time_corr = -14.3;
        if(N_qd == 4)time_corr = -13.5;
        Hcur1 = new TH1F(Form("time_pol1_T0PMT_%iCFD%i_%i_%s",N_mcp,N_qd,type,label),"",NumBin,MinBin,MaxBin);
        tree->Draw(Form("(main_2CFD%i.Time_pol1 + %.2f - (main_T0_C1.Time_pol1+main_T0_D1.Time_pol1)*0.5)*%.3f >>time_pol1_T0PMT_%iCFD%i_%i_%s",N_qd,time_corr,time_scale,N_mcp,N_qd,type,label),
                   Form("%s && (main_2CFD%i.Time_pol1 > 0)",condition,N_qd),"",Ev);
        H_Mean = Hcur1->GetMean(), H_RMS = Hcur1->GetRMS();
        Hcur1->Fit(&Resolution, Fit_OP,"",H_Mean-H_RMS*2,H_Mean+H_RMS*2);
        fit_mean = Resolution.GetParameter(1); fit_sigma = Resolution.GetParameter(2);
        printf("CFD Time resolution by T0PMTs [%.1f, %.1f]:	mean = %.1f, sigma = %.4f [ns]\n",H_Mean-H_RMS*2,H_Mean+H_RMS*2,fit_mean,fit_sigma);
        Hcur1->SetTitle( Form("CFD mod %s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        sprintf(pr_out_name[*pr_out_curr],"CFD mod %s",Hist_label);
        pr_out_array[*pr_out_curr] = fit_sigma*1000.;
        (*pr_out_curr)++;
        //Hcur1->SetTitle( Form("%s (#sigma = %.1fps);Time [ns]",Hist_label,fit_sigma*1000.) );
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );
    }

    /*
//pol3 vs MPC ampl
Hcur2 = new TH2F(Form("time_ampl_pol3_T0PMT_%s",MCP),Form("pol3 vs Ampl %s; Ampl [mV];Time [ns]",Hist_label,fit_sigma*1000.),1000,0,1000,NumBin,MinBin,MaxBin);
tree->Draw(Form("(main_%s.Time_pol3 - (main_T0_C1.Time_pol3+main_T0_D1.Time_pol3)*0.5)*%.3f:main_%s.Amplitude >>time_ampl_pol3_T0PMT_%s",MCP,time_scale,MCP,MCP),
       Form("%s && (main_%s.Time_pol3  > 0)",condition,MCP),"",Ev);
//Array->Add( Hcur2 );

//CFD module vs MPC ampl
Hcur2 = new TH2F(Form("time_ampl_pol3_T0PMT_%iCFD%i",N_mcp,N_qd),Form("CFD mod vs Ampl %s; Ampl [mV];Time [ns]",Hist_label,fit_sigma*1000.),1000,0,1000,NumBin,MinBin,MaxBin);
tree->Draw(Form("(main_2CFD%i.Time_pol3 - (main_T0_C1.Time_pol3+main_T0_D1.Time_pol3)*0.5)*%.3f:main_%s.Amplitude >>time_ampl_pol3_T0PMT_%iCFD%i",N_qd,time_scale,MCP,N_mcp,N_qd),
       Form("%s && (main_2CFD%i.Time_pol3 > 0)",condition,N_qd),"",Ev);
//Array->Add( Hcur2 );
*/
};



void TimeAmplCorrection(TObjArray *Array, TTree *tree, char* CHname, char *label_){

    char Label[40]; sprintf(Label,"%s_%s",CHname,label_);
    int Statistic = 50000;

    float Time_min = 28., Time_max = 31.;
    int Time_bin = ceil(Time_max-Time_min)*1000;
    float Ampl_min = 195., Ampl_max = 325.;
    int Ampl_bin = ceil( (Ampl_max-Ampl_min)*0.4 );

    TH1F *H1cur1, *H1cur2;
    TH2F *H2cur;
    TProfile *HProf;

    H2cur = NULL; Array->Add(H2cur = new TH2F(Form("AmplTime_%s",Label),Form("Amplitude vs Time (POL1) %s;Amplitude, mV; Time, ns",Label),Ampl_bin,Ampl_min,Ampl_max,Time_bin,Time_min,Time_max) );
    tree->Draw(Form("main_%s.Time_pol1 - (main_T0_C1.Time_pol1+main_T0_D1.Time_pol1)*0.5:main_%s.Amplitude>>AmplTime_%s",CHname,CHname,Label),
               Form("(main_%s.Time_pol1>0)&&(main_T0_C1.Time_pol1>0)&&(main_T0_D1.Time_pol1>0)",CHname),"",Statistic);


    HProf = NULL; Array->Add(HProf = H2cur->ProfileX(Form("AmplTimeProfile_%s",Label)) );
    TF1 *ATfunc = new TF1("ATfunction","pol6");
    HProf->Fit(ATfunc, "WI","", Ampl_min, Ampl_max);

    H2cur = NULL; Array->Add(H2cur = new TH2F(Form("AmplTimeC_%s",Label),Form("Amplitude vs Time (CORRECTED) %s;Amplitude, mV; Time, ns",Label),Ampl_bin,Ampl_min,Ampl_max,Time_bin,Time_min,Time_max) );
    H1cur1 = NULL; H1cur1 = new TH1F(Form("TimeWOC_%s",Label), Form("Time NOT CORRECTED %s; Time, ns",Label),Time_bin,Time_min,Time_max);
    H1cur2 = NULL; H1cur2 = new TH1F(Form("TimeWIC_%s",Label), Form("Time CORRECTED %s; Time, ns",Label),Time_bin,Time_min,Time_max);


    MAIN_signal signal; tree->SetBranchAddress(Form("main_%s", CHname), &signal);
    MAIN_signal T0_C1; tree->SetBranchAddress("main_T0_C1", &T0_C1);
    MAIN_signal T0_D1; tree->SetBranchAddress("main_T0_D1", &T0_D1);

    for(int Event=0; Event < ((Statistic < tree->GetEntries())?Statistic:tree->GetEntries()); Event++){
        tree->GetEntry(Event);

        float Ampl = signal.Amplitude;
        float Time = ( signal.Time_pol1 - (T0_C1.Time_pol1+T0_D1.Time_pol1)*0.5 );
        float Cor_Time = Time - ATfunc->Eval(Ampl) + (Time_max+Time_min)*0.5;

        H2cur->Fill(Ampl, Cor_Time);

        H1cur1->Fill(Time);
        H1cur2->Fill(Cor_Time);
    }

    HProf = NULL; Array->Add(HProf = H2cur->ProfileX(Form("AmplTimeProfileCORR_%s",Label)) );
    HProf->Fit(ATfunc, "WI","", Ampl_min, Ampl_max);

    Array->Add(H1cur1);
    Array->Add(H1cur2);
}

void MCP_HistCollect(TObjArray *Array, TTree *tree, const char *label_, const char* MCPname_, int ch, int type, char **pr_out_name, float *pr_out_array, int *pr_out_curr){

    int Stat = 0;
    float TimenSigma = 50.;
    float XYnSigma = 1.;
    float MaxHAmpl = 200;

    TH1 *Hcur1;
    TF1 Resolution("Resolution","gaus");

    int Time_bin = 0;
    float fit_sigma = 0;

    char Fit_OP[10]; sprintf(Fit_OP,"QN");
    char MCPname[100]; sprintf(MCPname,"%s%i",MCPname_,ch);
    char Label[100]; sprintf(Label,"%s_%s",label_, MCPname);
    if(Stat == 0) Stat = tree->GetEntries();

    //determine Time Range:
    Hcur1 = NULL; Hcur1 = new TH1F(Form("Time_pol1_range_%s",Label),"",10000,-250,250);
    tree->Draw(Form("%s.Time_pol1 >> Time_pol1_range_%s",MCPname,Label),Form("%s.Time_pol1 != 0",MCPname),"",Stat);
    ValueRange TimeRange(Hcur1->GetMean(), Hcur1->GetRMS(), -100., 100.);
    Hcur1->Fit(&Resolution, Fit_OP,"",TimeRange.MinRange(2.),TimeRange.MaxRange(2.));
    TimeRange.Mean = Resolution.GetParameter(1); TimeRange.Sigma = Resolution.GetParameter(2);
    Time_bin = ceil(TimeRange.Sigma*TimenSigma)*500;

    //pol1, all
    if(type == 1){
        //Hcur1 = NULL; Hcur1 = new TH1F(Form("Time_pol1_%s",Label),"",Time_bin,TimeRange.MinRange(TimenSigma),TimeRange.MaxRange(TimenSigma));
        Hcur1 = NULL; Hcur1 = new TH1F(Form("Time_pol1_%s",Label),"",30000,-150,150);
        tree->Draw(Form("%s.Time_pol1 >> Time_pol1_%s",MCPname,Label),Form("%s.Time_pol1 != 0",MCPname),"",Stat);
        Hcur1->Fit(&Resolution, Fit_OP,"",TimeRange.MinRange(TimenSigma),TimeRange.MaxRange(TimenSigma));
        fit_sigma = Resolution.GetParameter(2);
        Hcur1->SetTitle( Form("Time_pol1 \"%s\" (#sigma = %.1fps);Time [ns]",Label,fit_sigma*1000.) );
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );

        sprintf(pr_out_name[*pr_out_curr],"%s POL1",Label);
        pr_out_array[*pr_out_curr] = fit_sigma*1000.;
        (*pr_out_curr)++;
    }

    //pol1 ATC, all
    if(type == 2){
        Hcur1 = NULL; Hcur1 = new TH1F(Form("Time_pol1_corr_%s",Label),"",Time_bin,TimeRange.MinRange(TimenSigma),TimeRange.MaxRange(TimenSigma));
        tree->Draw(Form("%s.Time_pol1_ATC >> Time_pol1_corr_%s",MCPname,Label),Form("%s.Time_pol1_ATC != 0",MCPname),"",Stat);
        Hcur1->Fit(&Resolution, Fit_OP,"",TimeRange.MinRange(TimenSigma),TimeRange.MaxRange(TimenSigma));
        fit_sigma = Resolution.GetParameter(2);
        Hcur1->SetTitle( Form("Time_pol1 (ampl-time corrected) \"%s\" (#sigma = %.1fps);Time [ns]",Label,fit_sigma*1000.) );
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );

        sprintf(pr_out_name[*pr_out_curr],"%s POL1_ATC",Label);
        pr_out_array[*pr_out_curr] = fit_sigma*1000.;
        (*pr_out_curr)++;
    }

    //pol1 ATC, no crtl
    if(type == 3){
        Hcur1 = NULL; Hcur1 = new TH1F(Form("Time_pol1_corr__nocrtl%s",Label),"",Time_bin,TimeRange.MinRange(TimenSigma),TimeRange.MaxRange(TimenSigma));
        tree->Draw(Form("%s.Time_pol1_ATC >> Time_pol1_corr__nocrtl%s",MCPname,Label),Form("(%s.Time_pol1_ATC != 0)&&(%s_crtl == 1)",MCPname, MCPname_),"",Stat);
        Hcur1->Fit(&Resolution, Fit_OP,"",TimeRange.MinRange(TimenSigma),TimeRange.MaxRange(TimenSigma));
        fit_sigma = Resolution.GetParameter(2);
        Hcur1->SetTitle( Form("Time_pol1 (ampl-time corrected) [NO CRTLK] \"%s\" (#sigma = %.1fps);Time [ns]",Label,fit_sigma*1000.) );
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );

        sprintf(pr_out_name[*pr_out_curr],"%s POL1_ATC_NOCRTL",Label);
        pr_out_array[*pr_out_curr] = fit_sigma*1000.;
        (*pr_out_curr)++;
    }

    //pol1 ATC, no crtl, XY centered
    if(type == 4){
        Hcur1 = NULL; Hcur1 = new TH1F(Form("Time_pol1_corr__nocrtl_XY%s",Label),"",Time_bin,TimeRange.MinRange(TimenSigma),TimeRange.MaxRange(TimenSigma));
        tree->Draw(Form("%s.Time_pol1_ATC >> Time_pol1_corr__nocrtl_XY%s",MCPname,Label),Form("(%s.Time_pol1_ATC != 0)&&(%s_crtl == 1)&&(%s.X_range < %.2f)&&(%s.Y_range < %.2f)",MCPname, MCPname_,MCPname,XYnSigma,MCPname,XYnSigma),"",Stat);
        Hcur1->Fit(&Resolution, Fit_OP,"",TimeRange.MinRange(TimenSigma),TimeRange.MaxRange(TimenSigma));
        fit_sigma = Resolution.GetParameter(2);
        Hcur1->SetTitle( Form("Time_pol1 (ampl-time corrected) [NO CRTLK][XY centr] \"%s\" (#sigma = %.1fps);Time [ns]",Label,fit_sigma*1000.) );
        Hcur1->SetMaximum(MaxHAmpl);
        Array->Add( Hcur1 );

        sprintf(pr_out_name[*pr_out_curr],"%s POL1_ATC_NOCRTL_XY",Label);
        pr_out_array[*pr_out_curr] = fit_sigma*1000.;
        (*pr_out_curr)++;
    }

    //Amplitude
    if(type == 5){
        Hcur1 = NULL; Hcur1 = new TH1F(Form("Amplitude_%s",Label),Form("Amplitude %s;amplitude, mV",Label),500,0,500);
        tree->Draw(Form("%s.Amplitude >> Amplitude_%s",MCPname,Label),"","",Stat);
        Array->Add( Hcur1 );
    }

}


int DigTreeViewer(){
    printf("\n\n  ---- DigTreeViewer ---- \n\n");

    const int n_pr_out = 200;
    const int pr_name_len = 255;
    int pr_out_curr = 0;

    float pr_out_array[n_pr_out];
    char **pr_out_name=new char*[n_pr_out];
    for (int i=0;i<n_pr_out;i++) pr_out_name[i]=new char [pr_name_len];


    /***/
    //MCP hists by RUNs
    TObjArray RunName;
    TObjArray Trees;
    TObjArray Hists;

    TFile *currFile;
    TString currRUNname;


    TString Path("/mnt/hgfs/DETECTORS_DATA/Fit_June2015/Processed/");

    RunName.Add( new TObjString("CFD_1mV") );
    RunName.Add( new TObjString("CFD_2mV") );
    RunName.Add( new TObjString("CFD_3mV") );
    RunName.Add( new TObjString("CFD_4mV") );

    int type = 1;

    for(int RUN = 0; RUN <= RunName.GetLast(); RUN++){
        currRUNname = ((TObjString*)RunName.At(RUN) )->GetString();

        currFile = new TFile( Form("%s%s.root", Path.Data(), currRUNname.Data() ) ); //files rest open
        Trees.Add( (TTree*)currFile->Get("ProcDataTree") );
    }

    for(int ch=1;ch<=4;ch++)
        for(int RUN = 0; RUN <= RunName.GetLast(); RUN++){
            currRUNname = ((TObjString*)RunName.At(RUN) )->GetString();
            MCP_HistCollect(&Hists, (TTree*)Trees.At(RUN), currRUNname.Data() , "2CFD",ch,type,pr_out_name,pr_out_array,&pr_out_curr);
        }// for RUN

    HDraw(Hists, "MCP hists", 2, 2, 4, 0, 40, 0, 0, "");

    /***/


    /***
//MCP hists
TFile *hRUN = new TFile("/mnt/hgfs/DETECTORS_DATA/Fit_October2015/15_10_04_15_12_test1/Data_STARTby4MCP.root");
TTree *tRUN = (TTree*)hRUN->Get("ProcDataTree");


TObjArray Hists;

char *CHname = "5MCP";
char *Label = " CosmicRUN_byTRG1";
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN, Label, "5MCP",ch,1,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN, Label, "5CFD",ch,1,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN, Label, "4MCP",ch,1,pr_out_name,pr_out_array,&pr_out_curr);

HDraw(Hists, "MCP hists", 2, 2, 4, 0, 40, 0, 0, "");



//MCP hists
TFile *hRUN = new TFile("RUN31.root");
TTree *tRUN = (TTree*)hRUN->Get("ProcDataTree");


TObjArray Hists;

char *CHname = "1MCP";
char *Label = "RUN31";
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN, Label, CHname,ch,1,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN, Label, CHname,ch,2,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN, Label, CHname,ch,3,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN, Label, CHname,ch,4,pr_out_name,pr_out_array,&pr_out_curr);

HDraw(Hists, "MCP hists", 2, 2, 4, 0, 40, 0, 0, "");



//MCP hists by RUNs
TFile *hRUN1 = new TFile("RUN23.root");
TFile *hRUN2 = new TFile("RUN24.root");
TFile *hRUN3 = new TFile("RUN31.root");

TTree *tRUN1 = (TTree*)hRUN1->Get("ProcDataTree");
TTree *tRUN2 = (TTree*)hRUN2->Get("ProcDataTree");
TTree *tRUN3 = (TTree*)hRUN3->Get("ProcDataTree");

TObjArray Hists;

int t = 1;
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN1, "RUN23", "1MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN1, "RUN23", "2MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN2, "RUN24", "4MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN3, "RUN31", "1MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);

HDraw(Hists, "MCP hists", 2, 2, 4, 0, 40, 0, 0, "");



//CFD hists by RUNs
TFile *hRUN1 = new TFile("CFD_1mV.root");
TFile *hRUN2 = new TFile("CFD_2mV.root");
TFile *hRUN3 = new TFile("CFD_3mV.root");
TFile *hRUN4 = new TFile("CFD_4mV.root");


TTree *tRUN1 = (TTree*)hRUN1->Get("RawDataTree");
TTree *tRUN2 = (TTree*)hRUN2->Get("RawDataTree");
TTree *tRUN3 = (TTree*)hRUN3->Get("RawDataTree");
TTree *tRUN4 = (TTree*)hRUN4->Get("RawDataTree");

TObjArray Hists;

int t = 5;
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN1, "1mV", "main_2MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
//for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN1, "1mV", "2CFD",ch,t,pr_out_name,pr_out_array,&pr_out_curr);

for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN2, "2mV", "main_2MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
//for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN2, "2mV", "2CFD",ch,t,pr_out_name,pr_out_array,&pr_out_curr);

for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN3, "3mV", "main_2MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
//for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN3, "3mV", "2CFD",ch,t,pr_out_name,pr_out_array,&pr_out_curr);

for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN4, "4mV", "main_2MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
//for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN4, "4mV", "2CFD",ch,t,pr_out_name,pr_out_array,&pr_out_curr);

HDraw(Hists, "MCP hists", 2, 2, 4, 0, 40, 0, 0, "");



//full amplitude
TFile *hRUN1 = new TFile("RUN23.root");
TFile *hRUN2 = new TFile("RUN24.root");
TFile *hRUN3 = new TFile("RUN31.root");

TTree *tRUN1 = (TTree*)hRUN1->Get("RawDataTree");
TTree *tRUN2 = (TTree*)hRUN2->Get("RawDataTree");
TTree *tRUN3 = (TTree*)hRUN3->Get("RawDataTree");

TObjArray Hists;

int t = 5;
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN1, "RUN23", "main_1MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN1, "RUN23", "main_2MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN2, "RUN24", "main_4MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);
for(int ch=1;ch<=4;ch++) MCP_HistCollect(&Hists, tRUN3, "RUN31", "main_1MCP",ch,t,pr_out_name,pr_out_array,&pr_out_curr);

HDraw(Hists, "MCP hists", 2, 2, 4, 0, 40, 0, 0, "");



//Amplitude Time correction
TFile *hRUN24 = new TFile("RUN24_.root");
TTree *tRUN24 = (TTree*)hRUN24->Get("DigTree");

char *CHname = "4MCP3";
TObjArray Hists;

TimeAmplCorrection(&Hists, tRUN24, CHname, "RUN24");


HDraw(Hists, "Timing Amplitude correction", 2, 3, 1, 0, 20, 0, 0, "");




//MCP amplitudes vs timing
TFile *hRUN23 = new TFile("RUN23_.root");
TTree *tRUN23 = (TTree*)hRUN23->Get("DigTree");

char *CHname = "2MCP1";

TObjArray Hists;
Hists.Add( new TH2F(Form("AmplTime_%s",CHname),Form("Amplitude vs Time (POL1) %s;Amplitude, mV; Time, ns",CHname),500,0,1000,400,28,31) );
tRUN23->Draw(Form("main_%s.Time_pol1 - (main_T0_C1.Time_pol1+main_T0_D1.Time_pol1)*0.5:main_%s.Amplitude>>AmplTime_%s",CHname,CHname,CHname),
         Form("(main_%s.Time_pol1>0)&&(main_T0_C1.Time_pol1>0)&&(main_T0_D1.Time_pol1>0)",CHname),"",50000);

Hists.Add( new TH2F(Form("AmplPFlenght_%s",CHname),Form("Amplitude vs Time of peak front lenght %s;Amplitude, mV; Time, ns",CHname),500,0,1000,200,0,2) );
tRUN23->Draw(Form("main_%s.time_front_end - main_%s.time_begin:main_%s.Amplitude>>AmplPFlenght_%s",CHname,CHname,CHname,CHname),
         Form("(main_%s.Time_pol1>0)&&(main_T0_C1.Time_pol1>0)&&(main_T0_D1.Time_pol1>0)",CHname),"",50000);

HDraw(Hists, "Timing Amplitude", 2, 1, 1, 0, 4, 0, 0, "col");



//PMT amplitudes
TFile *hRUN23 = new TFile("./28_07/RUN23.root");
TTree *tRUN23 = (TTree*)hRUN23->Get("DigTree");

TObjArray HistsT0;

HistsT0.Add( new TH1F("T0_D1_ampl","amplitudes for T0_1D",20000,-10,2000) );
tRUN23->Draw("main_T0_D1.Amplitude >> T0_D1_ampl");

HDraw(HistsT0, "Tree viewer T0", 2, 1, 1, 0, 4, 0, 0, "");



//T0 PMT timing
gStyle->SetOptFit(111);
TFile *hRUN = new TFile("RUN23.root");
//TFile *hRUN = new TFile("CFD_4mV.root");
//TFile *hRUN = new TFile("test.root");
TTree *tRUN = (TTree*)hRUN->Get("ProcDataTree");

TObjArray HistsT0;

HistsT0.Add( new TH1F("T0_C1_pol1","POL1 T0_1C - (T0_1C + T0_1D)/2",2000,-3,3) );
tRUN->Draw("T0_C1.Time_pol1 - (T0_C1.Time_pol1+T0_D1.Time_pol1)*0.5 >>T0_C1_pol1",
             "(T0_C1.Time_pol1 != 0)&&(T0_D1.Time_pol1 != 0)");

HistsT0.Add( new TH1F("T0_D1_pol1","POL1 T0_1D - (T0_1C + T0_1D)/2",2000,-3,3) );
tRUN->Draw("T0_D1.Time_pol1 - (T0_C1.Time_pol1+T0_D1.Time_pol1)*0.5 >>T0_D1_pol1",
             "(T0_C1.Time_pol1 != 0)&&(T0_D1.Time_pol1 != 0)");

HistsT0.Add( new TH1F("T0_C1_pol1_ATC","POL1 (ATC) T0_1C - (T0_1C + T0_1D)/2",2000,-3,3) );
tRUN->Draw("T0_C1.Time_pol1_ATC - (T0_C1.Time_pol1_ATC+T0_D1.Time_pol1_ATC)*0.5 >>T0_C1_pol1_ATC",
             "(T0_C1.Time_pol1_ATC != 0)&&(T0_D1.Time_pol1_ATC != 0)");

HistsT0.Add( new TH1F("T0_D1_pol1_ATC","POL1 (ATC) T0_1D - (T0_1C + T0_1D)/2",2000,-3,3) );
tRUN->Draw("T0_D1.Time_pol1_ATC - (T0_C1.Time_pol1_ATC + T0_D1.Time_pol1_ATC)*0.5 >>T0_D1_pol1_ATC",
             "(T0_C1.Time_pol1_ATC != 0)&&(T0_D1.Time_pol1_ATC != 0)");

HDraw(HistsT0, "Tree viewer T0", 2, 1, 1, 0, 4, 0, 0, "");



//XY adjustement

TFile *hRUN_24 = new TFile("RUN24_.root");
TTree *tRUN_24 = (TTree*)hRUN_24->Get("DigTree");

TObjArray HistsXY;
for(int n=1; n<=4; n++) MCP_Analysis(&HistsXY, tRUN_24, 4, n, "RUN24",9);

HDraw(HistsXY, "XY coordinate", 2, 2, 2, 0, 100, 0, 1, "");



//filter compare

TFile *hRUN_NF = new TFile("RUN24_.root");
TTree *tRUN_NF = (TTree*)hRUN_NF->Get("DigTree");

TFile *hRUN_F = new TFile("RUN24_F_10_2_5.root");
TTree *tRUN_F = (TTree*)hRUN_F->Get("DigTree");

TObjArray HistsFL;

for(int n=1; n<=4; n++) MCP_Analysis(&HistsFL, tRUN_NF, 4, n, "no filter",2);
for(int n=1; n<=4; n++) MCP_Analysis(&HistsFL, tRUN_NF, 4, n, "no filter",3);
for(int n=1; n<=4; n++) MCP_Analysis(&HistsFL, tRUN_F, 4, n, "filter 10_2_5",2);
for(int n=1; n<=4; n++) MCP_Analysis(&HistsFL, tRUN_F, 4, n, "filter 10_2_5",3);
HDraw(HistsFL, "Filter Compare", 2, 2, 4, 0, 100, 0, 1, "");



//CFD compare
//TFile *hRUN_3mV = new TFile("./28_07/CFD3mV.root");
//TFile *hRUN_4mV = new TFile("./28_07/CFD4mV.root");
TFile *hRUN_3mV = new TFile("CFD3mV.root");
TFile *hRUN_4mV = new TFile("CFD4mV.root");

TTree *tRUN_3mV = (TTree*)hRUN_3mV->Get("DigTree");
TTree *tRUN_4mV = (TTree*)hRUN_4mV->Get("DigTree");

TObjArray Hists;

//type: 0 all, 1 lin, 2 pol1, 3 pol3, 4 led, 5 cfd sim, 6 cfd module
int nMCP = 2;
char dataname[100];
sprintf(dataname,"Tr_3mV");
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN_3mV, nMCP,n, dataname,2,pr_out_name,pr_out_array,&pr_out_curr);
//for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN_3mV, nMCP,n, dataname,3,pr_out_name,pr_out_array,&pr_out_curr);
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN_3mV, nMCP,n, dataname,5,pr_out_name,pr_out_array,&pr_out_curr);
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN_3mV, nMCP,n, dataname,6,pr_out_name,pr_out_array,&pr_out_curr);
sprintf(dataname,"Tr_4mV");
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN_4mV, nMCP,n, dataname,2,pr_out_name,pr_out_array,&pr_out_curr);
//for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN_4mV, nMCP,n, dataname,3,pr_out_name,pr_out_array,&pr_out_curr);
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN_4mV, nMCP,n, dataname,5,pr_out_name,pr_out_array,&pr_out_curr);
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN_4mV, nMCP,n, dataname,6,pr_out_name,pr_out_array,&pr_out_curr);
HDraw(Hists, "Tree viewer", 3, 2, 4, 0, 100, 0, 1, "");



//Timing:
//TFile *hRUN23 = new TFile("./28_07/RUN23.root");
//TFile *hRUN24 = new TFile("./28_07/RUN24.root");
//TFile *hRUN41 = new TFile("./28_07/RUN41.root");

TFile *hRUN23 = new TFile("RUN23_.root");
TFile *hRUN24 = new TFile("RUN24_.root");
TFile *hRUN41 = new TFile("RUN41_.root");

TTree *tRUN23 = (TTree*)hRUN23->Get("DigTree");
TTree *tRUN24 = (TTree*)hRUN24->Get("DigTree");
TTree *tRUN41 = (TTree*)hRUN41->Get("DigTree");

TObjArray Hists;

//type: 0 all, 1 lin, 2 pol1, 3 pol3, 4 led, 5 cfd sim, 6 cfd module
int nMCP = 0;
char dataname[100];
sprintf(dataname,"RUN23_MCP_Fin");nMCP = 1;
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN23, nMCP,n, dataname,2,pr_out_name,pr_out_array,&pr_out_curr);
//for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN23, nMCP,n, dataname,3,pr_out_name,pr_out_array,&pr_out_curr);

sprintf(dataname,"RUN23_MCP_USA");nMCP = 2;
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN23, nMCP,n, dataname,2,pr_out_name,pr_out_array,&pr_out_curr);
//for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN23, nMCP,n, dataname,3,pr_out_name,pr_out_array,&pr_out_curr);

sprintf(dataname,"RUN24_MCP_NBI");nMCP = 4;
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN24, nMCP,n, dataname,2,pr_out_name,pr_out_array,&pr_out_curr);
//for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN24, nMCP,n, dataname,3,pr_out_name,pr_out_array,&pr_out_curr);

sprintf(dataname,"RUN41_MCP_Fin");nMCP = 1;
for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN41, nMCP,n, dataname,2,pr_out_name,pr_out_array,&pr_out_curr);
//for(int n=1; n<=4; n++) MCP_Analysis(&Hists, tRUN41, nMCP,n, dataname,3,pr_out_name,pr_out_array,&pr_out_curr);

HDraw(Hists, "Tree viewer", 2, 2, 4, 0, 100, 0, 1, "");
*/

    printf("\n\n\n----------------------------------------------------\n");
    printf("Data Array print out:\n\n");
    for (int i=0;i<pr_out_curr;i++){
        float val = pr_out_array[i];
        int val_int = floor(val);
        int val_mant = floor( (val - val_int)*100. );
        printf("%s		%i,%i\n",pr_out_name[i], val_int, val_mant);
    }

    for (int i=0;i<n_pr_out;i++) delete []pr_out_name[i]; delete []pr_out_name;

    return 1;

};

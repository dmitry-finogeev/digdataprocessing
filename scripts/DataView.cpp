
#include "includes/DigData.h"
#include "includes/utilities.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include <TH1.h>
#include "TStyle.h"

int DataView(GlobalProcessStatus *globalProcessState){
    printf("\n\n  ---- Data View ---- \n\n");

    //parameters for CFD analysis

    TObjArray Hists;

    DigData *MainData = new DigData(globalProcessState);
    const std::string configPath = "/media/data1/Data_Oct_2015/11102015_1721_RUN45_5MCP_1579_att_CFD_tr20mV/DataConfig.txt";
    MainData->OpenData(configPath.c_str());
    float MCPamplSelection = 220.;

    DigEvent *CurEvent = MainData->GetEvent(0);
    int NumEvent = CurEvent->GetTotalEvents();
    //float TimeDim = MainData->GetDataRecipe()->GetProcParam()->TimeDimension;

    const int StartSignal = 4;
    const int CHtoShow = 8;
    const int Nsignals = CHtoShow+StartSignal;
    const int NCHinGr = floor(  (float)CHtoShow*0.5  );
    int CHsignal[Nsignals];
    MAIN_signal *MAIN_s[Nsignals];

    CHsignal[0] = MainData->GetDataRecipe()->FindChannelByName("T0_C1");
    CHsignal[1] = MainData->GetDataRecipe()->FindChannelByName("T0_D1");
    CHsignal[2] = MainData->GetDataRecipe()->FindChannelByName("T0_C2");
    CHsignal[3] = MainData->GetDataRecipe()->FindChannelByName("T0_Dsc");


    CHsignal[4] = MainData->GetDataRecipe()->FindChannelByName("5MCP1");
    CHsignal[5] = MainData->GetDataRecipe()->FindChannelByName("5MCP2");
    CHsignal[6] = MainData->GetDataRecipe()->FindChannelByName("5MCP3");
    CHsignal[7] = MainData->GetDataRecipe()->FindChannelByName("5MCP4");
    CHsignal[8] = MainData->GetDataRecipe()->FindChannelByName("5CFD1");
    CHsignal[9] = MainData->GetDataRecipe()->FindChannelByName("5CFD2");
    CHsignal[10] = MainData->GetDataRecipe()->FindChannelByName("5CFD3");
    CHsignal[11] = MainData->GetDataRecipe()->FindChannelByName("5CFD4");

    for(int s=0; s< Nsignals; s++){ printf("CH %i\n", CHsignal[s]); if( CHsignal[s] < 0 ) return 0;}
    for(int s=0; s< Nsignals; s++)CurEvent->GetChannel(CHsignal[s])->GetSignal(&(MAIN_s[s]));


    for(int ch=StartSignal;ch<CHtoShow+StartSignal;ch++)
        Hists.Add( new TH1F(Form("ampl_CH%i",CHsignal[ch]), Form("ampl_CH%i",CHsignal[ch]),200, 0,2000));

    for(int ch=StartSignal;ch<CHtoShow+StartSignal;ch++)
        Hists.Add( new TH1F(Form("time_CH%i",CHsignal[ch]), Form("time_CH%i",CHsignal[ch]),4000, 20,60));


    int prout = 0;

    int EvCalc;
    EvCalc = NumEvent;
    //EvCalc = 10000;

    if(EvCalc > NumEvent)EvCalc = NumEvent;

    printf("Total events: %i\n", NumEvent);
    printf("Events to show: %i\n", EvCalc);

    for(int nEvent = 0; nEvent < EvCalc; nEvent+=1){
        if( nEvent%31==0){
        	printf("processed %i (%.0f\%%)\r" ,nEvent, floor((float)nEvent/(float)EvCalc*100.));
        	std::cout<<std::flush;
        };

        if(prout) printf("-------------------------------------------\n");
        CurEvent->SetEventNumber(nEvent);

        for(int s=0; s<Nsignals; s++) CurEvent->GetChannel(CHsignal[s])->FindSignalParametrs();

        float STARTtimePMTGR0 = (MAIN_s[0]->Time_pol1 + MAIN_s[1]->Time_pol1)*0.5;
        float STARTtimePMTGR1 = (MAIN_s[2]->Time_pol1 + MAIN_s[3]->Time_pol1)*0.5;

        if((STARTtimePMTGR0 == 0)||(STARTtimePMTGR1 == 0)) continue;

        for(int ch =StartSignal; ch< NCHinGr+StartSignal; ch++){

            float SIGNALMCPtime = MAIN_s[ch]->Time_pol1;
            float SIGNALMCPamplitude = MAIN_s[ch]->Amplitude;

            float SIGNALCFDtime = MAIN_s[ch+NCHinGr]->Time_pol1;
            float SIGNALCFDamplitude = MAIN_s[ch+NCHinGr]->Amplitude;

            float SIGNALrealMCPTime = (CHsignal[ch] < 8)?(SIGNALMCPtime - STARTtimePMTGR0):(SIGNALMCPtime - STARTtimePMTGR1);
            float SIGNALrealCFDTime = (CHsignal[ch+NCHinGr] < 8)?(SIGNALCFDtime - STARTtimePMTGR0):(SIGNALCFDtime - STARTtimePMTGR1);

            if(SIGNALMCPamplitude < MCPamplSelection) continue; //show CFDampl >= 50
            if(SIGNALCFDamplitude < 400) continue; //show CFDampl >= 50

            //if((SIGNALamplitude<400)||(700<SIGNALamplitude)) continue;
            //if((SIGNALrealTime>5)||(3>SIGNALrealTime)) continue;

            ( (TH1*)Hists.FindObject(Form("ampl_CH%i",CHsignal[ch])) )->Fill(SIGNALMCPamplitude);
            ( (TH1*)Hists.FindObject(Form("time_CH%i",CHsignal[ch])) )->Fill(SIGNALrealMCPTime);
            ( (TH1*)Hists.FindObject(Form("ampl_CH%i",CHsignal[ch+NCHinGr])) )->Fill(SIGNALCFDamplitude);
            ( (TH1*)Hists.FindObject(Form("time_CH%i",CHsignal[ch+NCHinGr])) )->Fill(SIGNALrealCFDTime);
        }

    }

    const float FitGange = 2.;
    TF1 *GausFunc = new TF1("FitFunc", "gaus");

    printf("\n\n------------------------------------------------\n");
    printf("\n\nMCP amplitude selection: %.0fmV\n",MCPamplSelection);
    printf("CFD mean,ns sigma,ps\n");

    for(int ch =StartSignal; ch< NCHinGr+StartSignal; ch++){

        TH1* currCFDtimeHist = (TH1*)Hists.FindObject(Form("time_CH%i",CHsignal[ch+NCHinGr]));
        float HistsMean = currCFDtimeHist->GetMean();
        float HistRMS = currCFDtimeHist->GetRMS();

        GausFunc->SetRange(HistsMean-HistRMS*FitGange,HistsMean-HistRMS*FitGange);

        currCFDtimeHist->Fit(GausFunc, "Q+","",HistsMean-HistRMS*FitGange,HistsMean-HistRMS*FitGange);
        float FitMean = GausFunc->GetParameter(1);
        float FitSigma = GausFunc->GetParameter(2);

        printf("%i    %.3f    %.1f\n",ch-StartSignal+1,FitMean,FitSigma*1000.);


    }

    printf("\n\n------------------------------------------------\n");

    HDraw(Hists, "Signal", 2,2);
    delete MainData;


    return 1;
}

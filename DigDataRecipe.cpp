#include "includes/DigDataRecipe.h"


char DigDataRecipe::ChSignalTypeName[5][MAX_NAME_LENGHT] = {"UNDEFINED", "LOGIC", "AMPLITUDE", "XY_PANEL_X", "XY_PANEL_Y"};
char DigDataRecipe::ChPolarityName[2][MAX_NAME_LENGHT] = {"POSITIVE", "NEGATIVE"};


using namespace std;

DigDataRecipe::DigDataRecipe(){
    DefaultSettings();
}

DigDataRecipe::DigDataRecipe(const char *DataPath_){
    DigDataRecipe(); LoadConfig(DataPath_);
}

DigDataRecipe::~DigDataRecipe(){

}

int DigDataRecipe::Configure(const char *DataPath_){
    Configuration.Reset();
    LoadConfig(DataPath_);
    return BuildParameters();
}

DataConfiguration* DigDataRecipe::GetConfiguration(){
    return &Configuration;
}

DataProcParameters* DigDataRecipe::GetProcParam(){
    return &ProcParameters;
}

Main_Processing_Param* DigDataRecipe::GetMainDetault(){
    return &Main_Default;
}

Amplitude_Processing_Param* DigDataRecipe::GetAmplitudeDefault(){
    return &Amplitude_Default;
}

void DigDataRecipe::ClearEssentialChList(){
    ProcParameters.essentialChannels = 0;
}

//--------------------------------------------------------
int DigDataRecipe::FingChannelByType(ChSignalType CHtype){
    if(Configuration.IsConfigured < 1 ) return -1;

    for(int ch=0; ch < MAX_CH; ch++)
        if ( Configuration.ChannelConfig[ch].ChannelSignalType == CHtype) return ch;

    //if(VERBOSE > 0)printf("DigDataRecipe::FingChannelByType::ERROR: Channel class with type \"%i\" was requested and NOT found\n",CHtype);

    return -1;

}

//--------------------------------------------------------
int DigDataRecipe::FindChannelByName(const char *CHname){
    if(Configuration.IsConfigured < 1 ) return -1;

    for(int ch=0; ch < MAX_CH; ch++)
        if ( strcmp(Configuration.ChannelConfig[ch].ChName ,CHname) == 0 ) return ch;

    if(VERBOSE > 4)printf("DigDataRecipe::FingChannelByName::ERROR: Channel class \"%s\" was requested and NOT found\n",CHname);
    return -1;

}

//--------------------------------------------------------
int DigDataRecipe::AddEssentialChannel(const char *CHname, int val){
    if(ProcParameters.essentialChannels == MAX_CH) return 0;

    ProcParameters.essentialChName[ProcParameters.essentialChannels] = CHname;
    ProcParameters.essentialValue[ProcParameters.essentialChannels] = val;
    ProcParameters.essentialChannels++;
    return 1;
}

//--------------------------------------------------------
Main_Processing_Param* DigDataRecipe::GetMainParam(int Ch){
    for(int ch = 0; ch < MAX_CH; ch++)
        if(List_Main_Param[ch].Channel == Ch) return List_Main_Param+ch;
    //if(VERBOSE > 0)printf("DigDataRecipe::GetMainParam: ERROR main proc param was NOT found for CH %i\n", Ch);
    return NULL;
}

Amplitude_Processing_Param* DigDataRecipe::GetAmplParam(int Ch){
    for(int ch = 0; ch < MAX_CH; ch++)
        if(List_Ampl_Param[ch].Channel == Ch) return List_Ampl_Param+ch;
    if(VERBOSE > 0)printf("DigDataRecipe::GetAmplParam: ERROR Ampl proc param was NOT found for CH %i\n", Ch);
    return NULL;
}

//--------------------------------------------------------
int DigDataRecipe::BuildParameters(){
    if(Configuration.IsConfigured < 1) return 0;
    Num_Main_Param = 0; //new param list overwrite old one
    Num_Ampl_Param = 0;
    for(int CH=0; CH<MAX_CH; CH++)
        if(Configuration.ChannelConfig[CH].IsEnable){

            List_Main_Param[Num_Main_Param] = Main_Default;
            List_Main_Param[Num_Main_Param].Channel = CH;

            if(List_Main_Param[Num_Main_Param].minAmplitude > List_Main_Param[Num_Main_Param].POL1.minAmplitude)
                List_Main_Param[Num_Main_Param].POL1.minAmplitude = List_Main_Param[Num_Main_Param].minAmplitude;

            if(Configuration.ChannelConfig[CH].ChannelSignalType == LOGIC ){
                List_Main_Param[Num_Main_Param].POL1.MaxChisquare = 100000;
                List_Main_Param[Num_Main_Param].POL3.MaxChisquare = 100000;
            }

            if( (Configuration.ChannelConfig[CH].ChannelSignalType == XY_PANEL_X ) ||
                    (Configuration.ChannelConfig[CH].ChannelSignalType == XY_PANEL_Y) ){
                List_Main_Param[Num_Main_Param].minAmplitude = 15.;
                List_Main_Param[Num_Main_Param].POL1.FittingON = 0;
                List_Main_Param[Num_Main_Param].POL3.FittingON = 0;
            }


            Num_Main_Param++;

            if(Configuration.ChannelConfig[CH].ChannelSignalType == AMPLITUDE){
                List_Ampl_Param[Num_Ampl_Param] = Amplitude_Default;
                List_Ampl_Param[Num_Ampl_Param].Channel = CH;
                Num_Ampl_Param++;
            }


        }

    if(Configuration.IsConfigured < 2) Configuration.IsConfigured = 2;
    return 1;
}

//--------------------------------------------------------
void DigDataRecipe::Reset(){
    Configuration.Reset();
    ProcParameters.Reset();
    Main_Default.Reset();
    Amplitude_Default.Reset();
    Num_Main_Param = 0;
    Num_Ampl_Param = 0;
    for(int ch=0; ch<MAX_CH; ch++){
        List_Main_Param[ch].Reset();
        List_Ampl_Param[ch].Reset();
    }
}

//--------------------------------------------------------
void DigDataRecipe::DefaultSettings(){

    Reset();

    // Default DataConfiguration:
    ProcParameters.AmplitudeDimension = 0.28;// amplitude dimension [mV]
    ProcParameters.AmplitudeZeroLevel = -665.;// U[mV] = 0.28*A[ch] - 665.[mV]
    ProcParameters.TimeDimension = 200./1024.;// time dimension [ns]

    ProcParameters.Statistic = 1000;
    ProcParameters.AmplRange = 3.;

    ProcParameters.FilterON = 1;			// ON
    ProcParameters.AddFrontPoints = 1;		// ON
    ProcParameters.Filter_lenght = 10;
    ProcParameters.Filter_Fs = 2;
    ProcParameters.Filter_Fx = 5;

    // Default Main_Processing_Param:
    Main_Default.minAmplitude = 15.;		// mV
    Main_Default.maxAmplitude = 1500.;		// mV
    Main_Default.minAmplGate = 10.;          //ns
    Main_Default.maxAmplGate = 180.;          //ns, if zero will be used all range
    Main_Default.MainSgnPointsInerval = 0.1;
    Main_Default.ZeroLevelmaxRMS = 2.;		// mV

    Main_Default.POL1.FittingON = 1;
    sprintf(Main_Default.POL1.parameter,"QNFR");
    Main_Default.POL1.minAmplitude = 1.;	//mV
    Main_Default.POL1.MaxChisquare = 100000.;
    Main_Default.POL1.ChiRange = 3.;
    Main_Default.POL1.SigmaFitFunc = 0.0001;	//ns
    Main_Default.POL1.BorderFitRejTime = 0.15;

    Main_Default.POL3.FittingON = 0;
    sprintf(Main_Default.POL3.parameter,"QNFR");
    Main_Default.POL3.minAmplitude = 5.;	//mV
    Main_Default.POL3.MaxChisquare = 2000.;
    Main_Default.POL3.ChiRange = 1.;
    Main_Default.POL3.SigmaFitFunc = 0.0001;	//ns
    Main_Default.POL3.BorderFitRejTime = 0.15;

    //Deault Amplitude_Processing_Param:
    Amplitude_Default.isPrecCalc = 0.;
    Amplitude_Default.treshold = 200.;
    Amplitude_Default.CFD_dt =    1; //(ns) dt > tr*(1-f) tr - time of front reasing; f - easing coefficient
    Amplitude_Default.CFD_fV =    0.2;

}

//--------------------------------------------------------
void DigDataRecipe::PrintOut(int level){

    printf("\n-------------- PRINTOUT Data Recipe ---------------------\n");
    if(!Configuration.IsConfigured)printf("Data is not configured !!!\n");
    printf("Data name:                     %s\n", Configuration.DataName);
    printf("Run comment:                   %s\n", Configuration.RunComment);
    printf("Data file path:                %s\n", Configuration.DataPath);
    printf("Number of connected channels:  %d\n", Configuration.NactiveCh);
    printf("Number of channels to include: %d\n", Configuration.NenableCh);
    printf("Data header:                   %i\n", Configuration.IsReadHeader);
    printf("Data is configured:            ");
    if(Configuration.IsConfigured == 0)printf("NOT CONFIGURED\n");
    if(Configuration.IsConfigured == 1)printf("CONFIGURATION LOADED\n");
    if(Configuration.IsConfigured == 2)printf("PROC PARAM LIST FILLED\n");
    if(Configuration.IsConfigured == 3)printf("EVENT CONSTRUCTED\n");
    if(Configuration.IsConfigured == 4)printf("PROC PARAM CALCULADED\n");
    if(Configuration.IsConfigured >= 0){ printf("\nData processing configuration:\n"); ProcParameters.PrintOut();}

    ChConfiguration *temp_ptr = Configuration.ChannelConfig;
    for(int ch=0; ch<MAX_CH; ch++)
        if( (level > 1) || (temp_ptr[ch].ChannelSignalType != UNDEFINED))
            if( (level > 0) || (temp_ptr[ch].IsEnable != 0))
                if(temp_ptr[ch].IsEnable || !Configuration.IsConfigured){
                    printf("\nCHANNEL %d -----------------\n",ch);
                    printf("Name:      %s\n", temp_ptr[ch].ChName);
                    printf("Comment:   %s\n", temp_ptr[ch].ChComment);
                    printf("File name: %s\n", temp_ptr[ch].FileName);
                    printf("Signal is include: %d\n", temp_ptr[ch].IsEnable);
                    printf("Signal type:       %s\n", ChSignalTypeName[ temp_ptr[ch].ChannelSignalType ]);
                    printf("Signal polarity:   %s\n", ChPolarityName[ temp_ptr[ch].ChannelPolarity ]);
                    if(Configuration.IsConfigured >= 2){
                        printf("\n");
                        Main_Processing_Param *MPP_ptr = GetMainParam(ch); if(MPP_ptr)MPP_ptr->PrintOut();
                        printf("\n");
                        Amplitude_Processing_Param *APP_ptr = GetAmplParam(ch); if(APP_ptr)APP_ptr->PrintOut();
                    }
                }
    printf("-----------------------------------------------------------\n\n");

}

//--------------------------------------------------------
int DigDataRecipe::LoadConfig(const char *DataPath_){

    char fconfname[MAXLINE];
    string Pname = DataPath_;
    int PathLen = Pname.length();
    int LastFolder = Pname.find_last_of('/');

    if(VERBOSE > 0) printf("DigDataRecipe::LoadConfig: Configuring fromS %s\n",Pname.c_str());

    if(LastFolder == PathLen-1) Pname.erase(LastFolder,1); //     /.path/   --->    /path

    if(Pname.find(".txt") == std::string::npos){              //     /.path
        strcpy(Configuration.DataPath, Pname.c_str());
        strcpy(fconfname,Form("%s/DataConfig.txt",Configuration.DataPath));
    }else{                                               //     /.path/Dc.txt
        strcpy(fconfname,Pname.c_str());
        Pname.erase(LastFolder,PathLen - LastFolder);
        strcpy(Configuration.DataPath, Pname.c_str());
    }

    FILE *f_config = fopen(fconfname,"rt");
    if(f_config == NULL){
        if(VERBOSE > 0) printf("DigDataRecipe::LoadConfig: Configuration file \"%s\" was NOT opened\n",fconfname);
        return -1;
    }
    if(VERBOSE > 0) printf("DigDataRecipe::LoadConfig: Configuration file \"%s\" was opened\n",fconfname);


    string DataName_, DataComment_, ChComment_;
    int CurCh = 0;
    Configuration.NactiveCh = 0;
    Configuration.NenableCh = 0;
    ChConfiguration *temp_ptr = Configuration.ChannelConfig;

    int state_reading = 0; //1-runname 2-runcomments 3-ChTable
    int state_reading_table = 0; //number of colum in channel table
    const int num_col = 6; //number of colums

    while(!feof(f_config)) {
        char buffer[MAXLINE]; int read; char *result;

        // read a word from the files and skip comments
        read = fscanf(f_config, "%s", buffer);
        if( !read || (read == EOF) || !strlen(buffer)) continue;
        if(buffer[0] == '#') {result = fgets(buffer, 1000, f_config); continue; }

        // recognize key command and setup state_reading
        if (buffer[0] == '[') {
            if (strstr(buffer, "RUNNAME")) {
                result = fgets(buffer , MAXLINE, f_config);
                state_reading = 1;
                continue;}

            if (strstr(buffer, "DATACOMMENTS")) {
                result = fgets(buffer , MAXLINE, f_config);
                state_reading = 2;
                continue;}

	    if (strstr(buffer, "CHANNELSTABLE")) {
                result = fgets(buffer , MAXLINE, f_config);
                state_reading = 3;
                continue;}
	    
	    if (strstr(buffer, "READHEADER")) {
                result = fgets(buffer , MAXLINE, f_config);
		Configuration.IsReadHeader = 1;
                continue;}
        }
        result++;
        result--;
        //reading information
        if(state_reading == 1){ if(DataName_.size()!=0)DataName_ += " ";DataName_ += buffer;} //collect name by words
        if(state_reading == 2){ if(DataComment_.size()!=0)DataComment_ += " ";DataComment_ += buffer;} //collect comment by words
        if(state_reading == 3){ //reading columns by words (one word one column exetp ch_comment)

            state_reading_table++; if(state_reading_table > num_col) state_reading_table = 1;
            if(state_reading_table == 1){CurCh = atoi(buffer);};
            if(state_reading_table == 2){temp_ptr[CurCh].IsEnable = atoi(buffer);if(temp_ptr[CurCh].IsEnable) Configuration.NenableCh++;};
            if(state_reading_table == 3){temp_ptr[CurCh].ChannelPolarity = (ChPolarityType)atoi(buffer);};
            if(state_reading_table == 5){strcpy(temp_ptr[CurCh].ChName, buffer);};

            if(state_reading_table == 4){
                temp_ptr[CurCh].ChannelSignalType = (ChSignalType)atoi(buffer);
                if(temp_ptr[CurCh].ChannelSignalType != UNDEFINED){
                    Configuration.NactiveCh++;  //if channel is active set file_name and total_active_events
                    if(CurCh < 16) strcpy(temp_ptr[CurCh].FileName, Form("%s/wave_%d.dat", Configuration.DataPath,CurCh));
                    else           strcpy(temp_ptr[CurCh].FileName, Form("%s/TR_0_%d.dat", Configuration.DataPath,CurCh-16));
                };
            };

            if(state_reading_table == 6){
                if(ChComment_.size()!=0)ChComment_ += " ";
                ChComment_ += buffer;
                result = fgets(buffer, 1000, f_config); //finish reading line to collect ch_comment entirely
                ChComment_ += buffer;
                ChComment_.erase( ChComment_.find('\n'), 1 );
                strcpy(temp_ptr[CurCh].ChComment, ChComment_.c_str());
                ChComment_ = "";
            };

        };

    } //while()
    strcpy(Configuration.DataName, DataName_.c_str());
    strcpy(Configuration.RunComment ,DataComment_.c_str());

    fclose (f_config); if(VERBOSE > 0)printf("DigDataRecipe::LoadConfig: File \"%s\" was closed\n",fconfname);

    Configuration.IsConfigured = 1;

    return 1;
}

//--------------------------------------------------------
int DigDataRecipe::DeleteUnusedDataFiles()
{
    if(VERBOSE > 0) printf("\nDeleting unused digitizer data files ----------------------\n");
    if(Configuration.IsConfigured < 1){ if(VERBOSE > 0) printf("DigDataRecipe::DeleteUnusedDataFiles: Configurations are NOT loaded\n"); return 0;}
    else if(VERBOSE > 0) printf("Loaded %i data files from \"%s\":\n", Configuration.NenableCh, Configuration.DataPath);

    for(Int_t ch = 0; ch < MAX_CH; ch++)
    {
        if(!Configuration.ChannelConfig[ch].IsEnable) continue;
        if(VERBOSE > 0) printf("CH%i    [%s]    \"%s\"\n", ch, ChSignalTypeName[Configuration.ChannelConfig[ch].ChannelSignalType], Configuration.ChannelConfig[ch].FileName);
    }


    TString filespath = Configuration.DataPath;
    if(!filespath.EndsWith("/")) filespath += "/";

    TObjArray FilesArr;
    TSystemFile *currFile;
    TString currDataFileName;
    TSystemDirectory SDirectory(filespath, filespath);
    TIter nextfile(SDirectory.GetListOfFiles());

    if(VERBOSE > 0) printf("\nDirectory file list:");

    Int_t nfiles = 0;
    while ((currFile=(TSystemFile*)nextfile()))
    {
        if (!currFile->IsDirectory())
        {
            if(VERBOSE > 0) printf("\n file: \"%s\" ", currFile->GetName() );

            //check if this file has data file name and not used
            for(Int_t ch = 0; ch < MAX_CH; ch++)
            {
                currDataFileName = (ch < 16)? Form("wave_%d.dat", ch) : Form("TR_0_%d.dat", ch-16);

                if( ( (TString)currFile->GetName() ).Contains( currDataFileName.Data() ) )
                {
                    if( !Configuration.ChannelConfig[ch].IsEnable)
                    {
                        FilesArr.Add( currFile );
                        if(VERBOSE > 0) printf("[is UNUSED data file]");
                    }
                    else if(VERBOSE > 0) printf("[is USED data file]");

                    break;
                }

            }//for ch




            nfiles++;
        }
    }

    if(VERBOSE > 0) printf("\n\n");

    for(Int_t file = 0; file <= FilesArr.GetLast(); file++)
    {
        printf("deleting: \"%s/%s\"  ",Configuration.DataPath, FilesArr.At(file)->GetName() );
        gROOT->ProcessLine( Form(".rm %s/%s",Configuration.DataPath, FilesArr.At(file)->GetName()) );
    }

    if(VERBOSE > 0) printf("\nDeleted %i files -------------------------------------------\n", FilesArr.GetLast());

    return FilesArr.GetLast();

    //    if(0 < FilesArr.GetLast())
    //    {
    //        if(VERBOSE > 0)
    //        {
    //            printf("\n\nWill be DELETED:\n");
    //            for(Int_t file = 0; file <= FilesArr.GetLast(); file++) printf("%s\n", FilesArr.At(file)->GetName() );
    //        } else  printf("Unused data files will be deleted\nVERBOSE mod is 0, so files list was not printed\n");

    //        printf("Please confirm deteling operation (yes/no):");
    //        Int_t answer_is_yes = 0;
    //        TString answer;

    //        while(1)
    //        {
    //            std::cin >>answer;
    //            if(answer == "yes"){ answer_is_yes = 1; break;}
    //            if(answer == "no"){ answer_is_yes = 0; break;}
    //        }

    //        for(Int_t file = 0; file <= FilesArr.GetLast(); file++)
    //        {
    //            ( (TSystemFile*)FilesArr.At(file) )->Execute("rm","");
    //            printf("%s\n", FilesArr.At(file)->GetName() );
    //        }


    //    } else if(VERBOSE > 0) printf("\n\nNO files will be DELETED:\n");

}

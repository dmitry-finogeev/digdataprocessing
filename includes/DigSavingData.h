/*
 * DigSavingData.h
 *
 *  Created on: May 22, 2016
 *      Author: mss
 */

#ifndef INCLUDES_DIGSAVINGDATA_H_
#define INCLUDES_DIGSAVINGDATA_H_

#include "DigData.h"
#include "DigDataAnalysis.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include <TStyle.h>
#include <TFile.h>

int DigSavingData(GlobalProcessStatus *gProcessState, std::string path);

#endif /* INCLUDES_DIGSAVINGDATA_H_ */

#ifndef DIGSIGNALS_H
#define DIGSIGNALS_H

/*!
 * \file
 * \brief description of DigChannel successors for differents sygnals types
 */

#include <TF1.h>
#include <TFile.h>
#include "DigChannel.h"
#include <TGraph.h>

class DigSignalLOGIC :public DigChannel{
public:

	DigSignalLOGIC(int NumChannel_, DigDataRecipe *DataRecipe_ptr_);
	~DigSignalLOGIC();

private:
	virtual void CalculateSignal();
	virtual void SetBranchSignal(TTree* DataTree);
	virtual void Reset();

	LOGIC_signal LogicSignal;

public:
	using DigChannel::GetSignal;
	int GetSignal(LOGIC_signal **Sg_param_ptr);
};



//*****************************************************************************************************
class DigSignalAMPLITUDE :public DigChannel{
public:

	DigSignalAMPLITUDE(int NumChannel_, DigDataRecipe *DataRecipe_ptr_);
	~DigSignalAMPLITUDE();

private:
	virtual void CalculateSignal();
	virtual void SetBranchSignal(TTree* DataTree);
	virtual void Reset();
    
    TF1 *ZLevel_fit;
    TF1 *Peak_fit;
    TF1 *Front_fit;


	AMPLITUDE_signal AmplitudeSignal;
	Amplitude_Processing_Param *AmplProcParam_ptr;

public:
	using DigChannel::GetSignal;
	int GetSignal(AMPLITUDE_signal **Sg_param_ptr);
    
    TF1* GetZLevel_fit();
    TF1* GetPeak_fit();
    TF1* GetFront_fitt();
};



//*****************************************************************************************************
class DigSignalPANELX :public DigChannel{
public:

	DigSignalPANELX(int NumChannel_, DigDataRecipe *DataRecipe_ptr_);
	~DigSignalPANELX();
        
private:
	virtual void CalculateSignal();
	virtual void SetBranchSignal(TTree* DataTree);
	virtual void Reset();

	PANNELX_signal PannelxSignal;

public:
	using DigChannel::GetSignal;
	int GetSignal(PANNELX_signal **Sg_param_ptr);
};



//*****************************************************************************************************
class DigSignalPANELY :public DigChannel{
public:

	DigSignalPANELY(int NumChannel_, DigDataRecipe *DataRecipe_ptr_);
	~DigSignalPANELY();
        
private:
	virtual void CalculateSignal();
	virtual void SetBranchSignal(TTree* DataTree);
	virtual void Reset();

	PANNELY_signal PannelySignal;

public:
	using DigChannel::GetSignal;
	int GetSignal(PANNELY_signal **Sg_param_ptr);
};


#endif // DIGEVENT_H

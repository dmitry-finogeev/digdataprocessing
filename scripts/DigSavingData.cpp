#include "includes/DigSavingData.h"

using namespace std;

int DigSavingData(GlobalProcessStatus *gProcessState, string path){

	printf("\n\n  ---- DigSavingData ---- \n\n");


	/***/
	DigDataAnalysis *Analysis = new DigDataAnalysis(gProcessState);
	Analysis->SetStudyEv(0);


	//Analysis->AddCrtlGroup("1MCP","1MCP1","1MCP2","1MCP3","1MCP4");
	//Analysis->AddCrtlGroup("2MCP","2MCP1","2MCP2","2MCP3","2MCP4");

	/*
ChannelGroup *CrG = Analysis->GetCrtlArr(1);
CrG[0].enabled = 1; CrG[0].CHnumber = 4; strcpy(CrG[0].GroupName, "4MCP_crtl");
for(int i=0; i<4; i++)strcpy( CrG[0].CHnames[i], Form("4MCP%i",i+1) );


CrG[1].enabled = 1; CrG[1].CHnumber = 4; strcpy(CrG[1].GroupName, "4CFD_crtl");
for(int i=0; i<4; i++)strcpy( CrG[1].CHnames[i], Form("2CFD%i",i+1) );
	 */

	Analysis->AddStartDetector("T0_C1",0);
	Analysis->AddStartDetector("T0_D1",0);
	Analysis->AddStartDetector("T0_C2",1);
	//Analysis->AddStartDetector("T0_D2",1);

	Analysis->AddStartDetector("T0_Dsc",1);
	//Analysis->AddStartDetector("TRG1");
	//Analysis->AddStartDetector("4MCP");


	Analysis->AnalyseFile(path.c_str());
//	Analysis->AnalyseFile("/media/data1/Data_Oct_2015/10102015_2052_RUN35_5MCP_1465_CFD_20mV/byDscD2_sel240mV.root");
	Analysis->SetAmplMin(240);

	//Analysis->AddCrtlGroup("CFD","5CFD1","5CFD2","5CFD3", "5CFD4");


	/*
CrosstalkGroup *CrG = Analysis->GetCrtlArr(2);
CrG[0].enabled = 1; CrG[0].CHnumber = 4; strcpy(CrG[0].GroupName, "2CFD_crtl");
for(int i=0; i<4; i++)strcpy( CrG[0].CHnames[i], Form("2CFD%i",i+1) );

CrG[1].enabled = 1; CrG[1].CHnumber = 4; strcpy(CrG[1].GroupName, "2MCP_crtl");
for(int i=0; i<4; i++)strcpy( CrG[1].CHnames[i], Form("2MCP%i",i+1) );

Analysis->AnalyseFile("CFD_1mV.root");

//TObjArray *Hists = Analysis->GetProcHists();
//HDraw(*Hists, "Data Proc", 3, 4, 1, 0, 100, 0, 0, "");
	 */



	/***
TObjArray Hists;
int Stat = 0;
DigData *Data = new DigData("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-1607-CFD-att4-4mV/");
DigDataRecipe *DataRecipe_ptr = Data->GetDataRecipe();

DataRecipe_ptr->AddEssentialChannel("T0_C1",1);
DataRecipe_ptr->AddEssentialChannel("T0_D1",1);
DataRecipe_ptr->GetProcParam()->AmplRange = 3.;
DataRecipe_ptr->GetMainDetault()->minAmplitude = 50.;
DataRecipe_ptr->GetMainDetault()->POL3.FittingON = 0;


//DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-1607-CFD-att4-4mV/");
//DataRecipe_ptr->PrintOut(1);
Data->OpenData("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-1607-CFD-att4-4mV/");
Data->OpenData();
//Data->SaveDataAsTree("t",10000);
DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-1615-MCP1_HV1600_back-V0_25mmBC_XP2020_HV1400_ch10");//RUN31
//Data->OpenData();
Data->SaveDataAsTree("RUN31",Stat);

DataRecipe_ptr->GetMainDetault()->minAmplitude = 100.;
DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/06062015-2005-MCP1-HV1600-MCP2-HV2150-V0_6cm_XP2020_HV1800_ch10");//RUN23
//Data->OpenData();
Data->SaveDataAsTree("RUN23",Stat);

DataRecipe_ptr->GetMainDetault()->minAmplitude = 50.;
DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/07062015-1052-NO-MCP2-HV1582-MCP4-HV1750-V0_6cm_XP2020_HV1600_ch10");//RUN24
//Data->OpenData();
Data->SaveDataAsTree("RUN24",Stat);


DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/11062015-1532-MCP1_HV1600"); //RUN41
//Data->OpenData();
Data->SaveDataAsTree("RUN41",Stat);

DataRecipe_ptr->GetMainDetault()->minAmplitude = 7.;
DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/12062015-2209-MCP1_20deg_HV1600-MCP2-HV2150-back"); //RUN44
//Data->OpenData();
Data->SaveDataAsTree("RUN44",Stat);



DataRecipe_ptr->GetMainDetault()->minAmplitude = 30.;

DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/09062015-1235-CFD-att4-1mV");
//Data->OpenData();
Data->SaveDataAsTree("CFD_1mV",Stat);

DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-2250-CFD-att4-2mV");
//Data->OpenData();
Data->SaveDataAsTree("CFD_2mV",Stat);

DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-1940-CFD-att4-3mV");
//Data->OpenData();
Data->SaveDataAsTree("CFD_3mV",Stat);

DataRecipe_ptr->Configure("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-1607-CFD-att4-4mV");
//Data->OpenData();
Data->SaveDataAsTree("CFD_4mV",Stat);
	 */



	//DataRecipe_ptr->PrintOut(1);
	//Data->GetProcHistArray(&Hists);

	//HDraw(Hists, "Data Proc", 5, 3, 1, 0, 100, 0, 0, "");


	//delete Data;
	/***/


	/*
TFile *File = new TFile("RUN23_.root");
DigDataRecipe *DataRecipe_ptr_l = (DigDataRecipe*)File->Get("DataRecipe");
//DigDataRecipe *DataRecipe_ptr = new DigDataRecipe;
//printf("readed %i\n",DataRecipe_ptr->Read("DataRecipe") );
DataRecipe_ptr_l->PrintOut(2);
	 **/



	//MainData->SetData("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-1940-CFD-att4-3mV");
	//MainData->SetData("/mnt/hgfs/Root/PROJECTS/FIT/Data/08062015-1607-CFD-att4-4mV");


	return 1;
}

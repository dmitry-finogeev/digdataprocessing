#ifndef DIGDATAANALYSIS_H
#define DIGDATAANALYSIS_H

/*!
 * \file
 * \brief class DigDataAnalysis description
 */

#include <stdarg.h>

#include "DigDataRecipe.h"
#include "DigVariables.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>
#include <TTree.h>
#include <TFile.h>
#include <TDirectory.h>
#include "TStyle.h"
#include <TProfile.h>

/*!
 *\brief Analysing TTree constructed by DigData
 *\details This class opens root files that was saved by DigData class,
 * and save analysed data at new one as ttree to.\n
 * In difference from raw data file, after analysing data new ttree contains:\n
 * - signals times corresponding to start signal\n
 * - times with Time-Amplitude correction\n
 * - flags for each MCP cannels like as crosstalk, XY_center, and other\n
 *
 * Also this class will draw all data at hists convenient for analysis\n
 * This class structure and procedures heavily dependent of experiment setting\n
 */



//this groupe contains names of chanels wich groupet for flag crosstalk
struct ChannelGroup{

    int enabled;
    char GroupName[MAX_NAME_LENGHT];
    char **CHnames;
    int CHnumber;

    void PrintOut();
    ChannelGroup();
    ~ChannelGroup();
};

class DigDataAnalysis{
public:
    DigDataAnalysis(GlobalProcessStatus *state);
    ~DigDataAnalysis();

private:
    GlobalProcessStatus *gProcessState;
    int EventToAnalyse;//events saved in ttree
    int EventToStudy;

    //trees from and to process data
    TTree *RawDataTree_ptr;
    //TTree *ProcDataTree_ptr;
    DigDataRecipe *DataRecipe;

    //structures for TTree; for all channels by default, but connected to branches only fited
    //Structures for reading raw data
    MAIN_signal MainSignals[MAX_CH];
    LOGIC_signal LogicSignals[MAX_CH];
    AMPLITUDE_signal AmplitudeSignals[MAX_CH];
    PANNELX_signal PanelXSignals[MAX_CH];
    PANNELY_signal PanelYSignals[MAX_CH];

    //structures for writing processed data
    AmplitudeData AmplData[MAX_CH];
    LogicalData LogicData[MAX_CH];

    //start detectors groupes
    ChannelGroup StartDetectors[2];

    //Crosstalk groupes
    ChannelGroup CrtlGroupes[MAX_CH];

    int CrtlGrNumber;
    int Crtl_Flag[MAX_CH]; //array for branches

    //time - amplitude correcton functions
    TF1 *ATfunc_pol1_ptr[MAX_CH];

    //make amplitude-time correction for channel
    int MakeATcorrectionPOL1(char *ChName);


    //function to take values fron ttree
    float StartPMTTime(int Ch);
    float LeafAmpl(int Ch);
    float LeafXcoord(int Ch);
    float LeafYcoord(int Ch);
    float LeafTime_pol1(int Ch);
    float LeafTime_pol1_ATC(int Ch);

    //values ranges
    ValueRange AmplsRange[MAX_CH];
    ValueRange TimesPOL1Ranges[MAX_CH];
    ValueRange XcoordRange[MAX_CH];
    ValueRange YcoordRange[MAX_CH];
    float AmplRange;
    float AmplMinRange;
    float TimeRange;
    float XRange;
    float YRange;

    TString crtlGrNameMask;

    void GetMeanValForLeaf(ValueRange *, float(DigDataAnalysis::*)(int), int Ch, char *name,int Bining,float MinRange, float Range,float FitRange);

    //arrays whit process and results hists
    TObjArray ProcHists;
    TObjArray ResuHists;

public:
    void SetAnalyseEv(int Events);
    void SetStudyEv(int Events);
    void SetAmplMin(int AmplMin);
    void SetAmplRange(float AmplRange_);
    void SetCRTLgroupNameMask(TString mask);

    void AddCrtlGroup(char *, ...);
    void AddStartDetector(const char *Name, int Groupe);
    ChannelGroup *GetCrtlArr(int N); //temp, AddCrtlGroup does not work :(

    int AnalyseFile(const char *FileName);
};

#endif // DIGDATAANALYSIS_H

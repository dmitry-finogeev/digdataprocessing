/*
 * DigChannel.c
 *
 *  Created on: May 11, 2016
 *      Author: mss
 */

#include "includes/DigChannel.h"

// -------------------------------------------------
void DigChannel::SetBranch(TTree* DataTree){
    uint32_t timestamp;
    uint32_t timestamp_us;
    
    //add branch of main paraneters existing for all signals
    DataTree->Branch(Form("main_%s",ChannelName), &MainSignal, "Amplitude/F:Time:Time_pol1:Time_pol3:ZeroLevel:ZeroLevelRMS:time_peak:time_begin:time_end:time_front_end:time_back_begin:Charge_GATE:timestamp/i:timestamp_us");
    
    //call function for branch of special parameters
    this->SetBranchSignal(DataTree);
}

// -------------------------------------------------
int DigChannel::CoordinateByLevel(float Level_, float lv_bias, float lv_step, bool *CRDarr){
    
    
    //printf("\n\ncoordinate level : %f\n", Level_);
    if(Level_ < MainProcParam_ptr->minAmplitude)
    {
	for(int cord = 0; cord < 5; cord++) CRDarr[cord] = false;
	return 0.;
    }
    
    int value_code = (int)ceil(  (Level_-lv_bias)/lv_step  );
    //printf("value code %i [%x]\n", value_code, value_code);
    
    for(int cord = 0; cord < 5; cord++)
    {
	CRDarr[cord] = (value_code & (1<<cord)) > 0;
	//printf("cord %i is %i\n", cord, CRDarr[cord]);
    }
    
    for(int cord = 0; cord < 5; cord++)
	if( value_code == (1<<cord) ) 
	{
	    //printf("return %i\n", cord+1);
	    return cord+1;
	}
    
    return 0;
}

int DigChannel::CoordinateByLevel_Ars(float Level_){
    
    //    source
    //    r_amp[ch_num] = (max - min);
    //    r_time[ch_num] = r_amp[ch_num]>0 ? log(abs(r_amp[ch_num])/75.)/log(2) : 0.;
    //    if(r_time[ch_num]<=0.5) {r_charge[ch_num]=-1;}
    //    if(r_time[ch_num]>0.5 && r_time[ch_num]<=1.5) {r_charge[ch_num]=1;}
    //    if(r_time[ch_num]>1.5 && r_time[ch_num]<=2.8) {r_charge[ch_num]=2;}
    //    if(r_time[ch_num]>2.8 && r_time[ch_num]<=3.85) {r_charge[ch_num]=3;}
    //    if(r_time[ch_num]>3.85 && r_time[ch_num]<=4.7) {r_charge[ch_num]=4;}
    //    if(r_time[ch_num]>4.7) {r_charge[ch_num]=5;}
    
    
    if(Level_ < MainProcParam_ptr->minAmplitude)
	return 0.;
    
    int value_code = TMath::Log( Level_/75. ) / TMath::Log(2);
    
    int result = -1;
    if(value_code<=0.5) {result=-1;}
    if(value_code>0.5 && value_code<=1.5) {result=1;}
    if(value_code>1.5 && value_code<=2.8) {result=2;}
    if(value_code>2.8 && value_code<=3.85) {result=3;}
    if(value_code>3.85 && value_code<=4.7) {result=4;}
    if(value_code>4.7) {result=5;}
    
    return result;
}




// -------------------------------------------------
void DigChannel::MeanRMScalc(float*DataArr, float* Mean, float* RMS, int begin, int end, int step){
    
    begin = (begin < 0)? 0 : begin;
    end = (end >= EVENT_PATTERN_LENGHT)? EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT-1 : end;
    if(begin > end){float swap=end;end=begin;begin=swap;};
    step = TMath::Abs(step);
    
    *Mean = *RMS = 0.; int Delta = 0;
    for(int n=begin; n<=end; n += step){ *Mean += DataArr[n]; Delta++;}
    *Mean /= (float)Delta;
    
    for(int n=begin; n<=end; n += step) *RMS += (DataArr[n] - *Mean) * (DataArr[n] - *Mean);
    *RMS = TMath::Sqrt( *RMS/((float)Delta) );
    
    //printf("AMPL %.2f, RMS %.2f\n",*Mean,*RMS);
}

// -------------------------------------------------
int DigChannel::TimeToPoints(float time){
    return floor(time / (*TimeDim_ptr) + .5/*rounding*/);
}

// -------------------------------------------------
float DigChannel::PointsToTime(int point){
    return (float)point * (*TimeDim_ptr);
}

// -------------------------------------------------
void DigChannel::Filter(){
    
    const int MaxFlenght = 100;
    int Flenght = DataRecipe_ptr->GetProcParam()->Filter_lenght; //filter lenght
    if(Flenght > MaxFlenght)Flenght = MaxFlenght;
    float Fd = EVENT_PATTERN_LENGHT; //The sampling rate of the input data
    float Fs = DataRecipe_ptr->GetProcParam()->Filter_Fs; //Frequency bandwidth
    float Fx = DataRecipe_ptr->GetProcParam()->Filter_Fx; //The frequency band attenuation
    
    float H[MaxFlenght] = {0}; //The impulse response filter
    float Hid_i = 0; //The ideal impulse response
    float Wi = 0; //weighting function
    
    float Fc = (Fs + Fx) / (2 * Fd);
    //float M_PI = 3.141592653;
    
    for (int i=0; i < Flenght; i++){
	
	if (i==0) Hid_i = 2*M_PI*Fc;
	else Hid_i= TMath::Sin(2*M_PI*Fc*i )/(M_PI*i);
	
	// Blackman weighting function
	Wi = 0.42 - 0.5 * TMath::Cos((2*M_PI*i) /( Flenght-1)) + 0.08 * TMath::Cos((4*M_PI*i) /( Flenght-1));
	
	H[i] = Hid_i * Wi;
    }
    
    //normalization
    float SUM = 0.;
    for (int i=0; i < Flenght; i++) SUM += H[i];
    for (int i=0; i < Flenght; i++) H[i] /= SUM;
    
    //filter
    for (int point = 0; point < EVENT_PATTERN_LENGHT-Flenght; point++) {
	
	float Acur = 0;
	for (int Fi = 0; Fi < Flenght; Fi++)
	    Acur += H[Fi] * DataBuffer[point + Fi];
	DataBuffer[point] = Acur;
    }
    
}

// -------------------------------------------------
void DigChannel::FindZeroLevel(){
    
    int begin_search = 0;
    int end_search = EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT;
    int centr_search;
    float Mean1, RMS1, Mean2, RMS2;
    float AccuracyRMS = MainProcParam_ptr->ZeroLevelmaxRMS;
    //printf("start ZL  %s\n", ChannelName);
    for(int Iteration = 1; Iteration <= 6; Iteration++){
	centr_search = (int)(   floor( (end_search+begin_search)/2. )   );
	//printf("---\n");
	MeanRMScalc(DataBuffer, &Mean1, &RMS1, begin_search, centr_search, 1);
	MeanRMScalc(DataBuffer, &Mean2, &RMS2, centr_search, end_search, 1);
	//printf("[%d	%d	%d]",begin_search,centr_search,end_search);
	//printf("[Mean1 %.2f, Mean2 %.2f, DELTA Mean %.2f, RMS1 %.2f, RMS2 %.2f, DELTA RMS %.2f]\n",Mean1, Mean2,  TMath::Abs(Mean1-Mean2), RMS1, RMS2,TMath::Abs(RMS1-RMS2));
	
	//if mean difference is too low, chose sector with less RMS
	if( TMath::Abs(Mean1-Mean2) < 100* (*AmplitudeDim_ptr) ){
	    
	    if( RMS1 <= RMS2 ){end_search = centr_search;}else{begin_search = centr_search;}
	    
	    //else chose the sector with minimal mean accordance to polarity
	}else{
	    
	    if( (Mean1 - Mean2)*POLARITY < 0 ){end_search = centr_search;}else{begin_search = centr_search;}
	    
	};
	
	if(RMS1 < AccuracyRMS) break;
	if(RMS2 < AccuracyRMS){ Mean1 = Mean2; RMS1 = RMS2; break;}
	
    };
    
    MainSignal.ZeroLevel = Mean1;
    MainSignal.ZeroLevelRMS = RMS1;
    //printf("CH%d, ZeroLevel %.2f; RMS1 %.2f, Mean2 %.2f,  RMS2 %.2f\n",ThisNumChannel, Signal_parameters.Zero_level, RMS1,Mean2, RMS2);
    
}

// -------------------------------------------------

float DigChannel::LevelByTPoints(float X1, float Y1, float X2, float Y2, float Y0){
    
    //            [X2, Y2] 0
    //                   *
    //                 *
    //   Y0--------- 0
    //             * X0 (returned)
    //  [X1, Y1] 0
    
    return (X1*Y0 - X1*Y2 - X2*Y0 + X2*Y1)/(Y1-Y2);
    
}
// -------------------------------------------------
float DigChannel::GetFuncXbyY(TF1 *func, float Y_sought_for, float Sigma, float X1, float X2){ // for monotone function
    
    int iter = 0, inc;
    float X_centr, Y_centr, Y_Delta;
    if(X2 < X1){float swap = X1; X1 = X2; X2 = swap;};
    if(func->Eval(X1) < func->Eval(X2))inc = -1; else inc = 1;
    
    do{
	X_centr = (X1+X2)/2.;
	Y_centr = func->Eval( X_centr );
	Y_Delta = Y_centr-Y_sought_for;
	if(TMath::Abs(Y_Delta) < Sigma) break;
	if(0 > Y_Delta*inc){X2 = X_centr;}else{X1 = X_centr;} iter++;
    }while (iter < 100);
    
    return X_centr;
}

// -------------------------------------------------
float DigChannel::GoToLevel(float *DataArr, float Level, int *point, int iterator){
    //printf("\n\n-----\n");
    if(IS_VOID_VALUE(*point))return VOID_VALUE;
    float ResultTime;
    while( (*point>=0)&&(*point<EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT-1) ){
	
	if( (Level-DataArr[*point])*(Level-DataArr[*point+iterator]) <= 0  ){
	    ResultTime = LevelByTPoints((float)(*point),DataArr[*point],(float)(*point+iterator),DataArr[*point+iterator],Level);
	    if(ResultTime < 0.1){*point = VOID_VALUE; return VOID_VALUE;}
	    return ResultTime;
	}
	//printf("point %i, ampl1 %.2f ampl2 %.2f\n",*point,Level-DataArr[*point],Level-DataArr[*point+iterator]);
	*point += iterator;
    } //while
    
    *point = VOID_VALUE;
    return VOID_VALUE;
}

// -------------------------------------------------
void DigChannel::InsertAdFrontPoints(){
    
    if(IS_VOID_VALUE(peak_front_stop_p)||IS_VOID_VALUE(peak_back_start_p)) return;
    
    int n_shift = peak_front_stop_p-peak_start_p+1;
    if(n_shift <= AD_FIT_POINTS_LENGHT){
	
	float Ampl_p = 0.;
	float Time_p = 0.;
	
	//calculate new front points
	int p = peak_start_p;
	for(int point = peak_start_p-1; point <= peak_front_stop_p; point++){
	    Ampl_p = DataBuffer[point];
	    Time_p = point *  (*TimeDim_ptr) ;
	    SignalShapeGraph->SetPoint(p, Time_p, Ampl_p);
	    p++;
	    
	    Ampl_p = (DataBuffer[point] + DataBuffer[point+1])*0.5;
	    Time_p =  (point + 0.5) *  (*TimeDim_ptr) ;
	    SignalShapeGraph->SetPoint(p, Time_p, Ampl_p);
	    p++;
	}
	
	for(int point = peak_front_stop_p; point < EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT; point++){
	    Ampl_p = DataBuffer[point];
	    Time_p = point *  (*TimeDim_ptr) ;
	    SignalShapeGraph->SetPoint(p, Time_p, Ampl_p);
	    p++;
	}
	
	
    }
    
}

// -------------------------------------------------
int DigChannel::FindSignalAmplitude(){
    if( !(AmplValid < 0) ) return AmplValid;
    
    MainSignal.timestamp = timestamp_;
    MainSignal.timestamp_us = timestamp_us_;
    
    FindZeroLevel();
    
    //check for amplitude overload
    Bool_t is_overload = false;
    for(int nPnt=0; nPnt <= EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT-1; nPnt++)
    {
	if( (DataBuffer[nPnt] < -2000) || (DataBuffer[nPnt] > 2000) )
	{
	    is_overload = true;
	    break;
	}
    }
    
    if(is_overload)
	return -1;
    
    
    MaxAmplitude =  -10000;
    MinAmplitude = 10000;
    
    //	for(int nPoint = 0; nPoint < EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT; nPoint++){
    for(int nPoint = TimeToPoints(MainProcParam_ptr->minAmplGate); nPoint < TimeToPoints(MainProcParam_ptr->maxAmplGate); nPoint++){
	if(MaxAmplitude < DataBuffer[nPoint]){MaxAmplitude = DataBuffer[nPoint]; MaxAmpl_p = nPoint;}
	if(MinAmplitude > DataBuffer[nPoint]){MinAmplitude = DataBuffer[nPoint]; MinAmpl_p = nPoint;}
    }
    
    //amplitude = max (min) ampl dependace of polarity
    if(POLARITY > 0){MainSignal.Amplitude = MaxAmplitude-MainSignal.ZeroLevel; peak_max_p = MaxAmpl_p;}
    else {MainSignal.Amplitude = MainSignal.ZeroLevel-MinAmplitude; peak_max_p = MinAmpl_p;}
    
    AmplValid = (MainSignal.Amplitude >= MainProcParam_ptr->minAmplitude)&&(MainSignal.Amplitude <= MainProcParam_ptr->maxAmplitude);
    if(!AmplValid) MainSignal.Amplitude = VOID_VALUE;
    //if(ThisNumChannel == 0) printf("ampl = %.1f; valid = %i\n",MainSignal.Amplitude,AmplValid);
    return AmplValid;
}

// -------------------------------------------------
int DigChannel::FindSignalParametrs(){
    
    /*
//temporal peak parameters
float MaxAmplitude;
float MinAmplitude;
int MaxAmpl_p; // point at data array                //                    0 peak_max
int MinAmpl_p; //point of max & min                  // peak_front_end  0      0   peak_back_start
int peak_max_p;						                 //               *           *
int peak_start_p;				                	 //             *                *
int peak_stop_p;				                	 //            *                     *
int peak_front_stop_p;// point of front end of peak  //           *                          *
int peak_back_start_p;// point of start back of peak //          *                               *
int POLARITY;						                 //         0  peak_start                           0   peak_end
						     //     * *                                               *           *
						     
float time_peak;
float time_begin;
float time_end;
float time_front_end;
float time_back_begin;
float Charge_GATE;
	 */
    
    if(EventCalculated) return 1;
    if( FindSignalAmplitude() < 1) return 0;
    
    //charge integral in gate
    MainSignal.Charge_GATE = SignalIntegral( TimeToPoints(MainProcParam_ptr->minAmplGate) , TimeToPoints(MainProcParam_ptr->maxAmplGate) );
    
    MainSignal.time_peak = peak_max_p*(*TimeDim_ptr);
    float A_lev = MainProcParam_ptr->MainSgnPointsInerval;
    float Ampl01 = MainSignal.ZeroLevel + POLARITY*MainSignal.Amplitude*A_lev;
    float Ampl05 = MainSignal.ZeroLevel + POLARITY*MainSignal.Amplitude*0.5;
    float Ampl09 = MainSignal.ZeroLevel + POLARITY*MainSignal.Amplitude*(1.-A_lev);
    
    //printf("peak begin, ampl01 %.1f",Ampl01);
    
    //time and point of peak begin
    peak_start_p = peak_max_p;
    MainSignal.time_begin = GoToLevel(DataBuffer, Ampl01,  &peak_start_p, -1);
    if(!IS_VOID_VALUE(MainSignal.time_begin)) MainSignal.time_begin *= *TimeDim_ptr;
    //char key = getchar();
    
    //time and point of peak end
    peak_stop_p = peak_max_p;
    MainSignal.time_end = GoToLevel(DataBuffer, Ampl01,  &peak_stop_p, +1);
    if(!IS_VOID_VALUE(MainSignal.time_end)) MainSignal.time_end *= *TimeDim_ptr;
    
    //time and point of peak front end
    peak_front_stop_p = peak_start_p;
    MainSignal.time_front_end = GoToLevel(DataBuffer, Ampl09,  &peak_front_stop_p, +1);
    if(!IS_VOID_VALUE(MainSignal.time_front_end)) MainSignal.time_front_end *= *TimeDim_ptr;
    
    //time and point of back front end
    peak_back_start_p = peak_stop_p;
    MainSignal.time_back_begin = GoToLevel(DataBuffer, Ampl09,  &peak_back_start_p, -1);
    if(!IS_VOID_VALUE(MainSignal.time_back_begin)) MainSignal.time_back_begin *= *TimeDim_ptr;
    
    //front time
    float front_begin = MainSignal.time_begin;
    float front_end = MainSignal.time_front_end;
    
    if(IS_VOID_VALUE(front_begin) || IS_VOID_VALUE(front_end))  MainSignal.Time = VOID_VALUE;
    else
    {
	MainSignal.Time = (front_begin+front_end)*0.5;
	if(MainSignal.Time < 0.001) MainSignal.Time = VOID_VALUE;
	
    }
    
    //fitting: pol1 and pol3
    if(DataRecipe_ptr->GetProcParam()->AddFrontPoints)InsertAdFrontPoints();
    
    //front time by fitting pol1
    if(!IS_VOID_VALUE(MainSignal.Time))
	if(MainProcParam_ptr->POL1.FittingON)
	    if( (MainSignal.Amplitude > MainProcParam_ptr->POL1.minAmplitude) && (peak_front_stop_p-peak_start_p >= 3) ){
		//TF1 SignalFront_pol1("SignalFront_POL1","pol1");
		//SignalFront_pol1->SetRange(front_begin, front_end);
		SignalShapeGraph->Fit(SignalFront_pol1, MainProcParam_ptr->POL1.parameter,"", front_begin, front_end);
		MainSignal.Time_pol1 = GetFuncXbyY(SignalFront_pol1, Ampl05, MainProcParam_ptr->POL1.SigmaFitFunc, front_begin, front_end);
		//in case of wrong fitting time_pol3 is on ending of front
		//to separate it, time_pol3 must be near linear time
		float time_max_delta = (front_end-front_begin)*(0.5 - MainProcParam_ptr->POL1.BorderFitRejTime);
		if( TMath::Abs(MainSignal.Time_pol1 - MainSignal.Time) > time_max_delta ) MainSignal.Time_pol1 = VOID_VALUE;
		float Pol_chi = SignalFront_pol1->GetChisquare(); if( (Pol_chi > MainProcParam_ptr->POL1.MaxChisquare) ) MainSignal.Time_pol1 = VOID_VALUE;
		//if(ThisNumChannel == 0) printf("CH %i Chi_pol1 = %.4f\n",ThisNumChannel, Pol_chi);
	    }
    
    //front time by fitting pol3
    MainSignal.Time_pol3 = 0;
    if(!IS_VOID_VALUE(MainSignal.Time))
	if(MainProcParam_ptr->POL3.FittingON)
	    if( (MainSignal.Amplitude > MainProcParam_ptr->POL3.minAmplitude) && (peak_front_stop_p-peak_start_p >= 3) ){
		//TF1 SignalFront_pol3("SignalFront_POL3","pol3");
		SignalShapeGraph->Fit(SignalFront_pol3, MainProcParam_ptr->POL3.parameter,"", front_begin, front_end);
		MainSignal.Time_pol3 = GetFuncXbyY(SignalFront_pol3, Ampl05, MainProcParam_ptr->POL3.SigmaFitFunc, front_begin, front_end);
		//in case of wrong fitting time_pol3 is on ending of front
		//to separate it, time_pol3 must be near linear time
		float time_max_delta = (front_end-front_begin)*(0.5 - MainProcParam_ptr->POL3.BorderFitRejTime);
		if( TMath::Abs(MainSignal.Time_pol3 - MainSignal.Time) > time_max_delta ) MainSignal.Time_pol3 = VOID_VALUE;
		float Pol_chi = SignalFront_pol3->GetChisquare(); if( (Pol_chi > MainProcParam_ptr->POL3.MaxChisquare) ) MainSignal.Time_pol3 = VOID_VALUE;
		//if(ThisNumChannel == 7) printf("CH %i Chi_pol3 = %.4f\n",ThisNumChannel, Pol_chi);
	    }
    
    //printf("EVENT %d,	CH%d	 [ampl%.0f,	 zerol %.0f,	 begin %d,	 end %d,	pflenght %d]\n",
    //       CurEvent, ThisNumChannel, SignalAmplitude, ZeroLevel, peak_start, peak_end, peak_front_end-peak_start);
    
    this->CalculateSignal();
    
    EventCalculated = 1;
    return 1;
}

// -------------------------------------------------
DigChannel::DigChannel(int NumChannel_, DigDataRecipe *DataRecipe_ptr_){
    ThisNumChannel = NumChannel_;
    DataRecipe_ptr = DataRecipe_ptr_;
    MainProcParam_ptr = DataRecipe_ptr->GetMainParam(ThisNumChannel);
    AmplitudeDim_ptr = &(DataRecipe_ptr->GetProcParam()->AmplitudeDimension);
    TimeDim_ptr = &(DataRecipe_ptr->GetProcParam()->TimeDimension);
    ChConfiguration *CHConf_ptr = DataRecipe_ptr->GetConfiguration()->ChannelConfig+ThisNumChannel;
    POLARITY = (CHConf_ptr->ChannelPolarity == POSITIVE)? 1. : -1.;
    strcpy(ChannelName, CHConf_ptr->ChName);
    
    CheckParCorrect();
    
    //DemoCanv = new TCanvas("Demonstration","Demonstration");
    SignalShapeGraph = new TGraph();
    SignalFront_pol1 = new TF1("SignalFront_pol1","pol1");
    SignalFront_pol3 = new TF1("SignalFront_pol3","pol3");
    SignalShapeGraph->Set(EVENT_PATTERN_LENGHT+AD_FIT_POINTS_LENGHT); //aditional points for front fitting
    SignalShapeGraph->SetMaximum(1000.);
    SignalShapeGraph->SetMinimum(-1000.);
    SignalShapeGraph->SetName(Form("Graph CH%d",ThisNumChannel));
    SignalShapeGraph->SetTitle(Form("CH%d \"%s\"(%s) [%s];ns;mV",ThisNumChannel, ChannelName, (POLARITY > 0)? "+":"-", CHConf_ptr->ChComment));
    
    SignalShapeGraph->GetXaxis()->SetTitle("Time [ns]");
    SignalShapeGraph->GetYaxis()->SetTitle("Voltage [mV]");
    
    DataFile = fopen(CHConf_ptr->FileName,"rb");
    if(DataFile == NULL){
	
	if(VERBOSE > 0) printf("DigChannel:: data file \"%s\" was NOT opened\n",CHConf_ptr->FileName);
	
    }else{
	
	if(VERBOSE > 0) printf("DigChannel:: data file \"%s\" was opened for ch %d\n",CHConf_ptr->FileName,ThisNumChannel);
	
    };
    
    if(VERBOSE > 0) printf("DigChannel::Number of events: %d\n",GetTotalEvents());
    
}

DigChannel::~DigChannel(){
    if(VERBOSE > 1)printf("class DigChannel deconstructed for CH%d\n", ThisNumChannel);
    fclose(DataFile);
    if(SignalShapeGraph)delete SignalShapeGraph;
    if(SignalFront_pol1)delete SignalFront_pol1;
    if(SignalFront_pol3)delete SignalFront_pol3;
}

// -------------------------------------------------
int DigChannel::SetEvent(int NumEvent){
    
    //ChConfiguration *CHConf_ptr = DataRecipe_ptr->GetConfiguration()->ChannelConfig+ThisNumChannel;
    int size;     uint32_t BinHeader[7];
    bool is_header = DataRecipe_ptr->GetConfiguration()->IsReadHeader;
    
    for(int point = 0; point < EVENT_PATTERN_LENGHT; point++) DataBuffer[point] = VOID_VALUE;
    
    memset(DataFile, 0, sizeof(DataFile));
    fseek(DataFile, NumEvent*(sizeof(float)*EVENT_PATTERN_LENGHT+sizeof(*BinHeader)*7*is_header), SEEK_SET);
    
    if(is_header)
    {
	size = fread(BinHeader, sizeof(*BinHeader), 7, DataFile);
	
	timestamp_ = BinHeader[5];
	timestamp_us_ = BinHeader[6];
//	    printf("size           h[0] = %i\n", BinHeader[0]);
//	    printf("BoardId        h[1] = %i\n", BinHeader[1]);
//	    printf("Pattern        h[2] = %i\n", BinHeader[2]);
//	    printf("ch             h[3] = %i\n", BinHeader[3]);
//	    printf("Channel             = %i\n", ThisNumChannel);
//	    printf("EventCounter   h[4] = %i\n", BinHeader[4]);
//	    printf("Event               = %i\n", NumEvent);
//	    printf("Time stamp     h[5] = %d\n", BinHeader[5]);
//	    printf("time us        h[6] = %d\n", BinHeader[6]);    
    }
    
    
    
    
    size = fread(DataBuffer, 4, EVENT_PATTERN_LENGHT, DataFile);
    CurEvent = NumEvent;
    MaxAmplitude =  -10000;
    MinAmplitude = 10000;
    
    if(DataRecipe_ptr->GetProcParam()->FilterON)Filter();
    
    for(int nPoint = 0; nPoint < EVENT_PATTERN_LENGHT; nPoint++){
	
	DataBuffer[nPoint] = DataBuffer[nPoint]* (*AmplitudeDim_ptr) + DataRecipe_ptr->GetProcParam()->AmplitudeZeroLevel;
	SignalShapeGraph->SetPoint(nPoint, (float)nPoint* (*TimeDim_ptr) , DataBuffer[nPoint]);
	
    }
    
    float last_time = (float)(EVENT_PATTERN_LENGHT-1)* (*TimeDim_ptr);
    float last_ampl = DataBuffer[EVENT_PATTERN_LENGHT-1];
    
    // filling aditional points for front
    for(int nPoint = EVENT_PATTERN_LENGHT; nPoint < EVENT_PATTERN_LENGHT + AD_FIT_POINTS_LENGHT; nPoint++)
	SignalShapeGraph->SetPoint(nPoint, last_time, last_ampl);
    
    
    //reset signals variables for new event
    EventCalculated = 0;
    AmplValid = -1;
    MaxAmplitude = MinAmplitude = 0.;
    peak_max_p = peak_start_p = peak_stop_p = peak_front_stop_p = peak_back_start_p = 0.;
    
    this->Reset();
    //MainSignal.Reset();
    
    //if(CHConf_ptr->ChannelSignalType == LOGIC){LOGIC_signal *s_ptr; this->GetSignal(&s_ptr);s_ptr->Reset(); }
    //if(CHConf_ptr->ChannelSignalType == AMPLITUDE){AMPLITUDE_signal *s_ptr; this->GetSignal(&s_ptr);s_ptr->Reset(); }
    //if(CHConf_ptr->ChannelSignalType == XY_PANEL_X){PANNELX_signal *s_ptr; this->GetSignal(&s_ptr);s_ptr->Reset(); }
    //if(CHConf_ptr->ChannelSignalType == XY_PANEL_Y){PANNELY_signal *s_ptr; this->GetSignal(&s_ptr);s_ptr->Reset(); }
    
    return 0;
}

// -------------------------------------------------
int DigChannel::CheckParCorrect(){
    //correcting Ampl gate accordance to data array
    float &minAGate = MainProcParam_ptr->minAmplGate;
    float &maxAGate = MainProcParam_ptr->maxAmplGate;
    
    const float minGate = 10.; //ns
    
    if(maxAGate < minGate) maxAGate = PointsToTime(EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT);//set full window gate
    if(minAGate < 0.) minAGate = 0.;
    
    if(maxAGate < minAGate){float reg = maxAGate; maxAGate = minAGate; minAGate = reg;}
    
    if(EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT < TimeToPoints(maxAGate))
	maxAGate = PointsToTime(EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT);
    
    if(maxAGate - minAGate < minGate){
	if(TimeToPoints(minAGate + minGate) <= EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT){
	    maxAGate = minAGate + minGate;
	}else if(TimeToPoints(maxAGate - minGate) <= 0.){
	    minAGate = maxAGate - minGate;
	}
    }
    
    return 0;
}

// -------------------------------------------------
int DigChannel::GetTotalEvents(){
    
    fseek(DataFile, 0L, SEEK_END);
    return ftell(DataFile)/(sizeof(float)*EVENT_PATTERN_LENGHT);
    
}
// -------------------------------------------------
float DigChannel::SignalIntegral(int pointFirst, int pointLast)
{
    if(pointFirst < 0) pointFirst = 0;
    if(pointLast > EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT-1) pointLast = EVENT_PATTERN_LENGHT-N_LAST_POINT_REJECT-1;
    
    float result_charge = 0.;
    for(int point = pointFirst; point <= pointLast; point++){
	
	//integrating by zero level
	//result_charge/*fC*/ +=  CurAmpl(point)/*mV*/ * (*TimeDim_ptr) /*ps*/ * 0.02/*1/50Ω*/;
	
	//integrating by 0mV level
	result_charge/*fC*/ +=  DataBuffer[point]*POLARITY/*mV*/ * (*TimeDim_ptr) /*ps*/ * 0.02/*1/50Ω*/;
    }
    
    return result_charge;
}

// -------------------------------------------------

float DigChannel::CurAmpl(int ch){
    return (DataBuffer[ch]-MainSignal.ZeroLevel)*POLARITY;
}

void DigChannel::Reset(){
    MainSignal.Reset();
}

int DigChannel::FindSignalAmplitude(float *Ampl){
    int ret = FindSignalAmplitude(); *Ampl = MainSignal.Amplitude; return ret;
}

int DigChannel::GetSignal(MAIN_signal **Sg_param_ptr){
    *Sg_param_ptr = &MainSignal; return 1;
}

int DigChannel::GetNumberOfCh(){
    return ThisNumChannel;
}

float* DigChannel::GetDataBuffer(int *ArrayPoint_){
    *ArrayPoint_ = EVENT_PATTERN_LENGHT;
    return DataBuffer;
}

TGraph* DigChannel::GetSignalGraph(){
    return SignalShapeGraph;
}

TF1* DigChannel::GetSignalFitFunc_pol1(){
    return SignalFront_pol1;
}

TF1* DigChannel::GetSignalFitFunc_pol3(){
    return SignalFront_pol3;
}


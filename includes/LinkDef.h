/*
 * LinkDef.h
 *
 *  Created on: May 18, 2016
 *      Author: mss
 */

#ifndef INCLUDES_LINKDEF_H_
#define INCLUDES_LINKDEF_H_

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class DigDataRecipe+;
#pragma link C++ struct DataConfiguration+;
#pragma link C++ struct ChConfiguration+;
#pragma link C++ struct DataProcParameters+;
#pragma link C++ struct Main_Processing_Param+;
#pragma link C++ struct Fitting_Processing+;
#pragma link C++ struct Amplitude_Processing_Param+;
#pragma link C++ class ParametersFrame+;
#pragma link C++ class MyMainFrame+;
#pragma link C++ class EvSelectotrFrame+;

#endif




#endif /* INCLUDES_LINKDEF_H_ */

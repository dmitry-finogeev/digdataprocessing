/*
 * DigEvent.cpp
 *
 *  Created on: May 11, 2016
 *      Author: mss
 */

#include "includes/DigEvent.h"



// -------------------------------------------------
DigEvent::DigEvent(DigDataRecipe *DataRecipe_ptr_){
    DataRecipe_ptr = DataRecipe_ptr_;
    NCHloaded = 0;
    if(VERBOSE > 1)printf("\n----------------------- EVENT CONSTRUCTION ------------------------\n");

    for(int nList=0; nList<MAX_CH; nList++) ChannelList[nList] = NULL;

    //fill out ChannelList corresponding to Config
    for(int Ch = 0; Ch < MAX_CH; Ch++){
        ChConfiguration *CurChConf_ptr = DataRecipe_ptr->GetConfiguration()->ChannelConfig+Ch;

        if(CurChConf_ptr->ChannelSignalType == UNDEFINED){
            if(VERBOSE > 1)printf("DigEvent::channel %d is not defined\n",Ch);
        };

        if(!CurChConf_ptr->IsEnable){
            if(VERBOSE > 1)printf("DigEvent::channel %d is not included\n",Ch);
            continue;
        };

        if(CurChConf_ptr->ChannelSignalType == LOGIC){
            if(VERBOSE > 1)printf("DigEvent::creating LOGIC class for CH%d at %d position\n",Ch, NCHloaded);
            ChannelList[  NCHloaded ] = new DigSignalLOGIC(Ch, DataRecipe_ptr);
            (NCHloaded)++;
        };

        if(CurChConf_ptr->ChannelSignalType == AMPLITUDE){
            if(VERBOSE > 1)printf("DigEvent::creating AMPLITUDE class for CH%d at %d position\n",Ch, NCHloaded);
            ChannelList[  NCHloaded ] = new DigSignalAMPLITUDE(Ch, DataRecipe_ptr);
            (NCHloaded)++;
        };

        if(CurChConf_ptr->ChannelSignalType == XY_PANEL_X){
            if(VERBOSE > 1)printf("DigEvent::creating XY_PANEL_X class for CH%d at %d position\n",Ch, NCHloaded);
            ChannelList[  NCHloaded ] = new DigSignalPANELX(Ch, DataRecipe_ptr);
            (NCHloaded)++;
        };

        if(CurChConf_ptr->ChannelSignalType == XY_PANEL_Y){
            if(VERBOSE > 1)printf("DigEvent::creating XY_PANEL_Y class for CH%d at %d position\n",Ch, NCHloaded);
            ChannelList[  NCHloaded ] = new DigSignalPANELY(Ch, DataRecipe_ptr);
            (NCHloaded)++;
        };


    };

    printf("\n-------------------------------------------------------------------\n");
}



// -------------------------------------------------
DigEvent::~DigEvent(){
    if(VERBOSE>1)printf("DigEvent::~DigEvent: Clearing ChannelList[%i]\n",NCHloaded);
    for(int nList=0; nList< NCHloaded; nList++){
        if(ChannelList[nList]){
            delete ChannelList[nList];
            if(VERBOSE > 1) printf("DigEvent::~DigEvent: channel deleted at %d position\n",nList);
        }else{if(VERBOSE > 0) printf("DigEvent::~DigEvent: ERROR: loaded channel NOT found at %d position\n",nList);}

    }

}



// -------------------------------------------------
int DigEvent::SetTreeParam(TTree *DataTree){
    int nProcessed = 0; //all events must be set

    for(int nList=0; nList< NCHloaded; nList++){

        if(ChannelList[nList]){
            ChannelList[nList]->SetBranch(DataTree);
            nProcessed++;
        };

    };

    if(nProcessed !=  NCHloaded)
        if(VERBOSE > 0)printf("DigEvent::SetTreeParam() ERROR: Channels were lost\n");

    return 0;
}



// -------------------------------------------------
int DigEvent::GetTotalEvents(){
    int minNEvents = ChannelList[0]->GetTotalEvents();
    int nProcessed = 0, flag = 0; //all events must be set

    for(int nList=0; nList< NCHloaded; nList++){

        if(ChannelList[nList]){

            int curNEvents = ChannelList[nList]->GetTotalEvents();
            if(curNEvents < minNEvents){minNEvents = curNEvents; flag = 1;}
            nProcessed++;
        };

    }

    if(nProcessed !=  NCHloaded)
        if(VERBOSE > 0)printf("DigEvent::GetTotalEvents::ERROR: Channels were lost. (active: %d, processed: %d)\n",
                              NCHloaded, nProcessed);
    if(flag)
        if(VERBOSE > 0)printf("DigEvent::ERROR: Number of events is different for different channels\n");

    return minNEvents;

}



// -------------------------------------------------
void DigEvent::CalculateEvent(){
    int nProcessed = 0; //all events must be set

    for(int nList=0; nList< NCHloaded; nList++){

        if(ChannelList[nList]){
            ChannelList[nList]->FindSignalParametrs();
            nProcessed++;
        };

    };

    if(nProcessed !=  NCHloaded)
        if(VERBOSE > 0)printf("DigEvent::CalculateEvent() ERROR: Channels were lost\n");

}



// -------------------------------------------------
DigEvent* DigEvent::SetEventNumber(int Event){
    int nProcessed = 0; //all events must be set

    for(int nList=0; nList< NCHloaded; nList++){

        if(ChannelList[nList]){
            ChannelList[nList]->SetEvent(Event);
            nProcessed++;
        };

    };

    if(nProcessed !=  NCHloaded)
        if(VERBOSE > 0)printf("DigEvent::ERROR: Channels were lost\n");

    return this;
}



// -------------------------------------------------
DigChannel* DigEvent::GetChannel(int nCh){
    for(int nList=0; nList< NCHloaded; nList++)
        if (ChannelList[nList]->GetNumberOfCh() == nCh ) return ChannelList[nList];

    if(VERBOSE > 0)printf("DigEvent::ERROR: Channel class %d was requested and NOT found\n",nCh);
    return NULL;

}



// -------------------------------------------------
DigChannel* DigEvent::GetChannelByName(char* CHname){
    GetChannel( DataRecipe_ptr->FindChannelByName(CHname) >= 0 );
    return NULL;
}

// -------------------------------------------------
bool DigEvent::IsSelectorSuit(EventSelectorCond selector){

    MAIN_signal *MAIN_signal;

    bool isSuit = selector.isANDch_selection;

    if(VERBOSE >= 1) printf("*************************************\n");
    selector.PrintOut();

    for(int nList=0; nList < NCHloaded; nList++){

        if(!ChannelList[nList])continue;

        ChannelList[nList]->GetSignal(&MAIN_signal);


        if(selector.Channel[ ChannelList[nList]->GetNumberOfCh() ])
        {
            if(selector.isANDch_selection)
                isSuit = isSuit && selector.IsSuit(*MAIN_signal);
            else
                isSuit = isSuit || selector.IsSuit(*MAIN_signal);
            if(VERBOSE >= 1) printf("list%i, ch%i, IsSuit%i ", nList, ChannelList[nList]->GetNumberOfCh(), isSuit); MAIN_signal->PrintOut();
        }

    }

    return isSuit;
}




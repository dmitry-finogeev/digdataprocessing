/*
 * DigData.cpp
 *
 *  Created on: May 11, 2016
 *      Author: mss
 */

#include "includes/DigData.h"

using namespace std;

DigData::DigData(GlobalProcessStatus *status){
    gProcessState = status;
    DigDataInit();
}

DigData::DigData(GlobalProcessStatus *status, const char *DigFilesPath_){
    gProcessState = status;
    DigDataInit();
    OpenData(DigFilesPath_);
}

DigData::~DigData(){
    if(CurrentEvent_ptr) delete CurrentEvent_ptr;
    delete DataRecipe_ptr;
    delete HistsProc_ptr;
}

void DigData::DigDataInit(){
    DataRecipe_ptr = new DigDataRecipe(); HistsProc_ptr = new TObjArray(); HistsProc_ptr->SetOwner(); CurrentEvent_ptr = NULL;
}





//---------------------------------------------------------------------------------------------------
int DigData::OpenData(){
    if(VERBOSE>0)printf("========================== OPENING DATA ===========================\n");
    if(DataRecipe_ptr->GetConfiguration()->IsConfigured < 2){
        if(VERBOSE>0)printf("DigData::OpenData: ERROR: Can not open data, configuration are not loaded\n");
        return 0;
    }
    if(VERBOSE>0)printf("DigData::OpenData: data path: %s\n",DataRecipe_ptr->GetConfiguration()->DataPath);

    //delete old CurrentEvent
    if(CurrentEvent_ptr){
        if(VERBOSE>0)printf("DigData::OpenData: Deleting old event:\n");
        delete CurrentEvent_ptr;
        CurrentEvent_ptr = NULL;
    }

    //create new event (with new recipe)
    CurrentEvent_ptr = new DigEvent(DataRecipe_ptr);

    DataRecipe_ptr->GetConfiguration()->IsConfigured = 3;
    return 1;
}

int DigData::OpenData(const char *DigFilesPath_){
    if(!DataRecipe_ptr->Configure(DigFilesPath_)){
        return 0;
    }
    if(OpenData()){
        return 1;
    }
    return 0;
}//same, but open/reopen DataRecipe

//---------------------------------------------------------------------------------------------------
void DigData::DataProcParamConfigure(){
    DataProcParameters *ProcParameters_ptr = DataRecipe_ptr->GetProcParam();
    DataConfiguration *Configuration_ptr = DataRecipe_ptr->GetConfiguration();
    ChConfiguration *CHconf_ptr = Configuration_ptr->ChannelConfig;

    int TOTev = CurrentEvent_ptr->GetTotalEvents();
    if(ProcParameters_ptr->Statistic > TOTev)ProcParameters_ptr->Statistic = TOTev;
    //ResetArray(HistsProc_ptr);
    HistsProc_ptr->Clear();

    if(VERBOSE > 0){
        printf("\nData processing parameters conficurating --------------------------\n");
        printf("DigData::DataProcParamConfigure: Data processing parameters:\n");
        ProcParameters_ptr->PrintOut();
        printf("-------------------------------------------------------------------\n");
    }

    int NumActCh = 0;
    for(int Channel=0; Channel< MAX_CH; Channel++)
        if(CHconf_ptr[Channel].IsEnable){
            NumActCh++;
            gProcessState->SetStatus(1, (float)NumActCh / (float)Configuration_ptr->NenableCh);

            //if(CHconf_ptr[Channel].ChannelSignalType == AMPLITUDE) ConfAmplitude(Channel);
            if(CHconf_ptr[Channel].ChannelSignalType == AMPLITUDE) ConfChisquare(Channel);

            if(CHconf_ptr[Channel].ChannelSignalType == LOGIC) ConfAmplitude(Channel);
            if(CHconf_ptr[Channel].ChannelSignalType == LOGIC) ConfChisquare(Channel);

        }

    if(VERBOSE > 0)printf("--------------------------------------------------------------------\n");
    Configuration_ptr->IsConfigured = 4;

}

//---------------------------------------------------------------------------------------------------
void DigData::ResetArray(TObjArray *&array)
{
    if(!array) return;
    //printf("debug: deleting %s\n", array->GetName());
    for(Int_t element = 0; element <= array->GetLast(); element++)
        if(array->At(element)){
            //            printf("debug: deleting %s\n", array->At(element)->GetName());

            delete array->At(element);
        }

    array->Clear();

    //    delete array;
    //    array = new TObjArray();

}

//---------------------------------------------------------------------------------------------------
void DigData::ConfAmplitude(int channel){
    
    Main_Processing_Param *mainCHparam_ptr = DataRecipe_ptr->GetMainParam(channel);
    Amplitude_Processing_Param *amplitudeCHparam = DataRecipe_ptr->GetAmplParam(channel);
    DigChannel *curChannel_ptr = CurrentEvent_ptr->GetChannel(channel);
    ChConfiguration *ChConf_ptr = DataRecipe_ptr->GetConfiguration()->ChannelConfig+channel;

    char CHname[MAX_NAME_LENGHT]; strcpy(CHname, ChConf_ptr->ChName);
    float AmplMin = mainCHparam_ptr->minAmplitude;
    float AmplMax = mainCHparam_ptr->maxAmplitude;
    float range = DataRecipe_ptr->GetProcParam()->AmplRange;
    
    
    if((range < 0.1 )||(AmplMin < 0.1))
    {
        if(VERBOSE > 0) printf("Ampl range: %0.2f; Ampl min: %0.2f: amplitude configuation NOT applied\n", range, AmplMin);
        return;
    }

    float curAmpl;
    float  fit_mean, fit_sigma, H_Mean, H_RMS;
    float H_min, H_max;
    TH1F *Hcur1;
    TF1 Resolution("Resolution","gaus");


    //turning off precise analysis
    Int_t IsPrec_calc = 0;
    if(amplitudeCHparam)
    {
        IsPrec_calc = amplitudeCHparam->isPrecCalc;
        amplitudeCHparam->isPrecCalc = 0;
    }


    //amplitude analysis
    Hcur1 = NULL; HistsProc_ptr->Add( Hcur1 = new TH1F(Form("Ampl_%s",CHname),Form("Amplitude %s; Amplitude, mV",CHname),1000,AmplMin,AmplMax) );

    for(int Event = 0; Event < DataRecipe_ptr->GetProcParam()->Statistic; Event++){
        curChannel_ptr->SetEvent(Event);
        if( !curChannel_ptr->FindSignalAmplitude(&curAmpl) ) continue;
        if( curAmpl < AmplMin )continue;
        if( curAmpl > AmplMax )continue;
        Hcur1->Fill(curAmpl);
    }

    
    //determine min and max amplitude
    H_Mean = Hcur1->GetMean(); H_RMS = Hcur1->GetRMS();
    H_min = H_Mean-2.*H_RMS; if(H_min < AmplMin)H_min = AmplMin;
    H_max = H_Mean+2.*H_RMS; if(H_max > AmplMax)H_max = AmplMax;
    Hcur1->Fit(&Resolution, "Q","",H_min,H_max);
    fit_mean = Resolution.GetParameter(1); fit_sigma = Resolution.GetParameter(2);

    
    
    mainCHparam_ptr->minAmplitude = fit_mean-range*fit_sigma; if(mainCHparam_ptr->minAmplitude < AmplMin)mainCHparam_ptr->minAmplitude = AmplMin;
    mainCHparam_ptr->maxAmplitude = fit_mean+range*fit_sigma; if(mainCHparam_ptr->maxAmplitude > AmplMax)mainCHparam_ptr->maxAmplitude = AmplMax;

    mainCHparam_ptr->POL1.minAmplitude = (mainCHparam_ptr->minAmplitude < mainCHparam_ptr->POL1.minAmplitude)?
                mainCHparam_ptr->POL1.minAmplitude:mainCHparam_ptr->minAmplitude;
    
    mainCHparam_ptr->POL3.minAmplitude = (mainCHparam_ptr->minAmplitude < mainCHparam_ptr->POL3.minAmplitude)?
                mainCHparam_ptr->POL3.minAmplitude:mainCHparam_ptr->minAmplitude;


    if(Hcur1->GetFunction("Resolution"))
        Hcur1->GetFunction("Resolution")->SetRange(mainCHparam_ptr->minAmplitude, mainCHparam_ptr->maxAmplitude); //to see the range oh hists


    if(VERBOSE > 0)printf("AMPLITUDE configuration for CH%i \"%s\": MINampl %.1f, MAXampl %.1f",
                          channel, CHname, mainCHparam_ptr->minAmplitude, mainCHparam_ptr->maxAmplitude);

    //determine LED treshold
    if(amplitudeCHparam)
    {
        amplitudeCHparam->isPrecCalc = IsPrec_calc;
        amplitudeCHparam->treshold = fit_mean-1.*fit_sigma;
        if(VERBOSE > 0)printf(", LEDtreshold %.1f", amplitudeCHparam->treshold);

    }
    if(VERBOSE > 0)printf(" (mV)\n");

}

//---------------------------------------------------------------------------------------------------
void DigData::ConfChisquare(int channel){
    DigChannel *curChannel_ptr = CurrentEvent_ptr->GetChannel(channel);
    Main_Processing_Param *mainCHparam_ptr = DataRecipe_ptr->GetMainParam(channel);

    char CHname[MAX_NAME_LENGHT]; strcpy(CHname, DataRecipe_ptr->GetConfiguration()->ChannelConfig[channel].ChName);
    int POL1_ON = mainCHparam_ptr->POL1.FittingON;
    int POL3_ON = mainCHparam_ptr->POL3.FittingON;

    float AmplMin = mainCHparam_ptr->minAmplitude;
    float AmplMax = mainCHparam_ptr->maxAmplitude;
    float H_max, H_Mean, H_RMS;
    TH1F *Hcur1=NULL, *Hcur2=NULL;
    TH2F *H2cur1=NULL, *H2cur2=NULL, *H2cur3=NULL;

    //turning off precise analysis
    Amplitude_Processing_Param *currAmplProcParam = DataRecipe_ptr->GetAmplParam(channel);
    Int_t IsPrec_calc = 0;
    if(currAmplProcParam)
    {
        IsPrec_calc = currAmplProcParam->isPrecCalc;
        currAmplProcParam->isPrecCalc = 0;
    }


    //determine right fitting Chisquare range, than amplitude parameters calculated
    float Chi_POL1 = 0., Chi_POL3 = 0.;
    MAIN_signal *MAIN_signal; curChannel_ptr->GetSignal(&MAIN_signal);

    H2cur3 = NULL; HistsProc_ptr->Add( H2cur3 = new TH2F(Form("Ampl_FPL_%s",CHname),Form("Amplitude vs Lenght of peak front %s; Amplitude, mV;Peak front lenght, ns",CHname),1000,AmplMin,AmplMax,1000,0,10) );

    if(POL1_ON){
        Hcur1 = NULL; HistsProc_ptr->Add( Hcur1 = new TH1F(Form("Chi_POL1_%s",CHname),Form("Chisquare POL1 %s",CHname),1000,0,mainCHparam_ptr->POL1.MaxChisquare) );
        H2cur1 = NULL; HistsProc_ptr->Add( H2cur1 = new TH2F(Form("Chi_PFl_POL1_%s",CHname),Form("Chisquare POL1 vs Peak front lenght %s;Chisquare;peak front lenght, ns",CHname), 500,0,mainCHparam_ptr->POL3.MaxChisquare,1000,0,10) );
    }

    if(POL3_ON){
        Hcur2 = NULL; HistsProc_ptr->Add( Hcur2 = new TH1F(Form("Chi_POL2_%s",CHname),Form("Chisquare POL3 %s",CHname),1000,0,mainCHparam_ptr->POL3.MaxChisquare) );
        H2cur2 = NULL; HistsProc_ptr->Add( H2cur2 = new TH2F(Form("Chi_PFl_POL3_%s",CHname),Form("Chisquare POL3 vs Peak front lenght %s;Chisquare;peak front lenght, ns",CHname), 500,0,mainCHparam_ptr->POL3.MaxChisquare,1000,0,10) );
    }

    for(int Event = 0; Event < DataRecipe_ptr->GetProcParam()->Statistic; Event++){
        curChannel_ptr->SetEvent(Event);

        if( curChannel_ptr->FindSignalAmplitude() ){
            curChannel_ptr->FindSignalParametrs();

            H2cur3->Fill(MAIN_signal->Amplitude, MAIN_signal->time_front_end - MAIN_signal->time_begin);

            if(POL1_ON)
                if( !IS_VOID_VALUE( MAIN_signal->Time_pol1) ){
                    TF1 *Sfront_POL1 = curChannel_ptr->GetSignalFitFunc_pol1();
                    Chi_POL1 = Sfront_POL1->GetChisquare();
                    Hcur1->Fill(Chi_POL1);
                    H2cur1->Fill(Chi_POL1,MAIN_signal->time_front_end - MAIN_signal->time_begin );
                }

            if(POL3_ON)
                if( !IS_VOID_VALUE( MAIN_signal->Time_pol3) ){
                    TF1 *Sfront_POL3 = curChannel_ptr->GetSignalFitFunc_pol3();
                    Chi_POL3 = Sfront_POL3->GetChisquare();
                    Hcur2->Fill(Chi_POL3);
                    H2cur2->Fill(Chi_POL3,MAIN_signal->time_front_end - MAIN_signal->time_begin );
                }

        }// if Ampl

    }//for Event

    if(currAmplProcParam) currAmplProcParam->isPrecCalc = IsPrec_calc;


    if(POL1_ON){
        H_Mean = Hcur1->GetMean(); H_RMS = Hcur1->GetRMS();
        H_max = H_Mean + mainCHparam_ptr->POL1.ChiRange * H_RMS; if(H_max > mainCHparam_ptr->POL1.MaxChisquare)H_max = mainCHparam_ptr->POL1.MaxChisquare;
        mainCHparam_ptr->POL1.MaxChisquare = H_max;
        if(VERBOSE > 0)printf("CHISQUARE configuration for CH%i \"%s\": Max chi_POL1 = %.1f",channel, CHname,H_max);
    }

    if(POL3_ON){
        H_Mean = Hcur2->GetMean(); H_RMS = Hcur2->GetRMS();
        H_max = H_Mean + mainCHparam_ptr->POL3.ChiRange * H_RMS; if(H_max > mainCHparam_ptr->POL3.MaxChisquare)H_max = mainCHparam_ptr->POL3.MaxChisquare;
        mainCHparam_ptr->POL3.MaxChisquare = H_max;
        if(VERBOSE > 0)printf(", Max chi_POL3 = %.1f\n",H_max);
    }else{if(VERBOSE > 0)printf("\n");}

};

/*!
 * \brief Save data recorded from digitizer to root file as TTree format\n
 * \param[out] FileName Name of root file for recording data, file extension ".root" can be skipped
 * \param[out] NumEvToProcess Number of events to be calculated, to calculate all events set it to 0
 * \return 0 or 1 depending on the reading success
 * \details
 * This funtion read data and save it to ROOT file as TTree format\n\n
 * Examples how to read such ttree:\n
 * ttree->Draw("main_2MCP1.Time_pol3 - (main_T0_C1.Time_pol3+main_T0_D1.Time_pol3)*0.5 >>(10000,0,100)",
             "(main_2MCP1.Time_pol3 > 0)&&(main_T0_C1.Time_pol3>0)&&(main_T0_D1.Time_pol3>0)"); //draw time (by polynome^3) resolution for 2MCP1 channel relate to T0 PMTs time\n
             see more in DigTreeViewer.c
 */

//---------------------------------------------------------------------------------------------------

int DigData::SaveDataAsTree(const char *FileName, int NumEvToProcess){

    DataConfiguration *Configuration_ptr = DataRecipe_ptr->GetConfiguration();
    DataProcParameters *ProcParameters_ptr = DataRecipe_ptr->GetProcParam();

    if(VERBOSE>0)printf("Saving tree data:\n");
    if(Configuration_ptr->IsConfigured < 3)
        if(!OpenData()){if(VERBOSE > 0) printf("DigData::SaveDataAsTree ERROR Data was NOT opened !!!\n"); return 0;}

    //configurin proc parameters
    DataProcParamConfigure();

    int EventNumber = ( (NumEvToProcess<=0) || (NumEvToProcess>CurrentEvent_ptr->GetTotalEvents()) )? CurrentEvent_ptr->GetTotalEvents(): NumEvToProcess;
    if(EventNumber <= 0) return 0;

    string Fname = FileName;
    if(Fname.find(".root") == std::string::npos) Fname.append(".root");

    TFile *TreeFile = new TFile(Fname.c_str(),"RECREATE",Form("File with processed data for run %s",Configuration_ptr->DataName));
    if(!TreeFile->IsOpen()){
        if(VERBOSE > 0) printf("DigData::SaveDataAsTree: Tree data file \"%s\" was NOT opened\n",Fname.c_str());
        return 0;
    }

    if(VERBOSE >= 0) printf("DigData::SaveDataAsTree: Tree file \"%s\" was opened for collecting of %d events\n",Fname.c_str(),EventNumber);

    //configuring main_ch list
    int MainChannels[MAX_CH] = {0}; //array of main channels in CHnumber
    for(int ch=0; ch < ProcParameters_ptr->essentialChannels; ch++){
        int s_ch = DataRecipe_ptr->FindChannelByName( (char*)ProcParameters_ptr->essentialChName[ch].Data() );
        if(s_ch < 0) if(VERBOSE > 0) printf("DigData::SaveDataAsTree: ERROR: channel \"%s\" was found to include it in main_ch list\n", ProcParameters_ptr->essentialChName[ch].Data());
        if(s_ch >= 0){
            MainChannels[ch] = s_ch;
            if(VERBOSE > 0){
                if(ProcParameters_ptr->essentialValue[ch] == 0)printf("AMPL correct");
                if(ProcParameters_ptr->essentialValue[ch] == 1)printf("POL1 correct");
                if(ProcParameters_ptr->essentialValue[ch] == 2)printf("POL3 correct");
                printf(" must be in event for channel %i \"%s\"\n",s_ch,ProcParameters_ptr->essentialChName[ch].Data());
            }
        }
    };
    //filling TTree
    TTree *DataTree_ptr = new TTree("RawDataTree", Form("%s [%s]",Configuration_ptr->DataName,Configuration_ptr->RunComment));
    CurrentEvent_ptr->SetTreeParam(DataTree_ptr); // fill out branches

    time_t start_time = time(NULL);
    time_t current_time;
    int proc_sec;
    int Event_collected = 0;

    if(VERBOSE>0)printf("\nPROCESSING DATA TREE:\n");
    if(VERBOSE>0)printf("  Event processed      Event collected      Passed time TimeLeft\n");
    for(int nEvent = 0; nEvent <= EventNumber; nEvent++){
        gProcessState->SetStatus(2, (float)nEvent / (float)EventNumber);
        if(VERBOSE > 0) if( nEvent%31==0) if( nEvent != 0){
            current_time = time(NULL);
            proc_sec = difftime(current_time,start_time);
            float percents = (float)nEvent/EventNumber;
            int time_est = (percents == 0)?0:proc_sec/percents*(1. - percents);

            printf("%8.0d (%5.1f%%);   %8.0d (%5.1f%%);     [%3.0dm %2.0is]",
                   nEvent, percents*100., Event_collected, (float)Event_collected/(float)nEvent*100.,(proc_sec/60), proc_sec%60);
            printf("  [%3.0dm %2.0is]\r", (time_est/60), time_est%60);
            cout<<flush;
        }

        CurrentEvent_ptr->SetEventNumber(nEvent);

        //calculation channels from main channels list:
        DigChannel *Ch_ptr;
        int AmplCorrect, Event_incomlete = 0;
        for(int ch=0; ch < ProcParameters_ptr->essentialChannels; ch++){
            Ch_ptr = CurrentEvent_ptr->GetChannel( MainChannels[ch] );
            AmplCorrect = Ch_ptr->FindSignalAmplitude();

            if(!AmplCorrect){Event_incomlete = 1; break;}

            MAIN_signal *MAIN_signal; Ch_ptr->GetSignal(&MAIN_signal);

            if(ProcParameters_ptr->essentialValue[ch] == 1){
                Ch_ptr->FindSignalParametrs();
                if( IS_VOID_VALUE(MAIN_signal->Time_pol1) ){Event_incomlete = 1; break;}
            }

            if(ProcParameters_ptr->essentialValue[ch] == 2){
                Ch_ptr->FindSignalParametrs();
                if( IS_VOID_VALUE(MAIN_signal->Time_pol3) ){Event_incomlete = 1; break;}
            }

        }//for ch

        if(Event_incomlete) continue;

        CurrentEvent_ptr->CalculateEvent();
        DataTree_ptr->Fill();
        Event_collected++;
    }
    

    if(VERBOSE>0)printf("DigData::SaveDataAsTree: Writing data in file:\n");

    DataRecipe_ptr->Write("DataRecipe");
    
    DataTree_ptr->Write("RawDataTree");
    delete DataTree_ptr;
    
    TDirectory *HistDir = TreeFile->mkdir("DataProcParamHists"); HistDir->cd();
    HistsProc_ptr->Write();
    HistsProc_ptr->Clear();
    
    
    TreeFile->Close();
    delete TreeFile;

    if(VERBOSE > 0) printf("DigData::SaveDataAsTree: Tree file \"%s\" was closed\n",Fname.c_str());

    current_time = time(NULL);
    proc_sec = difftime(current_time,start_time);
    if(VERBOSE > 0) printf("Event collected %i; processing time: %d min %i sec (%i sec)\n",Event_collected, (proc_sec/60), proc_sec%60,proc_sec);

    return 1;
}

DigDataRecipe* DigData::GetDataRecipe(){
    return DataRecipe_ptr;
}
DigEvent* DigData::GetEvent(int nEvent){
    return CurrentEvent_ptr->SetEventNumber(nEvent);
}

void DigData::GetProcHistArray(TObjArray* arr){
    arr = HistsProc_ptr;
}

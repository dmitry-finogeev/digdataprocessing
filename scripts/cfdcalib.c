

#include "../../utilites/utilites.h"

/*******************************************************************************************************************/
TH1 *CreateErrorPojection(TH2 *sourcehist, TString rebinoption = "n", const Float_t minbinevX = 1,
                                        const Float_t minbinevedge = 0.001, const Int_t rebinX = 1, const Int_t rebinY = 1)
{
    static const Int_t minBinEvOnEdge = 5;
    static const Int_t ProjectionRebin = 5;
    static const Int_t ProjectionFit = 0;

    Int_t fVerbose = 5;

    if(fVerbose >= 2)cout <<Form("Creating Projection for \"%s\" ... ", sourcehist->GetName())<<endl;

    Int_t NumTotalEv = sourcehist->GetEntries();
    if(NumTotalEv < 1000)
    {
        cout <<Form("AliT0TimeAmplCorr::CreatePojection::ERROR: Too low events in \"%s\" hist: %i",sourcehist->GetName(), NumTotalEv)<<endl;
        return NULL;
    }

    TH2 *rebinedhist = sourcehist->Rebin2D(rebinX, rebinY, Form("%s_rebin",sourcehist->GetName()) );

    if(rebinoption.Contains("s"))rebinedhist->Smooth(3);

    Int_t NumBinsY = rebinedhist->GetNbinsY(); //number of bin along CFD
    Int_t NumBinsX = rebinedhist->GetNbinsX(); //number of bin along QTC

    //average events in bin, does not calculated empty bins
    Int_t AverageEvInBinX = 0, bincalculated = 0, integral; //events in bin (QTC axis) on the averages
    for(Int_t xbin = 1; xbin <= NumBinsX; xbin++)
    {
        integral = rebinedhist->Integral(xbin,xbin,1,NumBinsY);
        if(0 < integral){ AverageEvInBinX += integral; bincalculated++;}
    }
    AverageEvInBinX = ceil((Float_t)AverageEvInBinX / (Float_t)bincalculated);


    if(fVerbose >= 4)cout <<Form("Average events in bin: %i", AverageEvInBinX)<<endl;

    //fist and last bin with minbinevedge*AverageEvInBinX
    Int_t firstBinX = -1, lastBinX = -1;
    for(Int_t xbin = 1; xbin <= NumBinsX; xbin++)
    {
        if(firstBinX < 0)
            if( (ceil(minbinevedge*AverageEvInBinX) <= rebinedhist->Integral(xbin,xbin,1,NumBinsY)) &&
                    ( minBinEvOnEdge <= rebinedhist->Integral(xbin,xbin,1,NumBinsY)) ) firstBinX = xbin;

        if(lastBinX < 0)
            if( (ceil(minbinevedge*AverageEvInBinX) <= rebinedhist->Integral(NumBinsX-xbin+1,NumBinsX-xbin+1,1,NumBinsY)) &&
                    ( minBinEvOnEdge <= rebinedhist->Integral(NumBinsX-xbin+1,NumBinsX-xbin+1,1,NumBinsY))) lastBinX = NumBinsX-xbin+1;

        if( (lastBinX > 0) && (firstBinX > 0) ) break;
    }

    if(fVerbose >= 4)cout <<Form("FIRST BIN: %i, %.1f (%.1f ev); LAST BIN: %i, %.1f (%.1f ev)",
                                 firstBinX, rebinedhist->GetXaxis()->GetBinCenter(firstBinX), rebinedhist->Integral(firstBinX,firstBinX,1,NumBinsY),
                                 lastBinX,  rebinedhist->GetXaxis()->GetBinCenter( lastBinX), rebinedhist->Integral(lastBinX, lastBinX, 1,NumBinsY))<<endl;
    if(rebinoption.Contains("r"))if(fVerbose >= 4)cout <<Form("Bin before rebinning: %i", lastBinX-firstBinX)<<endl;

    //finding bins: in each bin minimum minQTCCFDbinEv*QTCCFDnumEvInBinXAverage events
    Float_t *NewBinning = new Float_t[lastBinX-firstBinX+2];
    Int_t eventsINbin = 0, numberOfBins = 0;

    if(fVerbose >= 5)cout <<Form("BINNING: ");
    NewBinning[0] = rebinedhist->GetXaxis()->GetBinLowEdge(firstBinX);
    for(Int_t xbin = firstBinX+1; xbin <= lastBinX; xbin++)
    {
        eventsINbin += rebinedhist->Integral(xbin,xbin,1,NumBinsY);

        if( (eventsINbin >= AverageEvInBinX*minbinevX) || (xbin == lastBinX) || (!rebinoption.Contains("r")))
        {
            numberOfBins++;
            NewBinning[numberOfBins] = rebinedhist->GetXaxis()->GetBinUpEdge(xbin);
            if(fVerbose >= 5)cout <<Form("%.1f ", NewBinning[numberOfBins]);
            eventsINbin = 0;
        }
    } //for sbin
    if(fVerbose >= 4)cout <<""<<endl;
    if(rebinoption.Contains("r"))if(fVerbose >= 4)cout <<Form("Bin after rebining: %i", numberOfBins)<<endl;


    //filling 1D histograms with mean and errors by gauss fit
    TH1 *ptrProjection = new TH1F("Projection","title",numberOfBins,0,1);
    ptrProjection->GetXaxis()->Set(numberOfBins, NewBinning);
    ptrProjection->SetOption("E1");

    Int_t firstbin, lastbin;
    Double_t mean, sigma, sigmaERROR;
    TH1 *ptrProjectionY;
    for(Int_t bin = 0; bin < numberOfBins; bin++)
    {
        mean = sigma = sigmaERROR = 0.;
        firstbin = rebinedhist->GetXaxis()->FindBin( NewBinning[bin] );
        lastbin = rebinedhist->GetXaxis()->FindBin( NewBinning[bin+1] );

        ptrProjectionY = rebinedhist->ProjectionY(Form("QTCCFDslice_bin_%i_%i",firstbin,lastbin),firstbin,lastbin);
        if(ptrProjectionY)
        {
            ptrProjectionY->Rebin(ProjectionRebin);
            mean = ptrProjectionY->GetMean();
            sigma = ptrProjectionY->GetRMS();
            sigmaERROR = ptrProjectionY->GetRMSError();
            if(ProjectionFit) FitHistogrammInRMSRange(ptrProjectionY,mean,sigma, .7);
            delete ptrProjectionY;
        } else {  cout <<Form("WARING: ProjectionY \"%s\" was NOT created", Form("QTCCFDslice_bin_%i_%i",firstbin,lastbin) )<<endl; }

        ptrProjection->SetBinContent(bin+1,mean);
        ptrProjection->SetBinError(bin+1,sigma);

    }

    delete rebinedhist;
    delete[] NewBinning;
    return ptrProjection;
}
/*******************************************************************************************************************/


Int_t cfdcalib()
{
    TString filepath, filecomment, TRIGGEReq, TriggerSelEq, MAINselEq;

//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/FIRST_START/data.root";  filecomment = "FIRST_START";
//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/CFD1_z040/data.root";  filecomment = "CFD1z040";
//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/CFD1_z-060/data.root";  filecomment = "CFD_z1_-060";
//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/CFD_z1_-110/data.root";  filecomment = "CFD_z1_-110";
//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/CFD_z1_-160_z2_15_z3_-225_z4_-125/data.root";  filecomment = "CFDz_-160_015_-225_-125";
//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/CFD_-100_-050_-200_-150/data.root";  filecomment = "CFD_-100_-050_-200_-150";
//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/CFD_-100_-80_-200_-170/data.root";  filecomment = "CFD_-100_-80_-200_-170";
//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/CFD_-100_-80_-200_-170_bb/data.root";  filecomment = "CFD_-100_-80_-200_-170";
//    filepath = "/home/DigWork/Desktop/DigData/CFD_laser_calib/CFD_hightHV_attMCP1/data.root";  filecomment = "CFD_HV1440_MCP1att80db_x03";
//    filepath = "/mnt/hgfs/DETECTORS_DATA/Fit_CFD_laser_calib/CFD_Data_HV1440_MCP1att80db_x03.root";  filecomment = "CFD_HV1440_CFD_amplf_X10"; Float_t amplAtt = 3.;
    filepath = "/mnt/hgfs/DETECTORS_DATA/Fit_CFD_laser_calib/CFD_Data_HV1440_MCP1att80db_x03_allchi.root";  filecomment = "CFD_HV1440_allchi_CFD_amplf_X10"; Float_t amplAtt = 3.;
 //   filepath = "/mnt/hgfs/DETECTORS_DATA/Fit_CFD_laser_calib/CFD_Data_CFD_HV1440_AMPLx10_MCP1att80db_x03.root";  filecomment = "CFD_HV1440_CFD_amplf_X10"; Float_t amplAtt = 3.;
//      filepath = "/mnt/hgfs/DETECTORS_DATA/Fit_CFD_laser_calib/CFD_Data_CFD_1370_AMPLFx10.root";  filecomment = "CFD_HV1370_CFD_amplfX10"; Float_t amplAtt = 1.;

    
//    TRIGGEReq = "((main_T0_C2.Time_pol1+main_T0_D2.Time_pol1)*0.5)";
//    TriggerSelEq = "( (main_T0_C2.Time_pol1 != 0)&&(main_T0_D2.Time_pol1 != 0) )";

    TRIGGEReq = "(main_TRG1.Time_pol1)";
    TriggerSelEq = "( 1 )";


    Double_t MCP_ampl_min = 0.;
    Double_t MCP_ampl_max = 3050.;
    Double_t MCP_ampl_bin = 10.;

    Double_t MCP_time_value = 114.;
    Double_t CFD_time_value = 134.;

    Double_t TIME_halfrange = 50.;
    Double_t TIME_bin = .001;
    Double_t TIME_sigma_range = 3.;

    Double_t mean, sigma;
    Double_t CFDtime_mean[4];
    Double_t CFDtime_sigma[4];
    Double_t MCPtime_mean[4];
    Double_t MCPtime_sigma[4];

    Double_t CFD_time_hist_min, CFD_time_hist_max;
    Double_t MCP_time_hist_min, MCP_time_hist_max;
    Int_t MCP_time_bin, CFD_time_bin;

    TFile *File = new TFile( filepath.Data() );
    TTree *tree = (TTree*)File->Get("RawDataTree");

    TObjArray *ArrayToSave = new TObjArray(), *currArray = 0;
    TH1 *currHist = 0;

    Int_t MCPampl = 0;

    for(int nMCP=1;nMCP<=4;nMCP++){

        ArrayToSave->Add( currArray = new TObjArray() );
        currArray->SetName(Form("MCP_%i",nMCP));

        MAINselEq = Form("(%s && (10. < main_5MCP%i.Amplitude*%f))", TriggerSelEq.Data(), nMCP, amplAtt);



        currHist = new TH1F(Form("MCPampl_%i",nMCP),
                            Form("5MCP%i Amplitude [%s] (Time selection); MCP Amplitude, mV", nMCP,filecomment.Data()),
                            hist_bin_calc(MCP_ampl_bin, MCP_ampl_min, MCP_ampl_max),MCP_ampl_min,MCP_ampl_max);
        tree->Draw( Form("main_5MCP%i.Amplitude*%f>>%s",nMCP,amplAtt,currHist->GetName()),
                    Form("%s", MAINselEq.Data()), "");
        currArray->Add(currHist);

        currHist = new TH1F(Form("CFDtime_%i",nMCP),
                            Form("5CFD%i time [%s]; CFD Time, ns", nMCP, filecomment.Data()),
                            hist_bin_calc(TIME_bin, 0., TIME_halfrange*2.), CFD_time_value - TIME_halfrange,CFD_time_value + TIME_halfrange);
        tree->Draw( Form("main_5CFD%i.Time_pol1-%s>>%s", nMCP,TRIGGEReq.Data(),currHist->GetName()),
                    Form("(main_5CFD%i.Time_pol1 != 0.)&&%s", nMCP, MAINselEq.Data()), "col");
        FitHistogrammInRMSRange(currHist, mean, sigma, 0.5);
        currHist->SetTitle(Form("5CFD%i time [%s] Sigma = %.1f ps; CFD Time, ns", nMCP, filecomment.Data(), sigma*1000.));
        CFDtime_mean[nMCP-1] = mean;
        CFDtime_sigma[nMCP-1] = sigma;
        currArray->Add(currHist);

        currHist = new TH1F(Form("MCPtime_%i",nMCP),
                            Form("5MCP%i time [%s]; MCP Time, ns", nMCP, filecomment.Data()),
                            hist_bin_calc(TIME_bin, 0., TIME_halfrange*2.), MCP_time_value - TIME_halfrange,MCP_time_value + TIME_halfrange);
        tree->Draw( Form("main_5MCP%i.Time_pol1-%s>>%s", nMCP,TRIGGEReq.Data(), currHist->GetName()),
                    Form("(main_5MCP%i.Time_pol1 != 0.)&&%s", nMCP, MAINselEq.Data()) );
        FitHistogrammInRMSRange(currHist, mean, sigma, 0.5);
        currHist->SetTitle(Form("5MCP%i time [%s] Sigma = %.1f ps; MCP Time, ns", nMCP, filecomment.Data(), sigma*1000.));
        MCPtime_mean[nMCP-1] = mean;
        MCPtime_sigma[nMCP-1] = sigma;
        currArray->Add(currHist);

        CFD_time_hist_min = CFDtime_mean[nMCP-1] - CFDtime_sigma[nMCP-1] * TIME_sigma_range;
        CFD_time_hist_max = CFDtime_mean[nMCP-1] + CFDtime_sigma[nMCP-1] * TIME_sigma_range;
        MCP_time_hist_min = MCPtime_mean[nMCP-1] - MCPtime_sigma[nMCP-1] * TIME_sigma_range;
        MCP_time_hist_max = MCPtime_mean[nMCP-1] + MCPtime_sigma[nMCP-1] * TIME_sigma_range;
        CFD_time_bin = hist_bin_calc(TIME_bin, CFD_time_hist_min, CFD_time_hist_max);
        MCP_time_bin = hist_bin_calc(TIME_bin, MCP_time_hist_min, MCP_time_hist_max);

        MCPampl = 1; //only one channel attenuated

        currHist = new TH2F(Form("MCP%damplCFDtime_%i",MCPampl,nMCP),
                            Form("5MCP%i Amplitude vs 5CFD%i time [%s]; MCP Amplitude, mV; CFD Time, ns", MCPampl,nMCP,filecomment.Data()),
                            hist_bin_calc(MCP_ampl_bin, MCP_ampl_min, MCP_ampl_max),MCP_ampl_min,MCP_ampl_max,
                            CFD_time_bin, CFD_time_hist_min, CFD_time_hist_max);
//                            hist_bin_calc(TIME_bin, 0., TIME_halfrange*2.), CFD_time_value - TIME_halfrange,CFD_time_value + TIME_halfrange);
        tree->Draw( Form("main_5CFD%i.Time_pol1-%s:main_5MCP%i.Amplitude*%f>>%s", nMCP,TRIGGEReq.Data(),MCPampl,amplAtt,currHist->GetName()),
                    Form("(main_5CFD%i.Time_pol1 != 0.)&&%s", nMCP, MAINselEq.Data()), "col");
        currArray->Add(currHist);
        currHist = CreateErrorPojection((TH2*)currHist, "r",0.5);
        if(!currHist) return 0;
        currHist->SetTitle( Form("5CFD%i Time PROJECTION vs Amplitude 5MCP%i [%s]; MCP Amplitude, mV; CFD Time mean", nMCP,MCPampl,filecomment.Data()) );
        currHist->SetName(Form("MCP%damplCFDtime_PROJ_%i",MCPampl,nMCP));
        currArray->Add(currHist);

        currHist = new TH2F(Form("MCPamplMCPtime_%i",nMCP),
                            Form("5MCP%i Amplitude vs 5MCP%i time [%s]; MCP Amplitude, mV; MCP Time, ns", nMCP,nMCP,filecomment.Data()),
                            hist_bin_calc(MCP_ampl_bin, MCP_ampl_min, MCP_ampl_max),MCP_ampl_min,MCP_ampl_max,
                            MCP_time_bin, MCP_time_hist_min, MCP_time_hist_max);
//                            hist_bin_calc(TIME_bin, 0., TIME_halfrange*2.), MCP_time_value - TIME_halfrange,MCP_time_value + TIME_halfrange);
        tree->Draw( Form("main_5MCP%i.Time_pol1-%s:main_5MCP%i.Amplitude*%f>>%s", nMCP,TRIGGEReq.Data(),nMCP,amplAtt,currHist->GetName()),
                    Form("(main_5MCP%i.Time_pol1 != 0.)&&%s", nMCP, MAINselEq.Data()), "col");
        currArray->Add(currHist);

    }


SaveArrayOfObjectsArraysInFile(ArrayToSave,Form("CFD_Result_%s.root",filecomment.Data()));

std::ofstream file_out(Form("CFD_Result_%s.txt",filecomment.Data()));
file_out<<Form("RESULTS FOR \"%s\"\n", filecomment.Data());
for(Int_t nMCP = 0; nMCP<4; nMCP++)file_out<<Form(" CFD_%i Time:%.3fns +- %.1f ps\n", nMCP+1, CFDtime_mean[nMCP], CFDtime_sigma[nMCP]*1000.);
file_out<<endl;
for(Int_t nMCP = 0; nMCP<4; nMCP++)file_out<<Form(" MCP_%i Time:%.3fns +- %.1f ps\n", nMCP+1, MCPtime_mean[nMCP], MCPtime_sigma[nMCP]*1000.);


new TBrowser();

}

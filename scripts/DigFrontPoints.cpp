#include "includes/DigData.h"
#include "includes/utilities.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include <TH1.h>
#include <TStyle.h>

int DigFrontPoints(GlobalProcessStatus *globalProcessState){
	printf("\n\n  ---- DigAnalysis ---- \n\n");

	TObjArray Hists;

	Hists.Add( new TH1I("Amplitude", "Amplitude;mV",1000, 0,1000));
	Hists.Add( new TH1F("TimeByPMT", "Time;Time, ns",2000, -100,100));
	Hists.Add( new TH2F("TIMEvsAMPL", "Time vs Amplitude;Time, ns; Front lenght, ns",2000, -100,100,1000, 0,1000));

	Hists.Add( new TH1I("NfrontPOINTS", "Number of points",20, 0,20));
	Hists.Add( new TH1I("FrontLenght", "Front lenght;ns",1000, 0,10));

	Hists.Add( new TH2F("TIMEvsFLENGH", "Time vsFront lenght;Time, ns; Front lenght, ns",2000, -100,100,1000, 0,10));

	DigData *MainData = new DigData(globalProcessState);
	//MainData->OpenData("/mnt/hgfs/DETECTORS_DATA/Fit_June2015/07062015-1052-NO-MCP2-HV1582-MCP4-HV1750-V0_6cm_XP2020_HV1600_ch10");
	//MainData->OpenData("/media/data1/Data_Oct_2015/08102015_1045_RUN8/DataConfig.txt");
	//MainData->OpenData("/media/data1/Data_Oct_2015/08102015_1341_RUN10_front_tightened_T0_D1delayed/DataConfig.txt");
	//MainData->OpenData("/media/data1/Data_Oct_2015/09102015_1358_RUN23_NBI1655frag22_MOD1618frag4_6gevc_collimator4x4/DataConfig.txt");
	//MainData->OpenData("/media/data1/Data_Oct_2015/09102015_1633_RUN24_CFD_testRUN/DataConfig.txt");
	MainData->OpenData("/media/data1/Data_Oct_2015/10102015_1158_RUN28_CFD_testRUN_cabling_test/DataConfig.txt");

	MainData->GetDataRecipe()->GetMainDetault()->minAmplitude = 0.;


	DigEvent *CurEvent = MainData->GetEvent(0);
	int NumEvent = CurEvent->GetTotalEvents();
	float TimeDim = MainData->GetDataRecipe()->GetProcParam()->TimeDimension;

	const int Nsignals = 4;
	int CHsignal[Nsignals];
	MAIN_signal *MAIN_s[Nsignals];



	CHsignal[0] = MainData->GetDataRecipe()->FindChannelByName("5CFD1");
	CHsignal[1] = MainData->GetDataRecipe()->FindChannelByName("T0_C2");
	CHsignal[2] = MainData->GetDataRecipe()->FindChannelByName("T0_D2");
	//CHsignal[3] = MainData->GetDataRecipe()->FindChannelByName("TRG1");

	for(int s=0; s< Nsignals; s++){ printf("CH %i\n", CHsignal[s]); if( CHsignal[s] < 0 ) return 0;}

	for(int s=0; s< Nsignals; s++)CurEvent->GetChannel(CHsignal[s])->GetSignal(&(MAIN_s[s]));


	int prout = 0, AmplIsNOTvalid;

	int EvCalc;
	EvCalc = NumEvent;
	EvCalc = 10000;

	printf("Total events: %i\n", NumEvent);

	for(int nEvent = 0; nEvent < EvCalc; nEvent+=1){
		if( nEvent%31==0){ printf("processed %i (%.0f\%%)\r",nEvent, floor((float)nEvent/(float)EvCalc*100.)); std::cout<<std::flush;};

		if(prout) printf("-------------------------------------------\n");
		CurEvent->SetEventNumber(nEvent);

		AmplIsNOTvalid = 0;
		for(int s=0; s<Nsignals; s++) if( !CurEvent->GetChannel(CHsignal[s])->FindSignalAmplitude() ) AmplIsNOTvalid = 1;
		if(AmplIsNOTvalid) continue;

		for(int s=0; s<Nsignals; s++) CurEvent->GetChannel(CHsignal[s])->FindSignalParametrs();

		float STARTtimePMTs = (MAIN_s[1]->Time_pol1 + MAIN_s[2]->Time_pol1)*0.5;

		float SIGNALtime = MAIN_s[0]->Time_pol1;
		float SIGNALamplitude = MAIN_s[0]->Amplitude;

		float FRONTstartCH = MAIN_s[0]->time_begin / TimeDim;
		float FRONTendCH = MAIN_s[0]->time_front_end / TimeDim;
		float SIGNALflorLenght = MAIN_s[0]->time_front_end - MAIN_s[0]->time_begin;

		int NfrontPOINTS = floor(FRONTendCH) - ceil(FRONTstartCH);

		//if((SIGNALamplitude<400)||(700<SIGNALamplitude)) continue;
		//if(((STARTtimePMTs - SIGNALtime)>5)||(3>(STARTtimePMTs - SIGNALtime))) continue;

		( (TH1*)Hists.FindObject("TimeByPMT") )->Fill(STARTtimePMTs - SIGNALtime);
		( (TH1*)Hists.FindObject("NfrontPOINTS") )->Fill(NfrontPOINTS);
		( (TH1*)Hists.FindObject("FrontLenght") )->Fill(SIGNALflorLenght);
		( (TH2*)Hists.FindObject("TIMEvsFLENGH") )->Fill(STARTtimePMTs - SIGNALtime,  SIGNALflorLenght);
		( (TH2*)Hists.FindObject("TIMEvsAMPL") )->Fill(STARTtimePMTs - SIGNALtime,  SIGNALamplitude);
		( (TH1*)Hists.FindObject("Amplitude") )->Fill(SIGNALamplitude);

	}


	HDraw(Hists, "Signal", 2,2);
	delete MainData;


	return 1;
}

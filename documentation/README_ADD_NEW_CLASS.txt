22.05.2016 - Maciej Slupecki <maciej.slupecki@gmail.com>

Instructions how to add a new class to the project:

1. Create header (newClass.h) file, which contains only class declaration and all includes, which your class will need. Put this file in the 'includes' directory

2. Create source (newClass.cpp) file, in which all the methods are defined. Place this file in the main directory (where the 'makefile' is). Remember to include only one header file here (#include "includes/newClass.h")

3. It is important that both files have the same name.

4. If your new class inherits from any root class the dictionary needs to be created. Open file includes/LinkDef.h and add a line corresponding to your new class, ie:
   #pragma link C++ class newClass+;

5. Modify the makefile.
5a. Add source file to the list in line 87:
    SOURCE = main.cpp alit0timeamplcorr.cpp [...] newClass.cpp
5b. If your class inherits from any root class, modify also line 86: 
    ROOTDICTSRC	= includes/alit0timeamplcorr.h includes/DigChannel.h [...] includes/newClass.h includes/LinkDef.h
    Make sure LinkDef.h is the last entry in the ROOTDICTSRC.

6. Compile the project with command 'make', which should be run in the same directory, in which makefile is.

7. In case of problems with compilation of the new class - check your code.

8. In case of problems with linking (especially if one of the existing classes was modified) try running 'make clean' before 'make'. Make sure your environmental variables $PATH and $LD_LIBRARY_PATH are set correctly. If not then run thisroot.sh script.


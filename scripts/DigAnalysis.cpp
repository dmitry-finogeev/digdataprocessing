#include "includes/DigData.h"
#include "includes/utilities.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include "TStyle.h"

int DigAnalysis(GlobalProcessStatus *gProcessState){
	printf("\n\n  ---- DigAnalysis ---- \n\n");

	DigData *MainData = new DigData(gProcessState);
	MainData->OpenData("/mnt/hgfs/DETECTORS_DATA/Fit_June2015/05062015-1608-NO-MCP1-MCP2_170deg-HV2150-X");

	float TimeDim = MainData->GetDataRecipe()->GetProcParam()->TimeDimension;

	TCanvas *Canvas = new TCanvas("canv", "Signals",900,700);
	TGraph *Graph = new TGraph();
	//Canvas->Divide(2,2);
	//TGraph *Graph;
	//Canvas->cd();

	int mainCH1 = 17;

	gStyle->SetFuncColor(kRed);

	DigEvent *CurEvent = MainData->GetEvent(0);
	int NumEvent = CurEvent->GetTotalEvents();
	MAIN_signal *Main_S; CurEvent->GetChannel(mainCH1)->GetSignal(&Main_S);

	//AMPLITUDE_signal *MCP2_s; CurEvent->GetChannel(mainCH1)->GetSignal(&MCP2_s);
	float *DataArray; int NumDataPoints;

	for(int nEvent = 0; nEvent < NumEvent; nEvent+=1){
		printf("-------------------------------------------\n");
		CurEvent->SetEventNumber(nEvent);
		//int amv = CurEvent->GetChannel(mainCH1)->FindSignalAmplitude();

		//if( (MCP2->Amplitude > 150) )
		//if( (MCP2->Time_pol3 == 0.)&&(MCP2->Amplitude > 50) )
		if( 1 )
		{
			CurEvent->CalculateEvent();

			printf("EVENT %d\n",nEvent);
			Main_S->PrintOut();

			//How to get Amplitude for current event:
			//Main_S->Amplitude

			//How to get data array:
			DataArray = CurEvent->GetChannel(mainCH1)->GetDataBuffer(&NumDataPoints);
			//printf ("\n"); for(int i=0;i<NumDataPoints;i++)printf("%.2f; ", DataArray[i]); printf("\n"); //Time = i*TimeDim

			//Canvas->cd(1);
			Graph = CurEvent->GetChannel(mainCH1)->GetSignalGraph();
			TF1 *FitFunc = CurEvent->GetChannel(mainCH1)->GetSignalFitFunc_pol1();


			float TimeBegin = Main_S->time_begin;
			float TimeEnd = Main_S->time_front_end;

			for(int i=0;i<NumDataPoints;i++){
				int nPoint=0;
				float AmplPoint = DataArray[nPoint];
				float TimeDim = MainData->GetDataRecipe()->GetProcParam()->TimeDimension;
				float TimePiont = (float)nPoint * TimeDim;

			}

			//Graph->SetHistogram( CurEvent->GetChannel(2)->GetSignalGraph()->GetHistogram() );
			//Graph = (TGraph *)CurEvent->GetChannel(2)->GetSignalGraph()->Clone();
			//memcpy(Graph, CurEvent->GetChannel(2)->GetSignalGraph(), sizeof(CurEvent->GetChannel(2)->GetSignalGraph()));

			//Graph->SetMaximum(450.);
			//Graph->SetMinimum(0.);
			//Graph->GetXaxis()->SetRange(floor(Main_S->time_begin*5-20),ceil(Main_S->time_back_begin*5+10));

			Graph->Draw("ALP*");
			Canvas->Update();
			Canvas->Modified();
			printf("INPUT 'q' TO QUIT -------------------------\n");
			//Canvas->GetSelectedPad()->Modified();

			char key = getchar();
			if(key == 'q')break;

		}
	}


	delete MainData;
	return 1;
};

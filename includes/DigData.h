/*!
 * \file
 * \brief DigData class description
 * \date 04.08.2015
 * \version 0.3.1
 */

#ifndef DIGDATA_H
#define DIGDATA_H

#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string.h>

#include "DigVariables.h"
#include "DigDataRecipe.h"
#include "DigEvent.h"

#include <TString.h>
#include <TGraph.h>
#include <TObjArray.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TFile.h>
#include <TDirectory.h>

/*!
 * \brief Head class of project "DigDataProcessing"\n
 * \details This is a head class of project "DigDataProcessing"\n
 * which works with files recorded with digitizer. Its main function:\n
 * - Controls the opening and closing data files\n
 * - Configures data processing parameters for reading data\n
 * - Saving data as TTree in root file ( SaveDataAsTree() )\n\n
 * How to use this class:\n\n
 * TObjArray Hists; // array of histograms from processing parameters calculation\n
 * DigData *Data = new DigData(); //new DigData class\n
 * DigDataRecipe *DataRecipe_ptr = Data->GetDataRecipe(); //getting DigDataRecipe ptr to change default data processing parameters \n
 * DataRecipe_ptr->AddEssentialChannel("T0_C1",1); //include essential channels (see class DigDataRecipe)\n
 * DataRecipe_ptr->AddEssentialChannel("T0_D1",1);\n
 * DataRecipe_ptr->GetMainDetault()->minAmplitude = 50.; // set minimal amplitude for all channels (default)\n
 * DataRecipe_ptr->GetMainDetault()->POL3.FittingON = 0; //fitting procedure by polynome ^ 3 is off\n
 * DataRecipe_ptr->Configure("/Data/06062015"); //open configuration file, and configure data processing parameters\n
 * Data->SaveDataAsTree("RUN23_",10000); //saving data as TTree in file RUN23_.root processing 10000 events, to process all events set this value to 0\n
 * Data->GetProcHistArray(&Hists); //get histograms with processing parameters calculation issue\n\n
 * \warning be shure that the data processing parameters was setted corectly.\n
 * check histograms to see voltages amplitudes ranges
 * \authors Dmitry Finogeev (dmitry-finogeev@yandex.ru)\n
 * Arseniy Shabanov (arseniy.shabanov@phystech.edu)\n
 * Artem Konevskih
 */
class DigData{
public:
	DigData(GlobalProcessStatus *status);
	DigData(GlobalProcessStatus *status, const char *DigFilesPath_);

	~DigData();

private:
	void DigDataInit();

	GlobalProcessStatus *gProcessState;
	DigDataRecipe *DataRecipe_ptr;
	DigEvent *CurrentEvent_ptr;

	TObjArray *HistsProc_ptr; //histogramms for calculate processing parameters

	//set processing parameter for channels according to DataProcParameters
	void ConfAmplitude(int channel); //called by DataProcParamConfigure for channels
	void ConfChisquare(int channel); //called by DataProcParamConfigure for channels

public:
	int OpenData(); //construct event and calc proc parameters if DaraRecipe loaded and configured
	int OpenData(const char *DigFilesPath_);//same, but open/reopen DataRecipe
	int SaveDataAsTree(const char *FileName, int numEv);
	void DataProcParamConfigure();

	//delete all elements in array
	void ResetArray(TObjArray *&array);

	DigDataRecipe* GetDataRecipe();
	DigEvent *GetEvent(int nEvent);
	void GetProcHistArray(TObjArray*);
};

#endif // DIGDATA_H

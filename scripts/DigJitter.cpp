#include "includes/DigData.h"
#include "includes/utilities.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TF1.h>
#include <TStyle.h>

int DigJitter(GlobalProcessStatus *globalProcessState){
	printf("\n\n  ---- Dig Jitter ---- \n\n");

	TObjArray hists;

	TH1 *TimeDistHist_ptr, *AmplDistHist_ptr;
	TH2 *ATimeDistHist_ptr, *AAmplDistHist_ptr, *ATDistHist_ptr;

	hists.Add( TimeDistHist_ptr = new TH1F("TimeDist","Time Distribution abs(Tpol - Ti); dTime [ns]",1000,-10,10) );
	hists.Add( AmplDistHist_ptr = new TH1F("AmplDist","Ampl Distribution abs(Apol - Ai); dAmpl [mV]",1000,-10,10) );
	hists.Add( ATimeDistHist_ptr = new TH2F("ATimeDist","Time Distribution abs(Tpol - Ti) vs Ampl; amplitude [mV]; dTime [ns]",1000,-1000,1000,1000,-10,10) );
	hists.Add( AAmplDistHist_ptr = new TH2F("AAmplDist","Ampl Distribution abs(Apol - Ai) vs Ampl; amplitude [mV]; dAmpl [mV]",1000,-1000,1000,1000,-10,10) );
	hists.Add( ATDistHist_ptr = new TH2F("ATDist","Ampl-Time Distribution; dAmpl [mV]; dTime [ns]",1000,-10,10,1000,-10,10) );

	DigData *MainData = new DigData(globalProcessState);
	MainData->OpenData("/mnt/hgfs/DETECTORS_DATA/Fit_June2015/DigNorm/ATmes");

	DigEvent *CurEvent = MainData->GetEvent(0);
	int NumEvent = CurEvent->GetTotalEvents();

	int mainCH1 = 0;
	MAIN_signal *Main_S; CurEvent->GetChannel(mainCH1)->GetSignal(&Main_S);
	float TimeDim = MainData->GetDataRecipe()->GetProcParam()->TimeDimension;
	float *DataArray; int NumDataPoints;


	for(int nEvent = 0; nEvent < NumEvent; nEvent+=1){
		CurEvent->SetEventNumber(nEvent);
		CurEvent->CalculateEvent();

		DataArray = CurEvent->GetChannel(mainCH1)->GetDataBuffer(&NumDataPoints);
		TF1 *FitFunc = CurEvent->GetChannel(mainCH1)->GetSignalFitFunc_pol1();

		float TimeBegin = Main_S->time_begin;
		float TimeEnd = Main_S->time_front_end;

		for(int nPoint=0;nPoint<NumDataPoints;nPoint++){

			float AmplPoint = DataArray[nPoint];
			float TimePiont = (float)nPoint * TimeDim;

			if( (TimeBegin <= TimePiont)&&(TimePiont<=TimeEnd) )
			{
				//printf("[%f, %f]",TimePiont,AmplPoint);

				float Derivative = FitFunc->Derivative(TimePiont);
				float DistanceToFunc = FitFunc->DistancetoPrimitive(TimePiont, AmplPoint)/10000.;

				float DeltaTime =  DistanceToFunc/TMath::Sqrt(Derivative*Derivative + 1.);
				float DeltaAmpl =  DeltaTime * Derivative;

				//DeltaAmpl = FitFunc->Eval(TimePiont)-AmplPoint;
				//DeltaTime = FitFunc->GetX(AmplPoint, TimeBegin, TimeEnd) - TimePiont;

				TimeDistHist_ptr->Fill( Derivative );
				AmplDistHist_ptr->Fill( DistanceToFunc );
				ATimeDistHist_ptr->Fill( AmplPoint, DeltaTime);
				AAmplDistHist_ptr->Fill(AmplPoint, DeltaAmpl);
				ATDistHist_ptr->Fill(DeltaAmpl,DeltaTime);
			}


		}

	}

	delete MainData;

	HDraw(hists,"hists");

	return 1;
}

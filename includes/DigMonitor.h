/*
 * DigMonitor.h
 *
 *  Created on: May 18, 2016
 *      Author: mss
 */

#ifndef INCLUDES_DIGMONITOR_H_
#define INCLUDES_DIGMONITOR_H_

#include "DigData.h"
#include "DigDataAnalysis.h"
//#include "DirListFrame.h"

#include <TCanvas.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TList.h>
#include <string.h>
#include <ctime>
#include <TMath.h>
#include <TH1F.h>
#include<TSystem.h>
#include<TSystemDirectory.h>

#include<TROOT.h>
#include <TGListBox.h>
#include <TSystem.h>
#include <TApplication.h>
#include <TGClient.h>
#include <TGButton.h>
#include <TGListBox.h>
#include <TGLabel.h>
#include <TRootEmbeddedCanvas.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <TGFileDialog.h>
#include <TBrowser.h>
//#include <TTreeViewer.h>
#include <TGNumberEntry.h>
#include <TGButton.h>

#include <stdlib.h>
#include <TGListTree.h>
#include <TGFrame.h>
#include <TFile.h>
#include <TGIcon.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TPolyMarker.h>
#include <TSystemDirectory.h>
#include <RQ_OBJECT.h>
#include <TTimer.h>
#include <TTime.h>
#include <TGProgressBar.h>

class EvSelectotrFrame : public TGMainFrame{
private:
    TGTextButton *Bapply, *Breset;
    TGListBox *LselectCH;
    TGCheckButton *IsSelectorON, *ISselectionAND;

    TGNumberEntry *minAmplitude, *maxAmplitude;
    TGNumberEntry *minTime ,*maxTime;
    TGNumberEntry *minTime_pol1 ,*maxTime_pol1;
    TGNumberEntry *minTime_pol3 ,*maxTime_pol3;
    TGNumberEntry *minZeroLevel ,*maxZeroLevel;
    TGNumberEntry *minZeroLevelRMS ,*maxZeroLevelRMS;
    TGNumberEntry *mintime_peak ,*maxtime_peak;
    TGNumberEntry *mintime_begin ,*maxtime_begin;
    TGNumberEntry *mintime_end ,*maxtime_end;
    TGNumberEntry *mintime_front_end ,*maxtime_front_end;
    TGNumberEntry *mintime_back_begin ,*maxtime_back_begin;
    TGNumberEntry *minCharge_GATE ,*maxCharge_GATE;

    DigData *DigEvData;
    EventSelectorCond *Selector;

public:

    void LoadSelection();
    void DOreset();
    void DOapply();

    EvSelectotrFrame(DigData *Data_, EventSelectorCond *Selector_, const TGWindow *p, UInt_t w, UInt_t h, GlobalProcessStatus *state);
    virtual ~EvSelectotrFrame();

    void CloseWindow();

    ClassDef(EvSelectotrFrame, 0)
};


class ParametersFrame : public TGMainFrame{

private:
    GlobalProcessStatus *gProcessState;
    TGNumberEntry *NEstat_p, *NEstat_s;
    TGTextEntry *crtlGroupMask;
    TGListBox *LstartCH;
    //TGListBox *LstartCHgr1;

    TGLabel *statusLabel;
    TGTextButton *Bcalculate, *Breset;
    TTimer *Timer;
    TGCheckButton *IsDeleteFiles;

    TGHProgressBar *ProgressBar;

    DigData *DigEvData;

    int FrameLenght;
    time_t start_time;

    int StatProc, StatSave;

public:

    ParametersFrame(DigData *Data_, const TGWindow *p, UInt_t w, UInt_t h, GlobalProcessStatus *state);
    virtual ~ParametersFrame();

    void CloseWindow();

    void DOreset();
    void DOcalculate();
    void DOtimer();

    ClassDef(ParametersFrame, 0)
};




class MyMainFrame : public TGMainFrame {

private:
    GlobalProcessStatus *gProcessState;
    TRootEmbeddedCanvas *fEcanvas;
    TGLabel *EvLabel1, *EvLabel2, *InfoLabel;
    TGTextEntry       *EvEdit;
    TGListBox *fChannelList;
    TList *ChList;
    TGTextButton *Bstart, *Bstop, *Bstartfb, *Bnext;
    TGTextButton *Bparall, *Bparsel, *Bparpr, *BeventSelector;
    TGTextButton *Bopen, *Bampldraw, *Bparameters, *Bbrowser;

    //parameters:
    TGNumberEntry *minAmplGateN, *maxAmplGateN;
    TGNumberEntry *NEampl_min, *NEampl_rng;
    TGCheckButton *IsAmplitudeCalc;
    TGCheckButton *IsInspect, *IsPOL1, *IsPOL3;


    //amplitudes
    TCanvas *AmplCanv;
    TObjArray AmplHistsArr;

    TString FileName;
    DigData *DigEvData;
    int TotalEvents, CurEvent;
    int show_ongoing;
    int show_speed;
    int num_ch_to_show;
    EventSelectorCond Selector_cond;
    TPolyMarker *SignalMarkers;

    void ShowEvLoop(int EventsToShow = 0);


public:

    MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, GlobalProcessStatus *state);
    virtual ~MyMainFrame();

    void CloseWindow();

    void ReDivideCanvas();
    void DOstart();
    void DOstop();
    void DOnext();
    void DOparall();
    void DOparsel();
    void DOparpr();
    void DOcontinue();
    void DOopen();
    void DOampldraw();
    void DOparameters();
    void DObrowser();
    void DOeventSelector();
    void LoadFiles();
    void SetFileName(TString FName);

    void OpenFile(TString filename);

    ClassDef(MyMainFrame, 0)
};

void DigMonitor(GlobalProcessStatus *gProcessState, TString filename = "");




#endif /* INCLUDES_DIGMONITOR_H_ */

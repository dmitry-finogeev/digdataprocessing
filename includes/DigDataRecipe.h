#ifndef DIGDATARECIPE_H
#define DIGDATARECIPE_H

/*!
 * \file
 * \brief class DigDataRecipe description
 */

#include "DigVariables.h"

#include <iostream>
#include <Rtypes.h>
#include "TObject.h"
#include <string.h>
#include <TFile.h>

#include<TROOT.h>
#include<TSystem.h>
#include<TSystemDirectory.h>
#include<TSystemFile.h>
#include<TObjArray.h>

/*!
 *\brief class DigDataRecipe contains parameters used for load and process data
 *\details This class contains srtuctures DataConfiguration and DataProcParameters
 * to manage the data processing. DataConfiguration is lading from file "DataConfig.txt" and describe
 * data configuration: channels type, name, files path ext... DataProcParameters
 * contains information about data processing calculatin events and saving tree: usage of filters, list of channels
 * with mandatory signals in event, ect...\n
 *  Also class have lists of structures Main_Processing_Param and Amplitude_Processing_Param
 * wich describe parameters to calculate corresponding data variables.\n
 *  Class contains all necessary procedures to service this stuctures.\n
 *  All structures have default configuration when class constructed
 * if needed, set ProcParameters, and default main and ampl  proc param
 * load conf files with settet parameters\n
 *  Configuration status checked in Configuration.IsConfigured flag.\n
 *  The configuraion sequence:\n
 * - flag == 0: DigData class and Configuration are empty
 * DataProcParameters and default Processing_Param's are default\n
 * - flag == 1: Configuration loaded from DataConfig.txt\n
 * - flag == 2: channel Processing_Parameters for all channels setted
 * according to default. To set certain parameters to all channels, save this parameters in
 * default before configurate parameters. After configuration individual channels parameters may be
 * setted\n
 * flag == 3: DigEvent constructed with loaded Configuration in DigData class. Data can  be readed\n
 * flag == 4: Data are "scanned" by procedure DigData::DataProcParamConfigure(). Channel Processing_Param's
 * are corrected according to DataProcParameters: amplitude range, fitting maximum chisquare, ect...
 * data are ready to be written as TTree in file\n
 *
 */
class DigDataRecipe: public TObject {

public:
	static char ChSignalTypeName[5][MAX_NAME_LENGHT];
	static char ChPolarityName[2][MAX_NAME_LENGHT];

	DigDataRecipe();
	DigDataRecipe(const char *DataPath_);
	virtual ~DigDataRecipe();

private:
	DataConfiguration Configuration;
	DataProcParameters ProcParameters;

	Main_Processing_Param Main_Default;
	Main_Processing_Param List_Main_Param[MAX_CH];
	int Num_Main_Param;

	Amplitude_Processing_Param Amplitude_Default;
	Amplitude_Processing_Param List_Ampl_Param[MAX_CH];
	int Num_Ampl_Param;

	void DefaultSettings();
	int LoadConfig(const char *DataPath_); //load configuration from file

public:
	int BuildParameters(); //makes default ch_config
	int Configure(const char *DataPath_);

	DataConfiguration* GetConfiguration();
	DataProcParameters* GetProcParam();

	Main_Processing_Param* GetMainParam(int Ch);
	Amplitude_Processing_Param* GetAmplParam(int Ch);

	Main_Processing_Param* GetMainDetault();
	Amplitude_Processing_Param* GetAmplitudeDefault();

	int FindChannelByName(const char *CHName);
	int FingChannelByType(ChSignalType CHtype);//reurn first in array used for XY cannels
	void ClearEssentialChList();
	int AddEssentialChannel(const char *CHname, int val);//val: 0-amplitude, 1-pol1, 2-pol3, ...;

    int DeleteUnusedDataFiles();

	void PrintOut(int level = 0);
	void Reset(); //full reset, use only in constructor

	ClassDef(DigDataRecipe, 1)
};

#endif // DIGDATARECIPE_H

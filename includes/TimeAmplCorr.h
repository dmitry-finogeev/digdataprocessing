
#ifndef TIMEAMPLCORR_H
#define TIMEAMPLCORR_H

#include <TFile.h> 
#include <TH1.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TPaveLabel.h>
#include <TString.h>

int FinFit(TH1 *HistSl, TF1 *Pol, Int_t X1, Int_t X2, const int maxPow, const int Nfunc);

#endif // TIMEAMPL_H

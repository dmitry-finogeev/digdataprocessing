#ifndef DIGVARIABLES_H
#define DIGVARIABLES_H

#include <TSystem.h>
#include <TROOT.h>
#include <stdio.h>
#include <TString.h>

#define MAX_NAME_LENGHT 500
#define MAXLINE 1000 //for run description ext
#define MAX_CH 18
#define VERBOSE 1 //define printing in terminal 0-nothing 1-something 2 all
#define VOID_VALUE -1.
#define IS_VOID_VALUE(val) ((val<VOID_VALUE+0.1)&&(VOID_VALUE-0.1<val))

enum ChSignalType {UNDEFINED=0, LOGIC, AMPLITUDE, XY_PANEL_X, XY_PANEL_Y}; //signal types
enum ChPolarityType {POSITIVE=0, NEGATIVE}; //polarity



//class for status bar at DigMonitor
class GlobalProcessStatus{
public:
    GlobalProcessStatus();
    ~GlobalProcessStatus();

    int enabled;
    int break_flag;
    int step;  // 0 - nothing, 1 data processing; 2 raw ttree saving; 3 analysing; 4 saving analysed ttree
    float percent;

    int GetPercent();
    void SetStatus(int step_, float percent_);
    const char *GetStepName();
};


//--------------------------------------------------------
struct ChConfiguration {
    ChConfiguration();

    char ChName[MAX_NAME_LENGHT];
    char ChComment[MAXLINE];
    char FileName[MAXLINE];

    int IsEnable; //include channel to processing
    ChSignalType ChannelSignalType;
    ChPolarityType ChannelPolarity;

    void Reset();
};

//--------------------------------------------------------
//configuration loaded from file and define names, types, ext of channels
struct DataConfiguration {
    DataConfiguration();
    char DataName[MAX_NAME_LENGHT];
    char RunComment[MAXLINE];
    char DataPath[MAXLINE];

    int NenableCh; //number include channel to processing
    int NactiveCh; //number of channels with defined types (connected to digitizer)
    bool IsReadHeader; // is data header presist
    
    ChConfiguration ChannelConfig[MAX_CH];

    int IsConfigured; //1 - conf loaded, 2 - proc param maked(calc number of ch.. default values)
    //3 event costructed (files opens, objects created) 4 proc param calculated for data by

    void Reset();
};

struct ValueRange{
    float Mean;
    float Sigma;
    float MinValue;
    float MaxValue;

    ValueRange();
    ValueRange(float Mn, float Sg, float Min, float Max);

    float MinRange(float nSigma);
    float MaxRange(float nSigma);

    int InSigmaRange(float Value, float nSigma);
    void PrintOut();
};

//--------------------------------------------------------
//for each channel unic event processing parameters may be setted
//for fitting, main and othe signal types
struct Fitting_Processing{
    Fitting_Processing();
    int FittingON;			//to turn off fitting procedure
    char parameter[10];		//fit parameter
    float minAmplitude;		//events with less ampl not fited
    float MaxChisquare;		//funtion with higher chi rejected
    float ChiRange;			//maxChi = CHI_mean + ChiRange * CHI_RMS
    float SigmaFitFunc;		//sigma with wich mean of fit function calculated
    float BorderFitRejTime;		//to exlude time on border of fit function time = BorderFitRejTime*front_lenght

    void PrintOut();
    void Reset();
};

//--------------------------------------------------------
struct Main_Processing_Param{
    Main_Processing_Param();
    int Channel;
    float minAmplitude;		//some values for events with less ampl not calculated
    float maxAmplitude;
    float minAmplGate; //time gate [ns] for searching signal
    float maxAmplGate; //signals begin and end may be out of gate

    float MainSgnPointsInerval;	//interval from zero level and signal's peak to main four signal points
    float ZeroLevelmaxRMS; 		//RMS with wich algorihm stops
    Fitting_Processing POL1;	//fit parameters for each fit funtion
    Fitting_Processing POL3;

    void PrintOut();
    void Reset();
};


//--------------------------------------------------------
struct Amplitude_Processing_Param{
    Amplitude_Processing_Param();
    int Channel;
    
    int isPrecCalc;
    float treshold;
    float CFD_dt;
    float CFD_fV;

    void PrintOut();
    void Reset();
};


//--------------------------------------------------------
// DataProcParameters is set of parameters for run data
// algorithm use data statistic to determine amplitude range, ect...
// and calculate uncic channel parameters before saving tree

struct DataProcParameters{
    DataProcParameters();
    float AmplitudeDimension;// amplitude dimension [mV]
    float AmplitudeZeroLevel;// U[mV] = AmplDim*A[ch] - AmplZL.
    float TimeDimension;// time dimension [ns]

    int FilterON;			//on/off filter procedure
    int Filter_lenght, Filter_Fs, Filter_Fx;
    int AddFrontPoints;		//on/off aditional points alg.

    int Statistic; //number of event to calc ch proc param
    float AmplRange;    //Ampl min = Amean - Asigma * AmplRange;

    //the are important base channels with out wich event has no meaning
    //for this channel must be calculate main value like pol1 or pol3
    //this array determine wich channels and values for their must be in calculated event
    //this event cut saving time data processing
    TString essentialChName[MAX_CH]; //names of channels
    int essentialValue[MAX_CH]; //code of important value: 0-amplitude, 1-pol1, 2-pol3, ...;
    int essentialChannels; //number of channels includet in the list

    void PrintOut();
    void Reset();
};

//--------------------------------------------------------
//structures for parameter of signals 
//for saving calculating issue at TTree format.
//each signal type matches special class and structure

struct MAIN_signal{
    MAIN_signal();
    float Amplitude;
    float Time;
    float Time_pol1;
    float Time_pol3;
    float ZeroLevel;
    float ZeroLevelRMS;

    //                    0 peak_max
    // peak_front_end  0      0   peak_back_start
    float time_peak;						//               *           *
    float time_begin;						//             *                *
    float time_end;							//            *                     *
    float time_front_end;// point of front end of peak		//           *                          *
    float time_back_begin;// point of start back of peak		//          *                               *
    float Charge_GATE;                                              //         0  peak_start                           0   peak_end
    //     * *                                               *           *
    unsigned int timestamp;
    unsigned int timestamp_us;
    
    void Reset();
    void PrintOut();
    bool IsVoid();
};


struct LOGIC_signal{
    LOGIC_signal();
    float Level;
    void Reset();
    bool IsVoid();
};

struct AMPLITUDE_signal{
    AMPLITUDE_signal();
    float mAmplitude;  // amplitude mean between max +- 1 point calc for prec_ZLevel
    float pfAmplitude;  // amplitude by peak fit
    float acTime;      //time by bettet front fit
    float pfTime;       //time by peak fit
    float Charge;
    float prec_ZLevel; //mean zero level by fit
    float prec_ZLevRMS;
    float LEDtime;
    float CFDtime;

    void Reset();
    void PrintOut();
    bool IsVoid();
};

struct PANNELX_signal{
    PANNELX_signal();
    float Level;
    int   Xch;
    bool  Xarr[5];
    void Reset();
    bool IsVoid();
};

struct PANNELY_signal{
    PANNELY_signal();
    float Level;
    int   Ych;
    bool  Yarr[5];
    void Reset();
    bool IsVoid();
};


//--------------------------------------------------------
//structures for writing analysed data:
struct AmplitudeData{
    float Amplitude;
    float Time_pol1; //time by pol1 relative T0_PMT pol1
    float Time_pol1_ATC; //time pol1 with ampl-time correction relative ATcorrected T0_PMT pol1
    float X_range; //distance from x coordinate to mean x in x_sigma; X_range = |X_mean-X|/X_sigma
    float Y_range; // --//-- for y

    int Check();//1- event exist
    /*
    return (Amplitude == 0.)
        &&(Time_pol1 == 0.)
        &&(Time_pol1_ATC == 0.);
         */

};

struct LogicalData{
    float Time_pol1; //time by pol1 relative T0_PMT pol1

    int Check();
};

//--------------------------------------------------------
//structure for selecting events:
struct EventSelectorCond{
    EventSelectorCond();

    bool isSelectorON;
    bool isANDch_selection;
    bool Channel[MAX_CH];
    MAIN_signal MinValues;
    MAIN_signal MaxValues;

    bool IsSuit( MAIN_signal signal );
    void Reset();
    void PrintOut();
};

#endif // DIGVARIABLES_H
